FROM node:16.14.2-slim AS websliver-base

WORKDIR /app-build/

# copy files needed for yarn install
COPY [ \
    "package.json", \
    "yarn.lock", \
    "./" \
]

COPY [ \
    "packages/web-sliver-data-interface/package.json", \
    "packages/web-sliver-data-interface/yarn.lock", \
    "./packages/web-sliver-data-interface/" \
]

COPY [ \
    "packages/web-sliver-ui-components/package.json", \
    "packages/web-sliver-ui-components/yarn.lock", \
    "./packages/web-sliver-ui-components/" \
]

COPY [ \
    "apps/web-sliver-app/package.json", \
    "apps/web-sliver-app/yarn.lock", \
    "./apps/web-sliver-app/" \
]

# install packages
RUN yarn install


# build phase -----------------------------------------------------------------
FROM websliver-base AS websliver-builder

WORKDIR /app-build/

COPY . ./

# required variables for building. these are defaults
ARG WEBSLIVER_APP_URL='application-url'
ARG WEBSLIVER_API_URL='api-url/v1/graphql'
ARG WEBSLIVER_AUTH_DOMAIN='websliver-auth0-domain'
ARG WEBSLIVER_AUTH_CLIENT_ID='websliver-auth0-client-id'

ENV WEBSLIVER_APP_URL=${WEBSLIVER_APP_URL}
ENV WEBSLIVER_API_URL=${WEBSLIVER_API_URL}

ENV WEBSLIVER_AUTH_DOMAIN=${WEBSLIVER_AUTH_DOMAIN}
ENV WEBSLIVER_AUTH_CLIENT_ID=${WEBSLIVER_AUTH_CLIENT_ID}

# build the app and then clean
RUN yarn workspace web-sliver-app build

# runner image ----------------------------------------------------------------
FROM node:16.14.2-slim

WORKDIR /app-build/

RUN yarn global add serve

COPY --from=websliver-builder \
    /app-build/entrypoint.sh \
    /app-build/package.json \
    /app-build/yarn.lock \
    ./

COPY --from=websliver-builder \
    /app-build/apps/web-sliver-app/package.json \
    /app-build/apps/web-sliver-app/yarn.lock \
    ./apps/web-sliver-app/

COPY --from=websliver-builder \
    /app-build/apps/web-sliver-app/build \
    ./apps/web-sliver-app/build/

RUN chmod +x entrypoint.sh

EXPOSE 9000

ENTRYPOINT ["/app-build/entrypoint.sh"]
