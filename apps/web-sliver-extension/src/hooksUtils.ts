import browser, { Tabs } from 'webextension-polyfill'


export interface ITabsState {
  current: Tabs.Tab | undefined,
  beforeCurrent: Tabs.Tab[],
  afterCurrent: Tabs.Tab[],
  selected: Tabs.Tab[],
  inWindow: Tabs.Tab[],
  all: Tabs.Tab[],
  allActive: Tabs.Tab[],
}

export interface ITabsStorage {
  current: number | undefined,
  beforeCurrent: number[],
  afterCurrent: number[],
  selected: number[],
  inWindow: number[],
  all: Tabs.Tab[],
  allActive: number[],
}

export const getTabsData = async () => {
  const tabs = await browser.tabs.query({})
  const currentWindowId = (await browser.windows.getCurrent()).id

  const includedFields = [
    'active', 'hidden', 'incognito', 'id', 'index', 'lastAccessed', 'windowId', 'highlighted', 'title', 'url', 'favIconUrl'
  ]

  const filterTabObject = (tab: Tabs.Tab) => {
    let newTab = Object.assign({})

    includedFields.forEach((field: string) => newTab[field] = tab[field as keyof Tabs.Tab])

    return newTab
  }

  return {
    tabs: tabs.map((tab: Tabs.Tab) => filterTabObject(tab)),
    currentWindowId
  }
}

export const computeTabsState = ({ tabs, currentWindowId }: { tabs: Tabs.Tab[], currentWindowId?: number }): ITabsStorage => {
  let current: number | undefined = undefined
  let selected: number[] = []
  let inWindow: number[] = []
  let beforeCurrent: number[] = []
  let afterCurrent: number[] = []
  let allActive: number[] = []

  for (const tab of tabs) {
    if (!tab.id) {
      continue
    }

    if (tab.active) {
      allActive.push(tab.id)
    }

    // skip tabs that are not in the current window
    if (tab.windowId !== currentWindowId) {
      continue
    }

    if (tab.active) {
      current = tab.id
    }

    if (current === undefined) {
      // current wasn't found yet
      beforeCurrent.push(tab.id)
    } else {
      // exclude current
      if (tab.id !== current) {
        afterCurrent.push(tab.id)
      }
    }

    if (tab.highlighted) {
      selected.push(tab.id)
    }

    // relies on getting here only for tabs in current window
    inWindow.push(tab.id)
  }

  return {
    current,
    beforeCurrent,
    afterCurrent,
    selected,
    inWindow,
    all: tabs,
    allActive,
  }
}
