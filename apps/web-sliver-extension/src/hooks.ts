import React from 'react'
import { Tabs } from 'webextension-polyfill'

import * as dataService from 'web-sliver-data-interface'

import { storageGet, storageSet } from './browser/storage'
import { getTokenFromStorage } from './browser/auth'
import useAsyncEffect from 'use-async-effect'
import { computeTabsState, getTabsData, ITabsState, ITabsStorage } from 'hooksUtils'


export const useStorage = ({ key, defaultValue = null, reset = false }: { key: string, defaultValue?: any, reset?: boolean }) => {
  const [value, setValueState] = React.useState<any>(defaultValue)
  const [isPending, setIsPending] = React.useState<boolean>(true)

  const setValue = (value: any) => {
    setValueState(value)
    setIsPending(false)
  }


  useAsyncEffect(async (isActive: Function) => {
    let storedValue = null

    if (reset) {
      setValue(defaultValue)
      return
    }

    try {
      storedValue = await storageGet(key)

    } catch (error: any) {
      console.warn(`Could not retrieve ${key} from storage: ${error}`)
    }

    if (!isActive()) return

    // load existing storage if found. default if not
    setValue(storedValue || defaultValue)
  }, [])

  return {
    value,
    setValue: (v: any, persist: boolean = true) => {
      setIsPending(true)

      if (persist) {
        storageSet(key, v).then(() => {
          setValue(v)
        })
      } else {
        setValue(v)
      }
    },
    sync: () => {
      // update state from storage
      storageGet(key)
        .then((v: any) => setValue(v || defaultValue))
        .catch((_e: any) => setValue(defaultValue))
    },
    isPending
  }
}

export const useWorkspaces = ({ token }: { token: string }) => {
  const [workspaces, setWorkspaces] = React.useState<any[]>([])
  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const { value: config } = useStorage({ key: 'config' })
  const host = config?.apiUrl

  useAsyncEffect(async (isActive: Function) => {
    if (!token || !host) {
      return
    }

    const data = await dataService.getWorkspaces({ host, token })

    if (!isActive()) return

    setWorkspaces(data)
    setIsLoading(false)
  }, [token, host])

  return {
    workspaces,
    isLoading
  }
}

export const useMatchingTags = ({ workspaceId, initialInput = '', token }: {
  workspaceId: string,
  initialInput?: string,
  token: string,
}) => {
  const [options, setOptions] = React.useState<TagType[]>([])
  const { value: config } = useStorage({ key: 'config' })
  const host = config?.apiUrl

  useAsyncEffect(async (isActive: Function) => {
    if (!token || !host) {
      return
    }

    const tags = await dataService.getMatchingTags({ workspaceId, value: initialInput, host, token })

    if (!isActive()) return

    setOptions(tags)

  }, [initialInput, token, host])

  return {
    options,
    setOptions
  }
}


export const useTabsStorage = () => {
  const [tabsStorage, setTabsStorageState] = React.useState<ITabsStorage>({
    current: undefined,
    beforeCurrent: [],
    afterCurrent: [],
    selected: [],
    inWindow: [],
    all: [],
    allActive: [],
  })
  const [tabsStorageLoading, setTabsStorageLoading] = React.useState(true)

  const setTabsStorage = (value: ITabsStorage) => {
    setTabsStorageState(value)
    setTabsStorageLoading(false)
  }

  const getTabsStorage = (): ITabsState => {
    // convert from ids
    let result: ITabsState = {
      current: undefined,
      beforeCurrent: [],
      afterCurrent: [],
      selected: [],
      inWindow: [],
      all: [],
      allActive: [],
    }

    tabsStorage.all.forEach((tab: Tabs.Tab) => {
      if (tab.id === undefined) {
        return
      }

      if (tabsStorage.current === tab.id) {
        result.current = tab
      }

      tabsStorage.beforeCurrent.includes(tab.id) && result.beforeCurrent.push(tab)
      tabsStorage.afterCurrent.includes(tab.id) && result.afterCurrent.push(tab)
      tabsStorage.selected.includes(tab.id) && result.selected.push(tab)
      tabsStorage.inWindow.includes(tab.id) && result.inWindow.push(tab)
      result.all = tabsStorage.all
      tabsStorage.allActive.includes(tab.id) && result.allActive.push(tab)
    })

    return result
  }

  const getTabsByKey = (key: keyof ITabsStorage): Tabs.Tab[] => {
    if (!Object.keys(tabsStorage).includes(key)) {
      throw Error(`Unknown tabs storage key ${key}.`)
    }

    switch (key) {
      case 'current':
        const current = tabsStorage.all.find((tab: Tabs.Tab) => tab.id === tabsStorage.current)
        return current ? [current] : []

      case 'all':
        return tabsStorage.all

      default:
        return tabsStorage.all.filter((tab: Tabs.Tab) => tab.id && tabsStorage[key].includes(tab.id))
    }
  }

  const getCount = (key: keyof ITabsStorage): number => {
    if (!Object.keys(tabsStorage).includes(key)) {
      throw Error(`Unknown tabs storage key ${key}.`)
    }

    switch (key) {
      case 'current':
        const current = tabsStorage.all.find((tab: Tabs.Tab) => tab.id === tabsStorage.current)
        return current ? 1 : 0

      case 'all':
        return tabsStorage.all.length

      default:
        return tabsStorage[key].length
    }
  }

  useAsyncEffect(async (isActive: Function) => {
    const data = await getTabsData()

    if (!isActive()) return

    const state = computeTabsState(data)
    setTabsStorage(state)
  }, [])

  return {
    getTabsStorage,
    getTabsByKey,
    getCount,
    tabsStorageLoading
  }
}


export const useAuthToken = () => {
  const [token, setToken] = React.useState<string>('')
  const [isAuthenticated, setIsAuthenticated] = React.useState<boolean>(false)

  useAsyncEffect(async (isActive: Function) => {

    let token = null
    try {
      token = await getTokenFromStorage()
    } catch (error: any) {
      console.error(error)
    }

    if (!isActive()) return

    if (!token) {
      setToken('')
      setIsAuthenticated(false)

    } else {
      setToken(token)
      setIsAuthenticated(true)
    }

  }, [])

  return {
    token,
    isAuthenticated
  }
}
