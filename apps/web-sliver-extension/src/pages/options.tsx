import { CssBaseline, ThemeProvider } from '@mui/material'
import React from 'react'
import ReactDOM from 'react-dom'

import theme from '../components/Theme'
import Options from 'screens/options'


const App = () => {

  return (
    <React.StrictMode>
        <ThemeProvider theme={theme} >
          <CssBaseline />
          <Options />
        </ThemeProvider>
    </React.StrictMode>
  )
}

const rootElement = document.getElementById('root')
ReactDOM.render(<App />, rootElement)
