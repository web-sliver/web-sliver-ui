import React from 'react'
import {
  Bookmark as BookmarkIcon, Bookmarks as BookmarksIcon, Settings as SettingsIcon,
} from "@mui/icons-material"
import {
  Alert,
  BottomNavigation,
  BottomNavigationAction,
  Box, FormControl, Paper, Typography
} from '@mui/material'

import browser from 'webextension-polyfill'
import useAsyncEffect from 'use-async-effect'


import { saveTabs, setIconForTab } from '../browser/service'
import * as dataService from 'web-sliver-data-interface'
import { useAuthToken, useStorage, useTabsStorage, useWorkspaces } from 'hooks'
import { ActionsTab } from 'components/TabsContainer/ActionsTab'
import { EntryTab } from 'components/TabsContainer/EntryTab'
import { AppToolbar } from 'components/AppToolbar'
import { settings } from 'config'
import { EntrySearchInput } from 'web-sliver-ui-components'


const styles = {
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'top',

    padding: '8px',

    overflow: 'auto',

    width: '800px',
    height: '600px',

    '& > *': {
      margin: '8px 0',
    }
  },
}

export const Popup = () => {

  const [isLoading, setIsLoading] = React.useState<boolean>(true)
  const [isSaving, setIsSaving] = React.useState<boolean>(false)
  const [selectedScreen, setSelectedScreen] = React.useState(0)
  const [errorMessage, setErrorMessage] = React.useState<string>('')
  const [savedItem, setSavedItem] = React.useState<any>(null)
  const [isRetrieving, setIsRetrieving] = React.useState<boolean>(false)
  const [focusedField, setFocusedField] = React.useState<any>(null)
  const [hasChanges, setHasChanges] = React.useState<boolean>(false)

  const [inputItem, setInputItem] = React.useState<any>({
    title: '',
    description: '',
    location: '',
    tags: [],
  })

  const { value: selectedWorkspaceId, setValue: setSelectedWorkspaceId, isPending: selectedWorkspaceIdLoading } = useStorage({ key: 'selectedWorkspaceId', defaultValue: '' })
  const { value: tagsToAdd, setValue: setTagsToAddState, sync: syncTagsToAdd } = useStorage({ key: 'selectedTags', defaultValue: {} })

  const { getTabsByKey, tabsStorageLoading } = useTabsStorage()
  const currentTab = getTabsByKey('current')[0]

  const { token, isAuthenticated } = useAuthToken()

  const { workspaces, isLoading: workspacesLoading } = useWorkspaces({ token })

  const isReady = !isLoading && isAuthenticated
  const hasItem = !!savedItem
  const hasSelectedWorkspace = !workspacesLoading && !selectedWorkspaceIdLoading
  const { value: config } = useStorage({ key: 'config' })
  const { apiUrl: host } = config || {}

  const getTagsToAdd = (workspace: string) => {
    // normalization
    return tagsToAdd[workspace] || []
  }

  const setTagsToAdd = (tags: EntrySearchInput[]) => {
    setTagsToAddState(Object.assign({}, tagsToAdd, { [selectedWorkspaceId]: tags.map((tag: EntrySearchInput) => typeof tag === 'string' ? tag : tag.name) }))
  }

  useAsyncEffect(async (isActive: Function) => {
    if (tabsStorageLoading || !isAuthenticated || !host || isSaving) {
      return
    }

    let workspaceId = selectedWorkspaceId

    // automatically select the default workspace or the first workspace if none is selected
    if (!selectedWorkspaceId) {
      const defaultWorkspaces = workspaces.filter((workspace: any) => workspace.default)
      const workspace = defaultWorkspaces.length !== 0 ? defaultWorkspaces[0] : workspaces[0]

      if (workspace) {
        selectWorkspace(null, workspace)
        workspaceId = workspace.uid
      } else {
        return
      }
    }

    if (currentTab && currentTab.url) {
      setIsRetrieving(true)
      const items = await dataService.getEntriesByLocations(
        { query: { workspaceId, locations: [currentTab.url], sortField: 'updated_at', sortDirection: 'desc' }, host, token })

      if (!isActive()) return

      const retrievedItem = items.length >= 1 ? items[0] : null

      if (retrievedItem) {
        setInputItem(retrievedItem)
      } else {
        setInputItem({
          ...currentTab,
          title: currentTab.title || '',
          location: currentTab.url || '',
          description: '',
          tags: getTagsToAdd(selectedWorkspaceId),
        })
      }

      setSavedItem(retrievedItem)
      setIsRetrieving(false)
    }

    setIsLoading(false)
  }, [hasSelectedWorkspace, tabsStorageLoading, host, isSaving])

  React.useEffect(() => {
    !hasItem && setInputItem({ ...inputItem, tags: getTagsToAdd(selectedWorkspaceId) })
  }, [tagsToAdd])

  const normalizeTabToSavedItem = (tab: browser.Tabs.Tab) => {
    const { title, url: location } = tab
    return {
      title,
      location,
      description: '',
      tags: getTagsToAdd(selectedWorkspaceId)
    }
  }

  const checkHasChanges = () => {
    if (!currentTab) {
      return false
    }

    const baseItem = hasItem ? savedItem : normalizeTabToSavedItem(currentTab)
    const comparedItem = inputItem

    return (
      baseItem.title !== comparedItem.title
      || baseItem.location !== comparedItem.location
      || baseItem.description !== comparedItem.description
      || baseItem.tags.length !== comparedItem.tags.length
      || !baseItem.tags.every((item: string) => comparedItem.tags.includes(item))
      || !comparedItem.tags.every((item: string) => baseItem.tags.includes(item))
    )
  }

  React.useEffect(() => {
    setHasChanges(checkHasChanges())
  }, [currentTab, savedItem, hasItem, inputItem, setHasChanges ])

  const selectWorkspace = async (_event: any, workspace: any) => {
    setSelectedWorkspaceId(workspace.uid)

    if (!isAuthenticated) {
      return
    }

    for (const tab of getTabsByKey('allActive')) {
      if (tab.id === undefined) {
        continue
      }

      await setIconForTab({
        workspaceId: workspace.uid,
        tabId: tab.id,
        tabUrl: tab.url,
        host,
        token
      })
    }
  }

  const updateItem = async () => {
    try {
      await dataService.updateEntries({ entries: [{ ...inputItem, created_at: null, updated_at: null, }], host, token, throwErrors: true })
    } catch (e: any) {
      if ((e.response?.errors[0].message || '').includes('Uniqueness violation')) {
        setErrorMessage('An entry with the same title and location already exists in this workspace.')
        setTimeout(() => setErrorMessage(''), 10000)

      } else {
        setErrorMessage('Could not update entry!')
        setTimeout(() => setErrorMessage(''), 10000)
      }
    }
  }

  const onSaveItem = async () => {
    setIsSaving(true)

    if (hasItem) {
      await updateItem()
    } else {
      await saveTabs({
        workspaceId: selectedWorkspaceId,
        tabs: [inputItem],
        tags: inputItem.tags,
        doUpsert: true,
        host,
        token
      })
    }

    setIsSaving(false)
  }

  const onDelete = async (_event: any) => {

    if (!isAuthenticated || !hasItem) {
      return
    }

    setIsSaving(true)
    await dataService.deleteEntries({ entryIds: [savedItem.uid], host, token })
    setIsSaving(false)

    currentTab.id !== undefined && await setIconForTab({ workspaceId: selectedWorkspaceId, tabId: currentTab.id, tabUrl: currentTab.url, host, token })
    setSavedItem(null)

    await browser.notifications.clear('websliver-deleted')
    await browser.notifications.create(
      'websliver-deleted',
      {
        type: 'basic',
        title: 'WebSliver - Delete',
        message: `Deleted entry.`
      })
  }

  const onSelectScreen = (_event: React.SyntheticEvent, newValue: number) => {
    setSelectedScreen(newValue)
    // storage changes don't update local state automatically
    syncTagsToAdd()
  }

  return (
    <Paper sx={styles.root}>
      {settings.WEBSLIVER_DEBUG && <Alert severity="info">DEBUG MODE!</Alert>}

      <AppToolbar
        onDelete={onDelete}
        onSave={onSaveItem}
        showUnsavedIndicator={isReady && selectedScreen === 0 && hasChanges}
        showDeleteButton={isReady && selectedScreen === 0 && hasItem}
        showSaveButton={isReady && selectedScreen === 0 && (!hasItem || hasChanges)}
        selectedWorkspaceId={selectedWorkspaceId}
        selectWorkspace={selectWorkspace}
      />

      <Box
        sx={{
          minWidth: '200px',
          minHeight: '200px',
          height: '100%',
          display: 'flex',
          alignItems: 'stretch',
          flexDirection: 'column',
          gap: '20px',
        }}
      >
        {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
        {!isAuthenticated && <Typography sx={{ textAlign: 'center' }}>Not authenticated</Typography>}
        {isAuthenticated && isLoading && <Typography sx={{ textAlign: 'center' }}>Loading...</Typography>}
        {isReady && <>

          <FormControl sx={{ pb: 7 }}>
            {hasSelectedWorkspace && !isRetrieving && <EntryTab
              value={savedItem}
              inputValue={inputItem}
              setInputValue={setInputItem}
              focused={focusedField}
              setFocused={setFocusedField}
              selectedWorkspaceId={selectedWorkspaceId}
              isOpen={selectedScreen === 0}
              onSave={onSaveItem}
            />}

            {hasSelectedWorkspace && <ActionsTab
              setIsSaving={setIsSaving}
              selectedWorkspaceId={selectedWorkspaceId}
              isOpen={selectedScreen === 1}
              tags={getTagsToAdd(selectedWorkspaceId)}
              setTags={setTagsToAdd}
            />}
          </FormControl>

          <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }}>
            <BottomNavigation
              value={selectedScreen}
              onChange={onSelectScreen}
              showLabels
            >
              <BottomNavigationAction icon={<BookmarkIcon />} label="ENTRY" />
              <BottomNavigationAction icon={<BookmarksIcon />} label="ACTIONS" />
              <BottomNavigationAction icon={<SettingsIcon />} disabled label="SETTINGS" />
            </BottomNavigation>
          </Paper>
        </>
        }
      </Box>
    </Paper>
  )
}
