import React from 'react'
import { Button, FormGroup, Paper, TextField } from '@mui/material'

import { useStorage } from 'hooks'
import { settings } from 'config'
import { useTextInput } from 'web-sliver-ui-components'


const Options = () => {
  const { value: config, setValue: setConfig, isPending: configIsPending } = useStorage({
    key: 'config', defaultValue: {
      apiUrl: settings.WEBSLIVER_API_URL,
      appUrl: settings.WEBSLIVER_APP_URL,
      authClientId: settings.WEBSLIVER_AUTH_CLIENT_ID,
      historyTracking: false,
      historyTrackingIncognito: false,
    }
  })

  const setConfigValue = (prop: any) => {
    setConfig({
      ...config,
      ...prop,
    })
  }
  const { value: appUrl, setValue: setAppUrl, bind: bindAppUrl } = useTextInput(config.appUrl, 'WebSliver Application URL')
  const { value: apiUrl, setValue: setApiUrl, bind: bindApiUrl } = useTextInput(config.apiUrl, 'WebSliver API URL')
  const { value: authClientId, setValue: setAuthClientId, bind: bindAuthClientId } = useTextInput(config.authClientId, 'WebSliver Auth Client ID')

  React.useEffect(() => {
    if (!configIsPending) {
      setAppUrl(config.appUrl)
      setApiUrl(config.apiUrl)
      setAuthClientId(config.authClientId)
    }
  }, [configIsPending])

  return (
    <Paper sx={{ height: '100%' }}>
      <FormGroup sx={{ display: 'flex', flexDirection: 'column', gap: 1, p: 1 }}>
        <TextField
          margin='dense'
          variant='outlined'
          helperText='The URL to the WebSliver web application. Example: https://demo-websliver.dystopianside.org'
          {...bindAppUrl}
        />
        <TextField
          margin='dense'
          variant='outlined'
          {...bindApiUrl}
        />
        <TextField
          margin='dense'
          variant='outlined'
          {...bindAuthClientId}
        />

        <Button
          variant='contained'
          onClick={() => {
            setConfigValue({
              appUrl,
              apiUrl,
              authClientId,
            })
          }}
        >
          Save
        </Button>

        {
          // todo: disabled until implemented
        }
        {/* <FormControlLabel
          control={
            <Checkbox
              color='primary'
              checked={config.historyTracking}
              onChange={(event: any) => { setConfigValue({ historyTracking: event.target.checked }) }}
            />
          }
          label="Enable history tracking."
        />

        <FormControlLabel
          control={
            <Checkbox
              color='primary'
              checked={config.historyTrackingIncognito}
              disabled={!config.historyTracking}
              onChange={(event: any) => setConfigValue({ historyTrackingIncognito: config.historyTracking && event.target.checked })}
            />
          }
          label="Enable history tracking in incognito."
        /> */}
      </FormGroup>
    </Paper>
  )
}

export default Options
