
const DECAY_FACTOR = 0.95 // Adjust this value based on how much you want recency to influence frecency
const CLEANUP_INTERVAL = 30 * 24 * 60 * 60 * 1000 // Cleanup data older than 30 days


interface TagInfo {
  tag: string
  frequency: number
  lastUpdated: number // Unix timestamp to store the last update time
}

interface RecentTags {
  [workspaceId: string]: TagInfo[]
}


function calculateFrecency(frequency: number, lastUpdated: number): number {
  const currentTime = Date.now()
  const recency = currentTime - lastUpdated
  const decayedFrequency = frequency * Math.pow(DECAY_FACTOR, recency)  // frequency * decayFactor^recency
  return decayedFrequency * Math.exp(-recency / DECAY_FACTOR) // decayedFrequency * e^(-recency/decayFactor)
}

export const updateRecentTags = (
  recentTags: RecentTags,
  workspaceId: string,
  tagsToUpdate: string[]
) => {
  const currentTime = Date.now()

  if (!recentTags[workspaceId]) {
    recentTags[workspaceId] = []
  }

  for (const tag of tagsToUpdate) {
    const existingTagIndex = recentTags[workspaceId].findIndex((item) => item.tag === tag)

    if (existingTagIndex !== -1) {
      // Tag already exists, update its frequency and last updated time
      const tagInfo = recentTags[workspaceId][existingTagIndex]
      tagInfo.frequency++
      tagInfo.lastUpdated = currentTime
    } else {
      // Tag does not exist, add it with initial frequency 1
      recentTags[workspaceId].push({ tag, frequency: 1, lastUpdated: currentTime })
    }
  }

  // Clean up old data in the recentTags object
  cleanupOldData(recentTags)

  return recentTags
}

export const getRecentTags = (
  recentTags: RecentTags,
  workspaceId: string,
  count: number
): string[] => {
  if (!recentTags[workspaceId]) {
    return []
  }

  // Sort tags by Exponential Frecency in descending order
  const sortedTags = recentTags[workspaceId].sort((a, b) => {
    const frecencyA = calculateFrecency(a.frequency, a.lastUpdated)
    const frecencyB = calculateFrecency(b.frequency, b.lastUpdated)
    return frecencyB - frecencyA
  })

  // Return the first 'count' tags as the most frequent ones
  return sortedTags.slice(0, count).map((tagInfo) => tagInfo.tag)
}

function cleanupOldData(recentTags: RecentTags): void {
  const currentTime = Date.now()

  for (const workspaceId in recentTags) {
    recentTags[workspaceId] = recentTags[workspaceId].filter((tagInfo) => {
      return currentTime - tagInfo.lastUpdated <= CLEANUP_INTERVAL
    })
  }
}

export const dropRecentTag = (recentTags: RecentTags, workspaceId: string, tag: string) => {
  if (!recentTags[workspaceId]) {
    return
  }

  const tagIndex = recentTags[workspaceId].findIndex((item) => item.tag === tag)

  if (tagIndex !== -1) {
    recentTags[workspaceId].splice(tagIndex, 1)
  }

  return recentTags
}
