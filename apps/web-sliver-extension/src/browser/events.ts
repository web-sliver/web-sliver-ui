import { settings } from 'config'
import browser from 'webextension-polyfill'
import { getTokenFromStorage, getTokenFromTab, setTokenStorage } from './auth'

import { setIconForTab } from './service'
import { storageGet, storageSet } from './storage'


const processToken = async ({ tabId, tabUrl }: { tabId: number, tabUrl: string }) => {
  let token = await getTokenFromStorage()

  if (token) {
    return token
  }

  let config = await storageGet('config')
  const wsDomain = config?.appUrl
  if (wsDomain && !tabUrl.startsWith(wsDomain)) {
    return null
  }

  token = await getTokenFromTab({ clientId: config?.authClientId, tabId })

  if (!token) {
    // token was not set by the UI
    return null
  }

  await setTokenStorage(token)

  return token
}

export const onLoad = async () => {
  console.debug('WS: onLoad event')

  let config = await storageGet('config')

  if (!config) {
    // initialize config
    config = {
      apiUrl: settings.WEBSLIVER_API_URL,
      appUrl: settings.WEBSLIVER_APP_URL,
      authClientId: settings.WEBSLIVER_AUTH_CLIENT_ID,
      historyTracking: false,
      historyTrackingIncognito: false,
    }
    await storageSet('config', config)
  }
  const host = config.apiUrl

  const workspaceId = await storageGet('selectedWorkspaceId')

  let token = await getTokenFromStorage()
  if (!token) {
    return
  }

  // skip if no workspace was selected
  if (!workspaceId) {
    return
  }

  // update icons for all active tabs on extension load
  const tabs = await browser.tabs.query({ active: true })

  for (const tab of tabs) {
    if (tab.id) {
      await setIconForTab({ workspaceId, tabId: tab.id, tabUrl: tab.url, host, token })
    }
  }
}

export const onTabUpdated = async (tabId: number, changeInfo: any, tab: any) => {
  const workspaceId = await storageGet('selectedWorkspaceId')
  const config = await storageGet('config')
  const host = config?.apiUrl

  // update icon when a tab is updated
  if (changeInfo.status === 'complete' && tab.active) {
    const token = await processToken({ tabId: tab.id, tabUrl: tab.url || '' })

    if (!token) {
      return
    }

    // set saved/not saved indicator on updated url for active tab
    await setIconForTab({ workspaceId, tabId, tabUrl: tab.url, host, token })
  }
}

export const onTabActivated = async (tab: any) => {
  const workspaceId = await storageGet('selectedWorkspaceId')

  const config = await storageGet('config')
  const host = config?.apiUrl
  const tabData = await browser.tabs.get(tab.tabId)

  const token = await processToken({ tabId: tab.id, tabUrl: tabData.url || '' })

  if (!token) {
    return
  }

  // update icon when a tab is activated
  await setIconForTab({ workspaceId, tabId: tab.id, tabUrl: tabData.url, host, token })
}
