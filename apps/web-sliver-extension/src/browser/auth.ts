import browser from 'webextension-polyfill'
import jwt from 'jwt-simple'

import { storageGet, storageSet } from './storage'


export const getTokenFromTab = async ({ clientId, tabId }: { clientId: string, tabId: number }) => {
  const key = `@@auth0spajs@@::${clientId}::default::openid profile email`
  const code = { code: `localStorage['${key}']` }

  let value = await browser.tabs.executeScript(tabId, code)

  if (!value || value.length === 0 || !value[0]) {
    // key is not set
    return null
  }

  return JSON.parse(value[0]).body?.id_token || null

}

const isTokenExpired = (token: string) => {
  try {
    const decodedToken = jwt.decode(token, '', true, 'RS256')

    return decodedToken.exp * 1000 < Date.now()

  } catch (err: any) {
    console.error(err)
  }

  return true
}

export const getTokenFromStorage = async () => {
  let token = await storageGet('authToken')

  if (token && isTokenExpired(token)) {
    await setTokenStorage(null)
    return null
  }

  return token
}

export const setTokenStorage = async (token: string | null) => {
  await storageSet('authToken', token)
}
