import browser from 'webextension-polyfill'

enum NotificationTypeId {
  ERROR = 'ws.notification.error'
}

enum NotificationTitle {
  ERROR = 'WebSliver - Error'
}

export const showError = async (message: string) => {
  await browser.notifications.clear(NotificationTypeId.ERROR)
  browser.notifications.create(
    NotificationTypeId.ERROR,
    {
      type: 'basic',
      title: NotificationTitle.ERROR,
      message,
    })
}
