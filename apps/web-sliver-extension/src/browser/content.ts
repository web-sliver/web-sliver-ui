import browser from 'webextension-polyfill'
import { storageGet } from './storage';


function openItems({
  items,
  newWindow = false,
  incognito = false,
}: {
  items: any[]
  newWindow?: boolean
  incognito?: boolean
}) {
  browser.runtime.sendMessage({ type: 'ws.ext.openItems', content: { items, newWindow, incognito } });
}

// only allow extension to run on websliver app
const injectScript = async () => {
  const config = await storageGet('config')

  if (!config) {
    console.debug('WS: no config found.')
    return
  }

  if (!window.location.href.startsWith(config.appUrl)) {
    console.debug('WS: not running on websliver app.')
    return
  }

  console.info('WS: Injecting websliver extension context...')

  browser.runtime.onMessage.addListener(async (message) => {
    // pass message to webpage
    if (message.type === 'ws.ext.failedItems') {
      window.postMessage({ type: 'ws.page.failedItems', content: message.content });
    }
  });

  // @ts-ignore
  exportFunction(openItems, window, { defineAs: "openItems" })

  // @ts-ignore
  window.wrappedJSObject.extensionContext = cloneInto({ enabled: true }, window)
}

injectScript()
