import browser, { Tabs } from 'webextension-polyfill'

import * as dataService from 'web-sliver-data-interface'

import { icons } from '../config'

export const setIconForTab = async ({ tabId, workspaceId, tabUrl = undefined, host, token }: {
  tabId: number,
  host: string,
  token: string,
  workspaceId: string,
  tabUrl?: string | undefined,
}) => {
  if (!tabUrl) {
    const tab = await browser.tabs.get(tabId)
    tabUrl = tab.url
  }

  const results = tabUrl ? await dataService.getEntriesByLocations(
    { query: { workspaceId, locations: [tabUrl], sortField: 'updated_at', sortDirection: 'desc' }, host, token }) : []

  let path = icons.NOT_SAVED
  let title = 'WebSliver Extension'

  if (results.length !== 0) {
    path = icons.SAVED
    if (results[0].tags.length !== 0) {
      title = `${title} - tags:\n   ${results[0].tags.join('\n   ')}`
    }
  }

  browser.browserAction.setIcon({ tabId, path })
  browser.browserAction.setTitle({ tabId, title })
}

export type TabEntry = Tabs.Tab & {
  title: string,
  description: string,
  location: string,
}

export const saveTabs = async ({ workspaceId, tabs, tags, doUpsert, host, token }: {
  workspaceId: string,
  tabs: TabEntry[],
  tags: string[],
  doUpsert: boolean,
  host: string,
  token: string,
}) => {

  const entries = tabs.map(tab => ({
    tags,
    workspace_id: workspaceId,
    ...tab,
  }))

  if (doUpsert) {
    await dataService.upsertEntries({ workspaceId, entries, host, token })
  } else {
    await dataService.insertEntries({ workspaceId, entries, host, token })
  }

  for (const tab of tabs) {
    if (tab.active && tab.id) {
      await setIconForTab({ workspaceId, tabId: tab.id, tabUrl: tab.url, host, token })
    }
  }
}
