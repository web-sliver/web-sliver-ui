import browser from 'webextension-polyfill'

import * as events from './events'
import { showError } from './notifications'


// run when background is first loaded
events.onLoad()

browser.tabs.onActivated.addListener(events.onTabActivated)
browser.tabs.onUpdated.addListener(events.onTabUpdated, { properties: ['status'] })

browser.runtime.onMessage.addListener(async (message) => {
  if (message.type === 'ws.ext.openItems') {
    const { items, newWindow, incognito } = message.content
    const activeTab = (await browser.tabs.query({ active: true, currentWindow: true }))[0]
    let windowId = activeTab.windowId
    let cookieStoreId = !incognito && activeTab.cookieStoreId || undefined

    if (incognito && !(await browser.extension.isAllowedIncognitoAccess())) {
      await showError(`Extension does not have access to incognito windows. Please enable access in the extension settings.`)

      return
    }

    if (newWindow) {
      windowId = (await browser.windows.create({
        incognito,
        state: 'maximized',
        cookieStoreId,
      })).id
    }

    let failedItems: any[] = []
    let index = activeTab.index + 1
    for (const item of items) {
      try {
        const location = ['about:blank', 'about:newtab', '', undefined].includes(item.location) ? undefined : item.location
        await browser.tabs.create({
          url: location,
          discarded: !!location,
          title: location && (item.title || item.location),
          index,
          cookieStoreId,
          windowId,
          active: false,
        })
        index += 1
      } catch (error) {
        if ((error as Error).message.startsWith('Illegal URL:')) {

          console.error(`Encountered illegal URL: ${item.location}`)
          failedItems.push(item)
        } else {
          await showError(`Encountered an error while opening ${item.location}: ${(error as Error).message}`)
          throw error
        }
      }
    }

    await browser.tabs.sendMessage(activeTab.id!, {
      type: 'ws.ext.failedItems',
      content: failedItems
    })
  }
});
