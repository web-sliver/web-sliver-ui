import browser from 'webextension-polyfill'


export const storageSet = async (key: string, data: any) => {
    // get existing store
    const store = await browser.storage.local.get()

    // update store
    await browser.storage.local.set(Object.assign({}, store, { [key]: data }))
}

export const storageGet = async (key: string) => {
    // get key from store
    return (await browser.storage.local.get(key))[key]
}
