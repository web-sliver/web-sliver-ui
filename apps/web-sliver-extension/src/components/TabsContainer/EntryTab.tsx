import React from 'react'
import {
  Box, Chip, FormGroup, TextField,
  Typography
} from '@mui/material'
import moment from 'moment'


import { SearchInput, SearchOptionType, StyledUL } from 'web-sliver-ui-components'

import { useAuthToken, useMatchingTags, useStorage } from 'hooks'
import { TabPanel } from 'components/TabsContainer/TabPanel'
import { dropRecentTag, getRecentTags, updateRecentTags } from 'browser/tagsFrecency'



const styles = {
  inputField: {
    width: '100%',
  },
  tagList: {
    display: 'flex',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: 0,
    margin: 0,
  },

  tagChip: {
    margin: 0.3,
  },

  tagChipHighlighted: {
    margin: 0.3,
    backgroundColor: '#202020',
  },
}

export const EntryTab = ({
  isOpen = false,
  selectedWorkspaceId,
  inputValue,
  value,
  setInputValue,
  focused,
  setFocused,
  onSave,
}: {
  inputValue: any,
  value: any,
  isOpen: boolean,
  selectedWorkspaceId: string,
  setInputValue: Function,
  focused: any,
  setFocused: Function,
  onSave: Function,
}) => {
  const { token } = useAuthToken()
  const useOptions = (query: string) => (useMatchingTags({
    workspaceId: selectedWorkspaceId || '',
    initialInput: query ? `.*${query}.*` : '',
    token,
  }).options || []).map((tag: TagType) => ({ name: tag.name, type: SearchOptionType.TAG, uses: tag.uses }))

  const [savedTags, setSavedTags] = React.useState<string[]>(inputValue.tags)
  const { value: recentTags, setValue: setRecentTags } = useStorage({ key: 'recentTags', defaultValue: {} })
  const [displayedRecentTags, setDisplayedRecentTags] = React.useState<string[]>([])

  const arraysAreEqual = (a: string[], b: string[]) => {

    if (a.length !== b.length) {
      return false
    }

    let [x, y] = [a.slice().sort(), b.slice().sort()]

    return x.every((e: string, i: number) => e === y[i])
  }

  React.useEffect(() => {
    if (isOpen && selectedWorkspaceId) {
      setDisplayedRecentTags(getRecentTags(recentTags, selectedWorkspaceId, 10))
    }

    if (isOpen && selectedWorkspaceId && !arraysAreEqual(savedTags, inputValue.tags)) {

      onSave()
      setSavedTags(inputValue.tags)

    } else if (!isOpen) {
      setSavedTags(inputValue.tags)
    }

  }, [inputValue.tags, isOpen, selectedWorkspaceId, recentTags])

  const hasItem = !!value

  const addItemTags = (tags: EntrySearchInput[]) => {
    const normalizedTags = tags.map(tag => typeof tag === 'string' ? tag : tag.name)

    const newTags = normalizedTags.filter(tag => !inputValue.tags.includes(tag))
    newTags.length != 0 && setRecentTags(updateRecentTags(recentTags, selectedWorkspaceId, newTags))

    setInputValue({ ...inputValue, tags: [...inputValue.tags || [], ...normalizedTags] })
  }

  const setItemTags = (tags: EntrySearchInput[]) => {
    const normalizedTags = tags.map(tag => typeof tag === 'string' ? tag : tag.name)

    const newTags = normalizedTags.filter(tag => !inputValue.tags.includes(tag))
    newTags.length != 0 && setRecentTags(updateRecentTags(recentTags, selectedWorkspaceId, newTags))

    setInputValue({ ...inputValue, tags: normalizedTags })
  }

  const submitOnEnter = {
    onKeyDown: (e: React.KeyboardEvent<HTMLDivElement>) => {
      if (e.key === 'Enter') {
        onSave()
      }
    }
  }

  const onRecentTagClicked = (tag: string) => {
    addItemTags([tag])
  }

  const onRecentTagDelete = (tag: string) => {
    setRecentTags(dropRecentTag(recentTags, selectedWorkspaceId, tag))
    setDisplayedRecentTags(getRecentTags(recentTags, selectedWorkspaceId, 10))
  }

  // note: on saving the entire item with the pre-selected tags, it does not bump frecency. this is intended as it would
  // not make sense to bump frecency for a tag that was not selected by the user.

  return (
    <TabPanel isOpen={isOpen} index={0}>
      <FormGroup sx={{ gap: '5px' }}>

        {!hasItem && <Typography sx={{ p: '0 10px 10px', textAlign: 'center' }}>Add Entry</Typography>}

        {<TextField
          sx={styles.inputField}
          variant='outlined'
          margin='dense'
          size='small'
          value={inputValue.title}
          onChange={(ev: any) => {
            setInputValue({ ...inputValue, title: ev.target.value })
          }}
          inputRef={(input: any) => input && (!focused || focused === 'title') && input.focus()}
          onFocus={() => { setFocused('title') }}
          label='Title'
          {...submitOnEnter}
        />}

        {<TextField
          sx={styles.inputField}
          variant='outlined'
          margin='dense'
          size='small'
          value={inputValue.location}
          onChange={(ev: any) => {
            setInputValue({ ...inputValue, location: ev.target.value })
          }}
          inputRef={(input: any) => input && focused === 'location' && input.focus()}
          onFocus={() => { setFocused('location') }}
          label='URL'
          {...submitOnEnter}
        />}

        {<TextField
          sx={styles.inputField}
          variant="outlined"
          margin="dense"
          size='small'
          value={inputValue.description}
          onChange={(ev: any) => {
            setInputValue({ ...inputValue, description: ev.target.value })
          }}
          inputRef={(input: any) => input && focused === 'description' && input.focus()}
          onFocus={() => { setFocused('description') }}
          label="Description"
          {...submitOnEnter}
        />}

        <SearchInput
          getOptions={useOptions}
          value={inputValue.tags}
          label="Tags"
          hideIcons={true}
          disabledTypes={[SearchOptionType.EXCLUSION, SearchOptionType.WILDCARD]}
          setValue={(value: EntrySearchInput[]) => {
            setItemTags(value)
          }}
          inputSx={{
            'mt': '10px',
            '& .MuiInputBase-input': {
              fontSize: 16,
            }
          }}
          inputProps={{
            inputRef: (input: any) => input && focused === 'tags' && input.focus(),
            onFocus: () => { setFocused('tags') }
          }}
        />

        <Typography variant="body1" color="textPrimary" component="span">Recently used tags:</Typography>
        {displayedRecentTags && displayedRecentTags.length !== 0 && <StyledUL sx={styles.tagList}>
          {displayedRecentTags.map((tag: string) => (
            <li key={tag}>
              <Chip
                label={tag}
                size="small"
                onClick={() => !inputValue.tags.includes(tag) && onRecentTagClicked(tag)}
                onDelete={() => onRecentTagDelete(tag)}
                sx={inputValue.tags.includes(tag) ? styles.tagChipHighlighted : styles.tagChip}
              />
            </li>
          ))}
        </StyledUL>}

        <Box sx={{ flexGrow: 1 }} />

        {hasItem && <Box sx={{ padding: '8px 0 0 0' }}>
          <Typography variant="body2" color="textSecondary" component="span">
            Created: {moment(value.created_at).format('Do MMMM YYYY, h:mm:ss a')}
          </Typography>
          <br />
          <Typography variant="body2" color="textSecondary" component="span">
            Updated: {moment(value.updated_at).format('Do MMMM YYYY, h:mm:ss a')}
          </Typography>
        </Box>}
      </FormGroup>
    </TabPanel>
  )
}
