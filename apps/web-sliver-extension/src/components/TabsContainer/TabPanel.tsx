import React from 'react'
import {
  Box} from '@mui/material'


export const TabPanel = ({ children, index, isOpen, ...other }: {
  children?: React.ReactNode
  index: number
  isOpen: boolean,
}) => {

  return (
    <Box
      role="tabpanel"
      hidden={!isOpen}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {isOpen && (
        <Box sx={{ p: '10px 0', height: '100%' }}>
          {children}
        </Box>
      )}
    </Box>
  )
}
