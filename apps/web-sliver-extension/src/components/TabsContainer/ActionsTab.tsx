import React from 'react'
import {
  Bookmark as BookmarkIcon, Bookmarks as BookmarksIcon,
} from "@mui/icons-material"
import {
  Divider, ListItemIcon, MenuItem, MenuList} from '@mui/material'
  import browser, { Tabs as BrowserTabs } from 'webextension-polyfill'

import { SearchInput, SearchOptionType } from 'web-sliver-ui-components'

import { TabPanel } from 'components/TabsContainer/TabPanel'

import { useAuthToken, useMatchingTags, useStorage, useTabsStorage } from 'hooks'
import { saveTabs } from 'browser/service'


const selectionOptions = {
  BEFORE_CURRENT: 'before_current',
  AFTER_CURRENT: 'after_current',
  SELECTED: 'selected',
  ALL_WINDOW: 'all_window',
  ALL_BROWSER: 'all_browser',
}

const getActions = (getCount: Function) => {
  let actions: any[] = [
    {
      option: selectionOptions.BEFORE_CURRENT,
      label: `Save tabs before current tab (${getCount('beforeCurrent')})`
    },
    {
      option: selectionOptions.AFTER_CURRENT,
      label: `Save tabs after current tab (${getCount('afterCurrent')})`
    },
    {
      option: selectionOptions.SELECTED,
      label: `Save selected tabs (${getCount('selected')})`
    },
    {
      option: selectionOptions.ALL_WINDOW,
      label: `Save all tabs in this window (${getCount('inWindow')})`
    },
    {
      option: selectionOptions.ALL_BROWSER,
      label: `Save all tabs in all windows (${getCount('all')})`
    },
  ]

  return actions
}

export const ActionsTab = ({
  isOpen = false,
  selectedWorkspaceId,
  setIsSaving,
  tags,
  setTags,
}: {
  isOpen: boolean,
  selectedWorkspaceId: string,
  setIsSaving: Function,
  tags: any[],
  setTags: (value: EntrySearchInput[]) => any,
}) => {
  const { token, isAuthenticated } = useAuthToken()
  const { getTabsByKey, getCount, } = useTabsStorage()
  const useOptions = (query: string) => (useMatchingTags({
    workspaceId: selectedWorkspaceId || '',
    initialInput: query ? `.*${query}.*` : '',
    token,
  }).options || []).map((tag: TagType) => ({ name: tag.name, type: SearchOptionType.TAG, uses: tag.uses }))
  const { value: config } = useStorage({ key: 'config' })
  const { apiUrl: host } = config || {}

  const actions = getActions(getCount)

  const getOptionTabs = (option: string): BrowserTabs.Tab[] => {
    switch (option) {
      case selectionOptions.BEFORE_CURRENT:
        return getTabsByKey('beforeCurrent')

      case selectionOptions.AFTER_CURRENT:
        return getTabsByKey('afterCurrent')

      case selectionOptions.SELECTED:
        return getTabsByKey('selected')

      case selectionOptions.ALL_WINDOW:
        return getTabsByKey('inWindow')

      case selectionOptions.ALL_BROWSER:
        return getTabsByKey('all')
    }
    return []
  }

  const onSave = async (option: string) => {

    if (!isAuthenticated) {
      return
    }

    const tabs = getOptionTabs(option)
    setIsSaving(true)

    await saveTabs({
      workspaceId: selectedWorkspaceId,
      tabs: tabs.map(tab => ({
        ...tab,
        title: tab.title || '',
        location: tab.url || '',
        description: '',
      })),
      tags,
      doUpsert: true,
      host,
      token
    })
    // }
    setIsSaving(false)

    await browser.notifications.clear('websliver-saved')
    await browser.notifications.create(
      'websliver-saved',
      {
        type: 'basic',
        title: 'WebSliver - Save',
        message: `Saved ${tabs.length} entries.`
      })
  }

  return (
    <TabPanel isOpen={isOpen} index={1}>
      <MenuList>
        {actions.map((action: any) => (
          <MenuItem
            key={action.option}
            onClick={(_event: any) => { onSave(action.option) }}
            value={action.option}
          >
            <ListItemIcon>
              {getOptionTabs(action.option).length <= 1 ? <BookmarkIcon /> : <BookmarksIcon />}
            </ListItemIcon>

            {action.label}
          </MenuItem>
        ))}
      </MenuList>

      <Divider variant='middle' flexItem sx={{ margin: '10px 0' }} />

      <SearchInput
        getOptions={useOptions}
        value={tags}
        inputProps={{ autoFocus: true }}
        label="Tags"
        hideIcons={true}
        disabledTypes={[SearchOptionType.EXCLUSION, SearchOptionType.WILDCARD]}
        setValue={setTags}
        inputSx={{
          'mt': '10px',
          '& .MuiInputBase-input': {
            fontSize: 16,
          }
        }}
      />

    </TabPanel>
  )
}
