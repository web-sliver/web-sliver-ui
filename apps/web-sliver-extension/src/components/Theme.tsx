import { createTheme } from "@mui/material/styles"


const theme = createTheme({
    palette: {
      mode: 'dark',
    },
    typography: {
      fontFamily: [
        'Segoe UI',
        'Roboto',
        'Helvetica',
        'Arial',
        'sans-serif'
      ].join(', '),
    },
    components: {
      MuiCssBaseline: {
        styleOverrides: {
          body: {
            padding: '0px !important'
          }
        }
      }
    }
})

export default theme
