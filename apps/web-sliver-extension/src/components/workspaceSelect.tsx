import React from 'react'
import {
  FormControl, InputLabel, MenuItem, Select, SxProps} from '@mui/material'


export const WorkspaceSelect = ({
  selectedWorkspaceId,
  workspaces = [],
  setSelectedWorkspace,
  sx = {},
}: {
  selectedWorkspaceId: string,
  workspaces: any[],
  setSelectedWorkspace: Function,
  sx?: SxProps,
}) => {
  return (
    <FormControl sx={sx}>
      <InputLabel id="workspace-select-label">Workspace</InputLabel>
      <Select
        margin='dense'
        size='small'
        labelId='workspace-select-label'
        label='Workspace'
        variant='outlined'
        value={selectedWorkspaceId}
      >
        {workspaces.map((workspace: any) => (
          <MenuItem
            key={workspace.uid}
            selected={workspace.uid === selectedWorkspaceId}
            onClick={(event: any) => setSelectedWorkspace(event, workspace)}
            value={workspace.uid}
          >
            {`${workspace.name} - ${workspace.entriesCount} entries`}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}
