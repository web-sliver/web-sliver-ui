import React from 'react'
import {
  Settings as SettingsIcon, Delete as DeleteIcon, Save as SaveIcon,
  Logout as LogoutIcon, OpenInNew as OpenInNewIcon, Home as HomeIcon,
} from "@mui/icons-material"
import {
  Badge,
  Box, Divider, IconButton, Tooltip
} from '@mui/material'

import browser from 'webextension-polyfill'


import { useAuthToken, useStorage, useWorkspaces } from 'hooks'
import { setTokenStorage } from 'browser/auth'
import { WorkspaceSelect } from './workspaceSelect'


export const AppToolbar = ({
  onDelete,
  onSave,
  showUnsavedIndicator = false,
  showDeleteButton = false,
  showSaveButton = false,
  selectedWorkspaceId,
  selectWorkspace,
}: {
  onDelete: Function,
  onSave: Function,
  showUnsavedIndicator: boolean,
  showDeleteButton: boolean,
  showSaveButton: boolean,
  selectedWorkspaceId: string,
  selectWorkspace: Function,
}) => {
  const { token, isAuthenticated } = useAuthToken()
  const { value: config } = useStorage({ key: 'config' })
  const { appUrl } = config || {}
  const { workspaces, isLoading: workspacesLoading } = useWorkspaces({ token })

  const onLogout = async () => {
    await setTokenStorage(null)
    window.close()
  }

  const onOpenHome = async () => {
    await browser.tabs.create({ url: appUrl })
    window.close()
  }

  const onLogin = async () => {
    await browser.tabs.create({ url: appUrl })
    window.close()
  }

  const onSettings = async () => {
    await browser.tabs.create({ url: browser.runtime.getURL('options.html') })
    window.close()
  }

  const onOpenInTab = async () => {
    await browser.tabs.create({ url: browser.runtime.getURL('popup.html') })
    window.close()
  }

  return (
    <Box sx={{
      display: 'flex',
      justifyContent: 'flex-end',
      gap: '5px'
    }}>
      {!workspacesLoading ? <WorkspaceSelect
        selectedWorkspaceId={selectedWorkspaceId}
        workspaces={workspaces}
        setSelectedWorkspace={selectWorkspace}
        sx={{ flexGrow: '1' }}
      /> : <Box sx={{ flexGrow: '1' }} />}

      <Tooltip title='Save Item'>
        <Badge color="secondary" variant="dot" invisible={!showUnsavedIndicator}>
          <IconButton
            edge="end"
            size="small"
            aria-label="save"
            disabled={!showSaveButton}
            onClick={() => onSave()}
          >
            <SaveIcon />
          </IconButton>
        </Badge>
      </Tooltip>

      <Divider variant='middle' orientation='vertical' flexItem sx={{ margin: '10px 0' }} />

      <Tooltip title='Delete entry'>
        <IconButton
          edge="start"
          size="small"
          color="error"
          aria-label="delete"
          disabled={!showDeleteButton}
          onClick={() => onDelete()}
        >
          <DeleteIcon />
        </IconButton>
      </Tooltip>

      <Tooltip title='Open this screen in a new tab'>
        <IconButton edge="end" size="small" aria-label="open this screen in a new tab" onClick={onOpenInTab}>
          <OpenInNewIcon />
        </IconButton>
      </Tooltip>

      <Tooltip title='Extension settings'>
        <IconButton edge="end" size="small" aria-label="settings" onClick={onSettings}>
          <SettingsIcon />
        </IconButton>
      </Tooltip>

      <Tooltip title='Open Site'>
        <IconButton edge="end" size="small" aria-label="open site" onClick={onOpenHome}>
          <HomeIcon />
        </IconButton>
      </Tooltip>

      <Divider variant='middle' orientation='vertical' flexItem sx={{ margin: '10px 0' }} />

      <Tooltip title={isAuthenticated ? 'Log out (navigating to the site will re-login)' : 'Log in through the site'}>
        <IconButton
          edge="end"
          size="small"
          color={isAuthenticated ? 'error' : 'default'}
          aria-label={isAuthenticated ? 'log out' : 'log in'}
          onClick={isAuthenticated ? onLogout : onLogin}
        >
          <LogoutIcon />
        </IconButton>
      </Tooltip>
    </Box>
  )
}
