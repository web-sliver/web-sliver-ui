export const MENU_ENTRIES = {
    CONTEXT: 'websliver-main',
    CONTEXT_ADD_SINGLE: 'websliver-add-single-tab',
    CONTEXT_ADD_SELECTED: 'websliver-add-selected-tabs',
    CONTEXT_ADD_ALL_WINDOW: 'websliver-add-all-window',
    CONTEXT_ADD_ALL_BROWSER: 'websliver-add-all-browser'
  }
