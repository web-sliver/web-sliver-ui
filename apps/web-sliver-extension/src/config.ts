// icons from the hawcons collection by Yannick Lung
// url: https://www.iconfinder.com/iconsets/hawcons

// @ts-ignore
import offImage from './assets/icons/bookmark_off.svg'
// @ts-ignore
import onImage from './assets/icons/bookmark_on.svg'


export const settings = {
  WEBSLIVER_APP_URL: process.env.WEBSLIVER_APP_URL || 'NO VALUE PROVIDED',
  WEBSLIVER_API_URL: process.env.WEBSLIVER_API_URL || 'NO VALUE PROVIDED',
  WEBSLIVER_AUTH_CLIENT_ID: process.env.WEBSLIVER_AUTH_CLIENT_ID || 'NO VALUE PROVIDED',
  WEBSLIVER_DEBUG: process.env.WEBSLIVER_DEBUG === 'true',
}

export const icons = {
  SAVED: onImage,
  NOT_SAVED: offImage,
}
