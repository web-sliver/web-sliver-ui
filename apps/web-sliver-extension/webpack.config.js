const webpack = require("webpack")
const path = require("path")
const CopyPlugin = require("copy-webpack-plugin")
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin')


const sourceLocations = [
  path.resolve(__dirname, 'src'),
  path.resolve(__dirname, '../../packages')
]
const buildConfig = (env, argv) => {
  const isDev = argv.mode === 'development'
  const mode = argv.mode || 'development'

  return {
    devtool: isDev ? "source-map" : undefined,
    entry: {
      popup: path.join(__dirname, "src/pages/popup.tsx"),
      options: path.join(__dirname, "src/pages/options.tsx"),
      content: path.join(__dirname, "src/browser/content.ts"),
      background: path.join(__dirname, "src/browser/background.ts"),
    },
    output: { path: path.join(__dirname, isDev ? "build" : "dist"), filename: "[name].js" },
    module: {
      rules: [
        // {
        //   test: /\.(js|jsx)$/,
        //   use: "babel-loader",
        //   include: sourceLocations,
        // },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
          include: sourceLocations,
          exclude: /\.module\.css$/,
        },
        {
          test: /\.ts(x)?$/,
          loader: "ts-loader",
          options: {
            transpileOnly: true,
          },
          include: sourceLocations,
        },
        // {
        //   test: /\.css$/,
        //   use: [
        //     "style-loader",
        //     {
        //       loader: "css-loader",
        //       options: {
        //         importLoaders: 1,
        //         modules: true,
        //       },
        //     },
        //   ],
        //   include: /\.module\.css$/,
        // },
        {
          test: /\.svg$/,
          use: "file-loader",
          include: sourceLocations,
        },
        // {
        //   test: /\.png$/,
        //   use: [
        //     {
        //       loader: "url-loader",
        //       options: {
        //         mimetype: "image/png",
        //       },
        //     },
        //   ],
        //   include: path.resolve(__dirname, 'src'),
        // },
      ],
    },
    resolve: {
      extensions: [".js", ".jsx", ".tsx", ".ts"],
      fallback: {
        "assert": require.resolve("assert/"),
        "buffer": require.resolve('buffer/'),
        "stream": require.resolve('stream-browserify'),
        "crypto": require.resolve('crypto-browserify'),
      },
      plugins: [
        new TsconfigPathsPlugin(),
      ],
      // alias: {
      //   "react-dom": "@hot-loader/react-dom",
      // },
    },
    devServer: {
      contentBase: "./build",
    },
    plugins: [
      new CopyPlugin({
        patterns: [
          { from: "src/assets", to: "assets" },
          { from: "src/_locales", to: "_locales" },
          { from: "src/manifest.json", to: "manifest.json" },
          { from: "*.html", to: ".", context: "src/pages" },
        ],
      }),
      new webpack.ProvidePlugin({
        process: 'process/browser',
        Buffer: ['buffer', 'Buffer'],
      }),
      new webpack.EnvironmentPlugin([
        'WEBSLIVER_APP_URL',
        'WEBSLIVER_API_URL',
        'WEBSLIVER_AUTH_CLIENT_ID',
        'WEBSLIVER_DEBUG',
      ])
    ],
    optimization: {
      // todo: make this work with react and separate by material, react and everything else
      splitChunks: {
        // chunks: 'all',
        cacheGroups: {
          // react: {
          //   test: /node_modules\/(react.*\/).*/,
          //   name: "react",
          //   chunks: "initial",
          // },
          // material: {
          //   test: /node_modules\/(\@mui\/).*/,
          //   name: "mui",
          //   chunks: "all",
          // }
        }
      }
    },
  }
}

module.exports = buildConfig
