const { getLoader, loaderByName } = require('@craco/craco')
const path = require('path')
const webpack = require("webpack")


const packages = []
packages.push(path.join(__dirname, '../../packages'))


module.exports = {
  webpack: {
    alias: {
      '~': path.resolve(__dirname, 'src'),
    },
    configure: (webpackConfig, arg) => {
      const { isFound, match } = getLoader(webpackConfig, loaderByName('babel-loader'))

      webpackConfig.resolve.fallback = {
        stream: require.resolve('stream-browserify'),
        crypto: require.resolve('crypto-browserify'),
        buffer: require.resolve('buffer/'),
      }

      webpackConfig.plugins = [
        ...(webpackConfig.plugins || []),
        ...[
          new webpack.ProvidePlugin({
            Buffer: ["buffer", "Buffer"],
            process: "process/browser",
          }),
        ]
      ]

      if (!isFound) {
        return webpackConfig
      }

      const include = Array.isArray(match.loader.include)
        ? match.loader.include : [match.loader.include]

      match.loader.include = include.concat(packages)

      return webpackConfig

    }
  },

  babel: {
    presets: [
      ["@babel/preset-react", {"runtime": "automatic"}]
    ],
    plugins: [],
    loaderOptions: (babelLoaderOptions, { env, paths }) => {
      return babelLoaderOptions;
    },
  },
}
