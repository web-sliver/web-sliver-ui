import Layout from "~/components/layout"
import Redirect from "~/components/redirect"


export const ErrorPage = () => {
  return (
    <Layout subTitle='Redirecting ..'>
      <Redirect />
    </Layout>
  )
}
