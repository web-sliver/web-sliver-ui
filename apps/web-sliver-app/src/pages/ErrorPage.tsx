import Layout from "~/components/layout"
import Error from "~/components/error"


export const ErrorPage = () => {
  return (
    <Layout subTitle='Error'>
      <Error />
    </Layout>
  )
}
