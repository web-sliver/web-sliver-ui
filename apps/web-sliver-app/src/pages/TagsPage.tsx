import Layout from "~/components/layout"
import TagsLister from "~/components/listers/tagsLister"


export const TagsPage = () => {
  return (
    <Layout subTitle='Tags'>
      <TagsLister />
    </Layout>
  )
}
