import Layout from "~/components/layout"
import EntriesLister from "~/components/listers/EntriesLister"

export const EntriesPage = () => {
  return (
    <Layout subTitle='Entries'>
      <EntriesLister />
    </Layout>
  )
}
