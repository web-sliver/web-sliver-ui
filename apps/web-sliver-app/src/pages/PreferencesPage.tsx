import Layout from "~/components/layout"
import Preferences from "~/components/preferences"


export const PreferencesPage = () => {
  return (
    <Layout subTitle='Preferences'>
      <Preferences />
    </Layout>
  )
}
