import React from "react"
import { getToken } from "~/services/auth"


export const useAuthToken = () => {
  const [ token, setToken ] = React.useState<string>(getToken())

  return {
    isAuthenticated: !!token,
    token,
    setToken
  }
}
