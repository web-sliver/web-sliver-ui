import { BrowserRouter } from 'react-router-dom'

import { Auth0ProviderWithRedirectCallback } from './auth0Provider'
import { LocationWrapper } from './components/LocationWrapper'
import { settings } from './config'


function App() {
  return (
    <BrowserRouter>
      <Auth0ProviderWithRedirectCallback
        domain={settings.WEBSLIVER_AUTH_DOMAIN || 'no auth domain configured'}
        clientId={settings.WEBSLIVER_AUTH_CLIENT_ID || 'no auth client id configured'}
        redirectUri={window.location.origin}
        // not ideal, but necessary to avoid the 1000 token limit, for now
        // this has the disadvantage of making SSO harder. potentially insecure
        cacheLocation="localstorage"
      >
        <LocationWrapper />
      </Auth0ProviderWithRedirectCallback>
    </BrowserRouter>
  )
}

export default App
