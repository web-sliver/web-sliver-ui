import PropTypes from "prop-types"
import React from "react"
import { Typography, AppBar, Toolbar, Divider, IconButton, Box } from "@mui/material"
import { Menu as MenuIcon } from '@mui/icons-material'
import { useAuth0 } from '@auth0/auth0-react'

import SEO from "~/components/seo"
import { LocationContext } from './contexts/locationContext'
import { useWorkspaces } from '~/services/swr_hooks'
import { StyledLink } from 'web-sliver-ui-components'
import { getQueryParamsString } from "./contexts/locationContextUtils"
import { WorkspaceManageDialog } from "./dialogs/workspaceManageDialog"
import { AppMenu } from "./appMenu"

const styles = {
  grow: {
    flexGrow: 1,
  },

  loginText: {
    padding: "0 10px",
  }
}


const Header = ({ siteTitle, subTitle }: {
  siteTitle: string,
  subTitle: string,
}) => {

  const { user, isAuthenticated, isLoading } = useAuth0()

  const [appMenuOpen, setAppMenuOpen] = React.useState<boolean>(false)
  const [workspacesDialogOpen, setWorkspacesDialogOpen] = React.useState<boolean>(false)
  const [fullSubTitle, setFullSubTitle] = React.useState(subTitle)
  const [selectedWorkspace, setSelectedWorkspace] = React.useState<any>(null)
  const { queryParams, navigateQuery } = React.useContext(LocationContext)
  const { data: workspaces, isLoading: workspacesLoading, mutate: mutateWorkspaces, defaultWorkspace } = useWorkspaces()

  const onWorkspaceSelect = (workspace: any) => {
    setWorkspacesDialogOpen(false)
    navigateQuery({ workspaceId: workspace.uid, page: 0 })
  }

  const WorkspacesDialog = () => <WorkspaceManageDialog
    isOpen={workspacesDialogOpen}
    workspaces={workspaces}
    refreshWorkspaces={mutateWorkspaces}
    onSelect={onWorkspaceSelect}
    onClose={() => setWorkspacesDialogOpen(false)}
  />

  React.useEffect(() => {
    if (workspacesLoading) {
      return
    }

    let titleParts = []

    if (subTitle) {
      titleParts.push(subTitle)
    }

    const workspace = workspaces.find((item: any) => item.uid === queryParams.workspaceId)
    const workspaceName = workspace?.name

    if (workspaceName) {
      titleParts.push(workspaceName)
    }

    if (workspace) {
      setSelectedWorkspace(workspace)
    } else {
      setSelectedWorkspace(defaultWorkspace)
    }

    const params = [...queryParams.tags || [], ...queryParams.query || []]
    if (params.length !== 0) {
      titleParts.push(params.join(', '))
    }

    setFullSubTitle(titleParts.join(' | '))
  }, [workspacesLoading, workspaces, queryParams, defaultWorkspace, subTitle])

  return (
    <React.Fragment>
      <SEO title={fullSubTitle} />
      <AppBar position="static">
        <Toolbar variant="dense" sx={{ gap: 1 }}>
          <Typography variant="h6" color="primary">
            <StyledLink
              to={`/${getQueryParamsString({ workspaceId: queryParams.workspaceId })}`}
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
            >
              {siteTitle}
            </StyledLink>
          </Typography>
          <Divider orientation="vertical" light flexItem variant="middle" />

          <Typography variant="h6">
            {selectedWorkspace?.name ? [subTitle, selectedWorkspace?.name].join(' - ') : subTitle}
          </Typography>

          <Box sx={styles.grow} />

          <Typography sx={styles.loginText} color="textSecondary">
            {isLoading ? 'Loading..' : isAuthenticated ? `${user?.name || user?.email || 'Unnamed'}` : 'Not logged in'}
          </Typography>

          <IconButton
            edge="end"
            color="inherit"
            aria-label="menu"
            onClick={() => setAppMenuOpen(true)}
          >
            <MenuIcon />
          </IconButton>

          {!workspacesLoading && <WorkspacesDialog />}

          <AppMenu isOpen={appMenuOpen} onClose={() => setAppMenuOpen(false)} />
        </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
  subTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
  subTitle: ``,
}

export default Header
