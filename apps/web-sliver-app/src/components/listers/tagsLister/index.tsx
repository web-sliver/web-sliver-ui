import React from 'react'
import { useAuth0 } from '@auth0/auth0-react'

import * as dataService from 'web-sliver-data-interface'

import { LocationContext, useLocationPaging, useLocationSearchInput, useLocationSorting, useLocationWorkspaceId } from '~/components/contexts/locationContext'
import BaseLister from '../baseLister'
import { useMatchingTags, useMatchingTagsCount, useWorkspaces } from '~/services/swr_hooks'
import { boundsConverter, DEFAULT_PAGE_SIZE, determineSortingDirection } from '~/utils'
import { useAuthToken } from '~/hooks/authTokenHook'
import { SelectionContext, useSelectionContext } from '../baseLister/selectionContext'
import { styled } from '@mui/material/styles'
import { settings } from '~/config'
import { useOkCancelConfirmationPopover } from '~/components/dialogs/confirmationPopover'
import { TagDialog } from '~/components/dialogs/tagDialog'
import { EntrySearchInput } from 'web-sliver-ui-components'


const RootDiv = styled('div')({
  width: '100%'
})

const headerFields = [
  { id: 'name', numeric: false, disablePadding: false, label: 'Name' },
]

const TagsLister = () => {
  const { isAuthenticated } = useAuth0()
  const { token } = useAuthToken()

  const [editDialogOpen, setEditDialogOpen] = React.useState<boolean>(false)

  const selectionContext = useSelectionContext()
  const { selection, deselectAll } = selectionContext

  const { defaultWorkspace, isLoading: workspacesLoading } = useWorkspaces()

  const { navigateQuery, queryParams } = React.useContext(LocationContext)
  const { workspaceId } = useLocationWorkspaceId(queryParams)
  const { searchInput, setSearchInputState } = useLocationSearchInput(queryParams)
  const { paging } = useLocationPaging(queryParams)
  const { sorting } = useLocationSorting(queryParams, 'name')
  const host = settings.WEBSLIVER_API_URL

  const [ocConfirm, setOCConfirm] = React.useState<{ action: Function, message: string }>({ action: () => { }, message: '' })

  const { data: total, mutate: mutateTagsCount } = useMatchingTagsCount({
    workspaceId: workspaceId || '',
    value: searchInput.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name).join('.*'),
  })

  const { offset, limit } = boundsConverter({ total, ...paging })
  const { data: items, mutate: mutateItems } = useMatchingTags({
    workspaceId: workspaceId || '',
    value: searchInput.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name).join('.*'),
    offset,
    limit,
    sortField: queryParams.sortField,
    sortDirection: queryParams.sortDirection,
  })

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []

  const { component: OCConfirm, openFor: openOCConfirm } = useOkCancelConfirmationPopover({
    onOK: ocConfirm.action, options: [], message: ocConfirm.message,
  })

  const setSearchInput = async (input: EntrySearchInput[]) => {
    setSearchInputState(input)
    const tags = input.filter((tag: any) => typeof tag !== 'string').map((tag: any) => tag.name)
    const query = input.filter((item: any) => typeof item === 'string')

    await navigateQuery({ query, tags })
  }

  React.useEffect(() => {
    // set workspace if not set
    if (isAuthenticated && !workspacesLoading && !workspaceId && defaultWorkspace?.uid) {
      navigateQuery({ workspaceId: defaultWorkspace.uid })
    }
  }, [workspacesLoading, isAuthenticated, defaultWorkspace?.uid, navigateQuery, workspaceId])

  const handleDeleteMultiple = async (event: any, selection: any[]) => {
    const deleteAction = async () => {
      await dataService.replaceTags({ workspaceId, oldTags: selection, newTags: [], host, token })

      await mutateItems(items.filter(((tag: any) => !selection.includes(tag.name))))
      await mutateTagsCount(total - selection.length)

      deselectAll()
    }

    // perform action without confirmation
    if (!event) {
      deleteAction()
      return
    }

    setOCConfirm({
      message: 'Are you sure you want to delete the selected items?',
      action: deleteAction
    })

    openOCConfirm(event?.currentTarget)
  }

  const handleDeleteOne = async (item: any) => {
    await handleDeleteMultiple(undefined, [item.name])
  }

  const handleChangePage = async (page: number) => {
    await navigateQuery({ page })
  }

  const handleChangePageSize = async (pageSize: number) => {
    await navigateQuery({ page: 0, pageSize })
  }

  const handleSorting = async (property: string) => {
    await navigateQuery({
      sortField: property,
      sortDirection: determineSortingDirection(sorting.sortDirection, sorting.sortField !== property)
    })

    await mutateItems()
  }

  const handleSaveTag = async ({ oldTag, newTag }: any) => {
    await dataService.replaceTags({ workspaceId, oldTags: [oldTag.name], newTags: [newTag.name], host, token })

    await mutateItems()
  }

  const onDeleteSelected = (event: any) => {
    handleDeleteMultiple(event, selection)
  }

  const [editedItem, setEditedItem] = React.useState()

  const EditTagDialog = () => <TagDialog
    isOpen={editDialogOpen}
    item={editedItem}
    onSave={(item: any) => { handleSaveTag(item); setEditDialogOpen(false) }}
    onDelete={(item: any) => { handleDeleteOne(item); setEditDialogOpen(false) }}
    onClose={() => setEditDialogOpen(false)}
  />

  return (
    <RootDiv>

      {isAuthenticated && <SelectionContext.Provider value={selectionContext}>
        <BaseLister
          items={items.map((item: any) => ({
            ...item,
            uid: item.name,
            title: `${item.name}`,
            description: `${item.uses} entries`,
            // todo: parametrize this further
            location: `/?dir=asc&ps=${DEFAULT_PAGE_SIZE}&sort=title&ws=${workspaceId}&tag=${item?.name}`
          }))}
          descriptionInTooltip={false}
          headerFields={headerFields}
          searchInput={searchInput}
          total={total}
          page={paging.page}
          pageSize={paging.pageSize}
          sorting={sorting}
          getOptions={useOptions}
          hideLocationSubtext={true}
          hideSelectAll={true}
          onSaveOne={(item: any) => { setEditedItem(item); setEditDialogOpen(true) }}
          onDeleteSelected={onDeleteSelected}
          onTagClick={() => null}
          onSearchInputChange={setSearchInput}
          onChangePage={handleChangePage}
          onChangePageSize={handleChangePageSize}
          onChangeSorting={handleSorting}
        />
      </SelectionContext.Provider>}

      <EditTagDialog />
      <OCConfirm />
    </RootDiv>
  )
}

export default TagsLister
