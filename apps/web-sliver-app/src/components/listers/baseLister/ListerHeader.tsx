import React from 'react'
import { SelectionContext } from './selectionContext'
import { TableCell, TableHead, TableRow, TableSortLabel, Checkbox, SortDirection } from '@mui/material'

import { Span } from 'web-sliver-ui-components'

import { toggleDirection } from '~/utils'


const ListerHeader = ({ items, styles, headerFields, sorting, onSort, rowCount }: {
  items: any[],
  styles: any,
  headerFields: any[],
  sorting: any,
  onSort: Function,
  rowCount: number,
}) => {
  const { selection, selectMultiple, deselectAll, allSelected } = React.useContext(SelectionContext)

  const handleSorting = (property: string) => (_event: React.MouseEvent) => {
    onSort(property)
  }

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            indeterminate={selection.length > 0 && selection.length < rowCount}
            checked={rowCount > 0 && (selection.length === rowCount || allSelected)}
            onChange={(event: any) => event.target.checked ? selectMultiple(items) : deselectAll()}
            inputProps={{ 'aria-label': 'select all items' }}
          />
        </TableCell>
        {headerFields.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={sorting.sortField === headCell.id ? sorting.sortDirection as SortDirection : undefined}
          >
            <TableSortLabel
              active={sorting.sortField === headCell.id}
              direction={sorting.sortField === headCell.id ? toggleDirection(sorting.sortDirection) : 'desc'}
              onClick={handleSorting(headCell.id)}
            >
              {headCell.label}
              {sorting.sortField === headCell.id ? (
                <Span sx={styles.visuallyHidden}>
                  {sorting.sortDirection === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}

export default ListerHeader
