import React from 'react'
import {
  Toolbar, Typography, Tooltip, IconButton, Box, Divider, Menu, MenuItem
} from '@mui/material'
import { Delete, Close, Add, FileCopy as Copy, LocalOffer as Tag, OpenWith as Move, DoneAll as SelectAll, Extension } from '@mui/icons-material'

import { SelectionContext } from './selectionContext'
import { useWorkspaces } from '~/services/swr_hooks'
import styles from './styles'


const SelectedToolbar = ({
  total,
  onDelete,
  onCopy,
  onMove,
  onTag,
  hideSelectAll = false,
  extensionMenuOptions = [],
}: {
  total: number,
  onDelete?: (event: any) => any,
  onCopy?: (event: any) => any,
  onMove?: (event: any) => any,
  onTag?: (event: any) => any,
  hideSelectAll?: boolean,
  extensionMenuOptions: { label: string, onClick: () => any }[],
}) => {

  const selectionContext = React.useContext(SelectionContext)
  const { selection, allSelected, deselectAll, setAllSelected } = selectionContext
  const { isLoading: workspacesLoading } = useWorkspaces()

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleExtensionMenuClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleExtensionMenuClose = () => {
    setAnchorEl(null);
  };

  const disableTagging = onTag === undefined
  const disableDuplicate = onCopy === undefined
  const disableMove = onMove === undefined
  const disableDelete = onDelete === undefined
  const disableExtensionMenu = extensionMenuOptions.length === 0

  // shown when selection length is > 0
  return (
    <Toolbar
      sx={{ ...styles.toolbarRoot, ...styles.toolbarHighlight, gap: 1 }}
    >

      <Tooltip title="Deselect all">
        <IconButton aria-label="deselect" onClick={deselectAll}>
          <Close />
        </IconButton>
      </Tooltip>

      <Typography color="inherit" variant="subtitle1" component="div">
        {allSelected ? total : selection.length} of {total} items selected
      </Typography>

      <Box sx={{ flexGrow: 1, flexShrink: 1 }} />

      <Box sx={{ display: 'flex' }}>
        {!disableExtensionMenu && <Tooltip title="Extension menu">
          <IconButton
          aria-label="extension-menu-button"
          aria-controls={open ? 'extension-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleExtensionMenuClick}
          >
            <Extension />
          </IconButton>
        </Tooltip>}

        {!disableExtensionMenu && <Menu
          id="extension-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleExtensionMenuClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          {extensionMenuOptions.map((option, index) => (<MenuItem key={index} onClick={() => {option.onClick(); handleExtensionMenuClose() }}>{option.label}</MenuItem>))}
        </Menu>}

        {!disableExtensionMenu && <Divider orientation="vertical" flexItem />}

        {!hideSelectAll && !allSelected &&
          <Tooltip title="Select all">
            <IconButton aria-label="deselect" onClick={() => setAllSelected(true)}>
              <SelectAll />
            </IconButton>
          </Tooltip>
        }

        {!disableDuplicate && !workspacesLoading && <Tooltip title="Copy to workspace">
          <IconButton aria-label="copy" onClick={onCopy}>
            <Copy />
          </IconButton>
        </Tooltip>}

        {!disableMove && !workspacesLoading && <Tooltip title="Move to workspace">
          <IconButton aria-label="move" onClick={onMove}>
            <Move />
          </IconButton>
        </Tooltip>}

        {!disableTagging && <Tooltip title="Add/remove tags">
          <IconButton aria-label="tags" onClick={onTag}>
            <Tag />
          </IconButton>
        </Tooltip>}

        {!disableDelete && <Tooltip title="Delete selected">
          <IconButton aria-label="delete" onClick={onDelete}>
            <Delete />
          </IconButton>
        </Tooltip>}
      </Box>
    </Toolbar>
  )
}


const BaseToolbar = ({
  hideSelectAll = false,
  onAdd,
}: {
  hideSelectAll?: boolean,
  onAdd?: ((item: any) => any), // can be false or a function
}) => {
  const selectionContext = React.useContext(SelectionContext)
  const { allSelected, setAllSelected } = selectionContext

  const disableAdding = onAdd === undefined

  // shown when selection is 0
  return (
    <Toolbar
      sx={styles.toolbarRoot}
    >
      <Typography variant="h6" id="tableTitle" component="div">
        Items
      </Typography>

      <Box sx={{ flexGrow: 1, flexShrink: 1 }} />

      {!hideSelectAll && !allSelected &&
        <Tooltip title="Select all">
          <IconButton
            aria-label="deselect"
            onClick={() => setAllSelected(true)}
          >
            <SelectAll />
          </IconButton>
        </Tooltip>
      }

      {!disableAdding &&
        <Tooltip title="Add">
          <IconButton
            aria-label="add"
            onClick={onAdd as any}
          >
            <Add />
          </IconButton>
        </Tooltip>
      }
    </Toolbar>
  )
}


const ListerToolbar = ({
  total = 0,
  showSelected = false,
  onDelete = undefined,
  onCopy = undefined,
  onMove = undefined,
  onTag = undefined,
  onAdd = undefined,
  hideSelectAll = false,
  extensionMenuOptions = [],
}: {
  total?: number,
  showSelected?: boolean,
  onDelete?: (event: any) => any,
  onCopy?: (event: any) => any,
  onMove?: (event: any) => any,
  onTag?: (event: any) => any,
  onAdd?: () => any,
  hideSelectAll?: boolean,
  extensionMenuOptions?: { label: string, onClick: () => any }[],
}) => {

  return (
    showSelected ?
      <SelectedToolbar
        total={total}
        onCopy={onCopy}
        onDelete={onDelete}
        onMove={onMove}
        onTag={onTag}
        hideSelectAll={hideSelectAll}
        extensionMenuOptions={extensionMenuOptions}
      /> :
      <BaseToolbar
        onAdd={onAdd}
        hideSelectAll={hideSelectAll}
      />
  )
}

export default ListerToolbar
