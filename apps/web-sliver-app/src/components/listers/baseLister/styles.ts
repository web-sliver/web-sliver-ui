
const styles = {
  listerRoot: {
    width: '100%',
    marginBottom: 2,
  },

  visuallyHidden: {
    border: 0,
    clip: 'rect(0 0 0 0)',
    height: 1,
    margin: -1,
    overflow: 'hidden',
    padding: 0,
    position: 'absolute',
    top: 20,
    width: 1,
  },

  cardHeader: {
    padding: '8px'
  },

  cardContent: {
    flex: '1 0 auto',
    padding: '8px',
    '&:last-child': {
      paddingBottom: '8px'
    }
  },

  tagList: {
    display: 'flex',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: 0,
    margin: 0,
    maxWidth: '85%'
  },

  tagChip: {
    margin: 0.3,
  },

  tagChipHighlighted: {
    margin: 0.3,
    backgroundColor: '#202020',
  },

  overflowHidden: {  // todo: use or delete
    width: '85vw',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    display: 'inline-block',
  },

  toolbarRoot: {
    paddingLeft: 1,
    paddingRight: 1,
  },

  toolbarHighlight: {
    color: 'primary',
    backgroundColor: 'secondary.dark',
  },

  tableCell: {
    border: 0,
    p: 1,
  },
}


export default styles
