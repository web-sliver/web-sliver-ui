import React from 'react'
import {
  Table, TableBody, TableCell, TableContainer, TablePagination,
  TableRow, Paper, Box
} from '@mui/material'
import { styled } from '@mui/material/styles'

import { EntrySearchInput, ListerSorting, SearchInput } from 'web-sliver-ui-components'
import { Entry } from 'web-sliver-data-interface'

import ListerHeader from './ListerHeader'
import ListerToolbar from './ListerToolbar'
import ItemCard from './ItemCard'
import { DEFAULT_PAGE_SIZE } from '~/utils'
import { SelectionContext } from './selectionContext'
import styles from './styles'


const RootDiv = styled('div')({
  width: '100%'
})


const Pagination = ({
  total = 0,
  page = 0,
  pageSize = DEFAULT_PAGE_SIZE,
  onChangePage = () => { },
  onChangePageSize = () => { },
}: {
  total: number,
  page: number,
  pageSize: number,
  onChangePage: (page: number) => any,
  onChangePageSize: (pageSize: number) => any,
}) => {

  pageSize = pageSize === 0 ? DEFAULT_PAGE_SIZE : pageSize

  const labelDisplayedRows = ({ from, to, count, page }: any) => {
    const entriesSubLabel = `${from} - ${to} of ${count !== -1 ? count : `many`} entries`
    const pageSubLabel = `${page + 1} of ${count !== -1 ? Math.ceil(count / pageSize) : 'many'}`

    return `Page ${pageSubLabel} (${entriesSubLabel})`
  }

  return (
    <TablePagination
      rowsPerPageOptions={[10, 25, 100]}
      component="div"
      count={total}
      rowsPerPage={pageSize}
      page={page}
      labelRowsPerPage='Page size:'
      showFirstButton={true}
      showLastButton={true}
      labelDisplayedRows={labelDisplayedRows}
      onPageChange={(_event, page) => onChangePage(page)}
      onRowsPerPageChange={(event) => onChangePageSize(parseInt(event.target.value) || DEFAULT_PAGE_SIZE)}
    />
  )
}

const BaseLister = ({
  items,
  headerFields = [],
  searchInput = [],
  total = 0,
  page = 0,
  pageSize = DEFAULT_PAGE_SIZE,
  sorting,
  getOptions = () => { },
  onAdd,
  onSaveOne = () => { },
  onTagClick = () => { },
  onDeleteSelected,
  onCopySelected,
  onMoveSelected,
  onTagSelected,
  onSearchInputChange = () => { },
  onChangePage = () => { },
  onChangePageSize = () => { },
  onChangeSorting = () => { },
  hideLocationSubtext = false,
  hideSelectAll = false,
  descriptionInTooltip = true,
  extensionMenuOptions = [],
}: {
  items: Entry[],
  headerFields: any[],
  searchInput: EntrySearchInput[],
  total: number,
  page: number,
  pageSize: number,
  sorting: ListerSorting,
  getOptions: (query: string) => any,
  hideLocationSubtext?: boolean,
  hideSelectAll?: boolean,
  descriptionInTooltip?: boolean,
  extensionMenuOptions?: { label: string, onClick: () => any }[],
  onAdd?: (() => any),
  onSaveOne: (item: Entry) => any,
  onTagSelected?: ((event: React.MouseEvent) => void),
  onDeleteSelected?: (event: React.MouseEvent) => any,
  onCopySelected?: (event: React.MouseEvent) => any,
  onMoveSelected?: (event: React.MouseEvent) => any,
  onTagClick: (tag: string) => any,
  onSearchInputChange: (input: EntrySearchInput[]) => any,
  onChangePage: (page: number) => any,
  onChangePageSize: (pageSize: number) => any,
  onChangeSorting: (sortField: string) => any,
}) => {
  const selectionContext = React.useContext(SelectionContext)
  const { selection, deselect, select, isSelected, hasSelection } = selectionContext

  const handleCellCheckboxClick = (item: any) =>
    selection.includes(item.uid) ? deselect(item.uid) : select(item.uid)

  return (
    <RootDiv>
      <Box sx={{ pb: 2, pt: 2 }}>
        <SearchInput
          getOptions={getOptions}
          value={searchInput}
          setValue={onSearchInputChange}
          inputSx={{
            '& .MuiInputBase-input': {
              fontSize: 20,
            }
          }}
        />
      </Box>

      <Paper sx={styles.listerRoot} elevation={1}>
        <ListerToolbar
          total={total}
          showSelected={hasSelection()}
          onDelete={onDeleteSelected}
          onCopy={onCopySelected}
          onMove={onMoveSelected}
          onAdd={onAdd}
          onTag={onTagSelected}
          hideSelectAll={hideSelectAll}
          extensionMenuOptions={extensionMenuOptions}
        />

        {total !== undefined && total !== 0 && (
          <Pagination
            total={total}
            page={page}
            pageSize={pageSize}
            onChangePage={onChangePage}
            onChangePageSize={onChangePageSize}
          />
        )}

        <TableContainer>
          <Table
            aria-labelledby="tableTitle"
            size="medium"
            aria-label="paged lister"
          >
            {items && items.length !== 0 && (<ListerHeader
              items={items.map((item: any) => item.uid)}
              styles={styles}
              headerFields={headerFields}
              sorting={sorting}
              onSort={onChangeSorting}
              rowCount={items.length}
            />)}

            <TableBody>
              {items.map((item: any, index: number) => {
                const labelId = `paged-lister-checkbox-${index}`

                return (
                  <TableRow
                    hover
                    tabIndex={-1}
                    key={item.uid}
                  >
                    <TableCell sx={styles.tableCell} component="th" id={labelId} scope="row" padding="none" colSpan={headerFields.length + 1}>
                      <ItemCard
                        item={item}
                        isSelected={isSelected(item.uid)}
                        highlights={searchInput}
                        hideLocationSubtext={hideLocationSubtext}
                        descriptionInTooltip={descriptionInTooltip}
                        onSelect={() => handleCellCheckboxClick(item)}
                        onSaveClicked={() => onSaveOne(item)}
                        onTagClicked={onTagClick}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>

        {total !== undefined && total !== 0 && (
          <Pagination
            total={total}
            page={page}
            pageSize={pageSize}
            onChangePage={onChangePage}
            onChangePageSize={onChangePageSize}
          />
        )}
      </Paper>
    </RootDiv>
  )
}

export default BaseLister
