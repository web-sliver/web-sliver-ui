import React from 'react'


export interface ISelection {
  selection: string[],
  allSelected: boolean,
  hasSelection: () => boolean,
  select: (id: string) => void,
  deselect: (id: string) => void,
  selectMultiple: (items: any[]) => void,
  deselectAll: () => void,
  setAllSelected: (value: boolean) => void,
  isSelected: (id: string) => boolean,
}

export const SelectionContext = React.createContext<ISelection>({
  selection: [],
  allSelected: false,
  hasSelection: () => false,
  select: () => { },
  deselect: () => { },
  selectMultiple: (_items: any[]) => { },
  deselectAll: () => { },
  setAllSelected: (_value: boolean) => { },
  isSelected: (_id: string) => false,
})


export const useSelectionContext = () => {
  const [selection, setSelection] = React.useState<string[]>([])
  const [allSelected, setAllSelected] = React.useState<boolean>(false)

  const selectMultiple = (items: any[]) => { setSelection(items) }

  const selectionContext = {
    selection,
    allSelected,
    hasSelection: () => allSelected || selection.length !== 0,
    setAllSelected: (value: boolean) => {
      if (value) {
        setSelection([])
      }
      setAllSelected(value)
    },
    select: (id: string) => {
      setSelection((new Array<string>()).concat(selection, id))
      setAllSelected(false)
    },
    deselect: (id: string) => {
      setSelection(selection.filter(item => item !== id))
      setAllSelected(false)
    },
    deselectAll: () => { setSelection([]); setAllSelected(false) },
    selectMultiple,
    isSelected: (id: string) => allSelected || selection.includes(id)
  }

  return selectionContext
}
