import React from 'react'
import {
  IconButton, Link, Typography, Chip, Box, Paper
} from '@mui/material'
import { Edit as EditIcon } from '@mui/icons-material'

import { EntrySearchInput, StyledLink, StyledTooltip, StyledUL, TagType } from 'web-sliver-ui-components'

import { dateFromNow, dateToString } from '~/utils'
import styles from './styles'


const ItemCard = ({ item, isSelected = false, highlights, onSaveClicked, onTagClicked, onSelect, hideLocationSubtext = false, descriptionInTooltip = true }: {
  item: any,
  isSelected: boolean,
  highlights?: EntrySearchInput[],
  hideLocationSubtext?: boolean,
  descriptionInTooltip?: boolean,
  onSaveClicked: () => void,
  onTagClicked: (tag: string) => void,
  onSelect: () => void,
}) => {
  const highlightedTags = highlights?.filter(item => typeof (item) !== 'string').map(item => (item as TagType).name) || []
  const title = item.title || (item.location ? `${item.location}` : '(no title)')

  const createInternalLink = (wrappedComponent: any) => (
    <StyledLink
      to={item.location}
      sx={{ color: 'inherit', textDecoration: 'none' }}
      rel="_noreferrer"
      onClick={(e: React.MouseEvent) => e.stopPropagation()}
    >
      {wrappedComponent}
    </StyledLink>
  )

  const createExternalLink = (wrappedComponent: any) => (
    <Link
      href={item.location}
      sx={{ color: 'inherit', textDecoration: 'none' }}
      rel="_noreferrer"
      onClick={(e: React.MouseEvent) => e.stopPropagation()}
    >
      {wrappedComponent}
    </Link>
  )

  const createLink = (wrappedComponent: any) => {

    return item.location.startsWith('/') ? createInternalLink(wrappedComponent) : createExternalLink(wrappedComponent)
  }

  const getTitle = () => (
    <Typography color="primary" sx={{ fontSize: 18, fontWeight: 'medium', pb: 0.5 }}>{title}</Typography>
  )

  const DescriptionTooltip = () => (
    <Typography variant="body1" color="textSecondary" component="span">
      <pre style={{ fontFamily: 'inherit', fontSize: 18 }}>
        {item.description}
      </pre>
    </Typography>
  )

  const TitleWrapper = ({ children }: any) => (
    descriptionInTooltip ?
      <StyledTooltip title={item.description &&
        <React.Fragment>
          <DescriptionTooltip />
        </React.Fragment>}
      >
        {children}
      </StyledTooltip>
      :
      <Box>{children}</Box>
  )

  return (
    <Paper
      sx={{
        display: 'flex',
        flexDirection: 'column',
        gap: 1,
        p: 1,
      }}
      elevation={isSelected ? 24 : 3}
      onClick={onSelect}
      onDragEnter={onSelect}
    >
      <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
        <Box>

          <TitleWrapper>
            {item.location ? createLink(getTitle()) : getTitle()}
          </TitleWrapper>

          {!hideLocationSubtext && item.location &&
            <Typography variant="body2" color="gray" component="span">
              {item.location}
            </Typography>
          }

          {!descriptionInTooltip && <DescriptionTooltip />}
        </Box>


        <IconButton aria-label="edit" onClick={onSaveClicked}>
          <EditIcon />
        </IconButton>
      </Box>

      <Box sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-end' }}>
        {item.tags && item.tags.length !== 0 ?
          <StyledUL sx={styles.tagList}>
            {item.tags.map((tag: string) => (
              <li key={tag}>
                <Chip
                  label={tag}
                  size="small"
                  onClick={() => onTagClicked(tag)}
                  sx={highlightedTags?.includes(tag) ? styles.tagChipHighlighted : styles.tagChip}
                />
              </li>
            ))}
          </StyledUL> : <Box></Box>
        }

        {item.created_at && <StyledTooltip title={
          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
            <Typography variant="body2" color="textSecondary">Created: {dateToString(item.created_at)}</Typography>
            <Typography variant="body2" color="textSecondary">Modified: {item.updated_at ? dateToString(item.updated_at) : 'never'}</Typography>
          </Box>
        }>

          <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'flex-end' }}>
            {item.created_at &&
              <Typography variant="body2" color="gray" component="span" sx={{ pr: 1 }}>Added {dateFromNow(item.created_at)}</Typography>
            }
            {item.updated_at &&
              <Typography variant="body2" color="gray" component="span" sx={{ pr: 1 }}>Updated {item.created_at !== item.updated_at ? dateFromNow(item.updated_at) : 'never'}</Typography>
            }
          </Box>
        </StyledTooltip>}
      </Box>
    </Paper>
  )
}

export default ItemCard
