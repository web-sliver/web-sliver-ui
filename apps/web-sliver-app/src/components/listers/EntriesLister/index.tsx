import React from 'react'

import { styled } from '@mui/material/styles'
import { Alert } from '@mui/material'
import { useAuth0 } from '@auth0/auth0-react'

import * as dataService from 'web-sliver-data-interface'

import { useAuthToken } from '~/hooks/authTokenHook'
import { useEntries, useEntriesCount, useMatchingTags, useWorkspaces } from '~/services/swr_hooks'
import { boundsConverter, determineSortingDirection } from '~/utils'
import { LocationContext, useLocationPaging, useLocationSearchInput, useLocationSorting, useLocationWorkspaceId } from '~/components/contexts/locationContext'
import BaseLister from '../baseLister'
import { SelectionContext, useSelectionContext } from '../baseLister/selectionContext'
import { settings } from '~/config'
import { useOkCancelConfirmationPopover, } from '~/components/dialogs/confirmationPopover'
import { EntryDialog } from '~/components/dialogs/EntryDialog'
import { WorkspaceManageDialog } from '~/components/dialogs/workspaceManageDialog'
import { TagsInputDialog } from '~/components/dialogs/TagsInputDialog'
import { FailedListDialog } from '~/components/dialogs/FailedListDialog'


const RootDiv = styled('div')({
  width: '100%'
})

const headerFields = [
  { id: 'title', numeric: false, disablePadding: false, label: 'Title' },
  { id: 'description', numeric: false, disablePadding: true, label: 'Description' },
  { id: 'location', numeric: false, disablePadding: true, label: 'Location' },
  { id: 'created_at', numeric: false, disablePadding: true, label: 'Created' },
  { id: 'updated_at', numeric: false, disablePadding: true, label: 'Updated' },
]

const EntriesLister = () => {

  const [addDialogOpen, setAddDialogOpen] = React.useState<boolean>(false)
  const [editDialogOpen, setEditDialogOpen] = React.useState<boolean>(false)
  const [workspaceMoveOpen, setWorkspaceMoveOpen] = React.useState<boolean>(false)
  const [workspaceCopyOpen, setWorkspaceCopyOpen] = React.useState<boolean>(false)
  const [addTagsDialogOpen, setAddTagsDialogOpen] = React.useState<boolean>(false)
  const [failedListDialogOpen, setFailedListDialogOpen] = React.useState<boolean>(false)
  const [editedItem, setEditedItem] = React.useState()

  const { isAuthenticated } = useAuth0()
  const { token } = useAuthToken()
  const windowAug = window as AugmentedWindow

  const queryContext = React.useContext(LocationContext)
  const { navigateQuery, queryParams } = queryContext

  const selectionContext = useSelectionContext()
  const { selection, deselectAll, allSelected } = selectionContext

  const { data: workspaces, mutate: mutateWorkspaces, defaultWorkspace, isLoading: workspacesLoading } = useWorkspaces()

  const { workspaceId } = useLocationWorkspaceId(queryParams)
  const { searchInput, setSearchInputState } = useLocationSearchInput(queryParams)
  const { paging } = useLocationPaging(queryParams)
  const { sorting } = useLocationSorting(queryParams, 'title')

  const { data: total, mutate: mutateEntriesCount } = useEntriesCount(workspaceId || '', searchInput)
  const { offset, limit } = boundsConverter({ total, ...paging })
  const host = settings.WEBSLIVER_API_URL

  const [OkCancelState, setOkCancelState] = React.useState<{ action: Function, message: string }>({ action: () => { }, message: '' })

  const { data: items, mutate: mutateItems } = useEntries({
    workspaceId: workspaceId || '',
    searchTerm: searchInput,
    offset,
    limit,
    sortField: queryParams.sortField,
    sortDirection: queryParams.sortDirection,
  })

  const [failedItems, setFailedItems] = React.useState<any[]>([])

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []

  React.useEffect(() => {
    // set workspace if not set
    if (isAuthenticated && !workspacesLoading && !workspaceId && defaultWorkspace?.uid) {
      navigateQuery({ workspaceId: defaultWorkspace.uid })
    }
  }, [workspacesLoading, isAuthenticated, defaultWorkspace?.uid, navigateQuery, workspaceId])

  const setSearchInput = async (input: dataService.EntrySearchInput[]) => {
    setSearchInputState(input)
    const tags = input.filter((tag: any) => typeof tag !== 'string').map((tag: any) => tag.name)
    const query = input.filter((item: any) => typeof item === 'string')

    await navigateQuery({ query, tags })
  }

  const handleDeleteMultiple = async (event: any, entryIds: any[]) => {

    const deleteAction = async () => {
      if (allSelected) {
        await dataService.deleteEntriesByQuery({
          query: {
            workspaceId: workspaceId || '',
            searchTerm: searchInput,
          },
          host, token
        })

        await mutateItems([])
        await mutateEntriesCount(0)

      } else {
        await dataService.deleteEntries({ entryIds, host, token })

        await mutateItems(items.filter((item: any) => !entryIds.includes(item.uid)))
        await mutateEntriesCount(total - entryIds.length)

      }

      deselectAll()
    }

    if (!event) {
      await deleteAction()
      return
    }

    // use popover if event is specified
    setOkCancelState({
      message: 'Are you sure you want to delete the selected items?',
      action: deleteAction
    })

    openOkCancelConfirm(event?.currentTarget)
  }

  const onDeleteOne = async (entry: any) => {
    await handleDeleteMultiple(undefined, [entry.uid])

    setEditDialogOpen(false)
  }

  const handleTagClick = async (tag: string) => {
    const remaining = searchInput.filter((item: dataService.EntrySearchInput) => typeof (item) === 'string' || item.name !== tag)

    if (remaining.length !== searchInput.length) {
      // we already have it, so we remove it
      await setSearchInput(remaining)
    } else {
      // we don't have it, so we add it
      await setSearchInput(Array.from(new Set([
        ...searchInput,
        { name: tag }
      ])))
    }
  }

  const onAddEntry = async (entry: any) => {
    if (workspaceId && entry) {
      // using upsert in case the item already exists, to prevent accidental overwrites
      await dataService.upsertEntry({ workspaceId, entry, host, token })

      await mutateItems()
      await mutateEntriesCount()

      setAddDialogOpen(false)
    }
  }

  const onEditEntry = async (entry: any) => {
    await dataService.updateEntry({ entry, host, token })

    await mutateItems()

    setEditDialogOpen(false)
  }

  React.useEffect(() => {

    const handleFailedItemsEvent = (event: any) => {
      if (event.source === window && event.data.type && event.data.type === 'ws.page.failedItems') {
        setFailedItems(event.data.content)

        if (event.data.content.length !== 0) {
          setFailedListDialogOpen(true)
        }
      }
    }

    window.addEventListener('message', handleFailedItemsEvent)
  }, [setFailedListDialogOpen])

  const { component: OkCancelConfirm, openFor: openOkCancelConfirm } = useOkCancelConfirmationPopover({
    onOK: OkCancelState.action, options: [], message: OkCancelState.message,
  })

  const onWorkspaceCopySelect = async (workspace: any) => {
    setWorkspaceCopyOpen(false)

    if (allSelected) {
      await dataService.copyEntriesToWorkspaceByQuery(
        {
          query: {
            workspaceId: workspaceId || '',
            searchTerm: searchInput,
          },
          destinationWorkspaceId: workspace.uid,
          host,
          token
        }
      )

    } else {
      await dataService.copyEntriesToWorkspace({
        entryIds: selection,
        sourceWorkspaceId: queryParams.workspaceId,
        destinationWorkspaceId: workspace.uid,
        host,
        token
      })
    }

    await mutateItems()
    await mutateEntriesCount()
  }

  const onWorkspaceMoveSelect = async (workspace: any) => {
    setWorkspaceMoveOpen(false)

    let result
    if (allSelected) {
      result = await dataService.moveEntriesToWorkspaceByQuery(
        {
          query: {
            workspaceId: workspaceId || '',
            searchTerm: searchInput,
          },
          destinationWorkspaceId: workspace.uid,
          host,
          token
        }
      )

    } else {
      result = await dataService.moveEntriesToWorkspace({
        entryIds: selection,
        sourceWorkspaceId: queryParams.workspaceId,
        destinationWorkspaceId: workspace.uid,
        host,
        token
      })
    }

    // skip unnecessary refresh
    if (result === undefined) return

    await mutateItems()
    await mutateEntriesCount()

    deselectAll()
  }

  const onSaveTags = async (tags: string[]) => {
    setAddTagsDialogOpen(false)

    if (allSelected) {
      await dataService.addTagsToEntriesByQuery({
        query: {
          workspaceId: workspaceId || '',
          searchTerm: searchInput,
        }, tags, host, token
      })

    } else {
      await dataService.addTagsToEntries({ workspaceId, entryIds: selection, tags, host, token })
    }


    await mutateItems()
  }

  const onDeleteTags = async (tags: string[]) => {
    setAddTagsDialogOpen(false)

    if (allSelected) {
      await dataService.deleteTagsForEntriesByQuery({
        query: {
          workspaceId: workspaceId || '',
          searchTerm: searchInput,
        }, tags, host, token
      })

    } else {
      await dataService.deleteTagsForEntries({ workspaceId, entryIds: selection, tags, host, token })
    }

    await mutateItems()
  }

  const handleChangePage = async (page: number) => {
    await navigateQuery({ page })
    await mutateItems()
  }

  const handleChangePageSize = async (pageSize: number) => {
    await navigateQuery({ page: 0, pageSize })
    await mutateItems()
  }

  const handleSorting = async (property: string) => {
    await navigateQuery({
      sortField: property,
      sortDirection: determineSortingDirection(sorting.sortDirection, sorting.sortField !== property)
    })

    await mutateItems()
  }

  const onSaveOne = (item: any) => {
    setEditedItem(item)
    setEditDialogOpen(true)
  }

  const openSelected = (newWindow = false, incognito = false) => {
    const selectedItems = items.filter(item => item.uid && selection.includes(item.uid))

    // @ts-ignore
    windowAug.openItems({ items: selectedItems, newWindow, incognito })
  }

  return (
    <RootDiv>
      {isAuthenticated &&
        <SelectionContext.Provider value={selectionContext}>
          <BaseLister
            items={items}
            headerFields={headerFields}
            searchInput={searchInput}
            total={total}
            page={paging.page}
            pageSize={paging.pageSize}
            sorting={sorting}
            getOptions={useOptions}
            onAdd={() => setAddDialogOpen(true)}
            onTagSelected={() => setAddTagsDialogOpen(true)}
            onSaveOne={onSaveOne}
            onDeleteSelected={(event: any) => handleDeleteMultiple(event, selection)}
            onCopySelected={() => setWorkspaceCopyOpen(true)}
            onMoveSelected={() => setWorkspaceMoveOpen(true)}
            onTagClick={handleTagClick}
            onSearchInputChange={setSearchInput}
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
            onChangeSorting={handleSorting}
            extensionMenuOptions={windowAug.extensionContext?.enabled ? [
              { label: 'Open selected in new tabs', onClick: () => openSelected() },
              { label: 'Open selected in a new window', onClick: () => openSelected(true) },
              { label: 'Open selected in new incognito window', onClick: () => openSelected(true, true) },
            ] : []}
          />
        </SelectionContext.Provider>}

      <EntryDialog
        isOpen={addDialogOpen}
        workspaceId={workspaceId}
        onSave={onAddEntry}
        onClose={() => setAddDialogOpen(false)}
      />
      <EntryDialog
        isOpen={editDialogOpen}
        item={editedItem}
        workspaceId={workspaceId}
        onSave={onEditEntry}
        onDelete={onDeleteOne}
        onClose={() => setEditDialogOpen(false)}
      />
      <WorkspaceManageDialog
        isOpen={workspaceCopyOpen}
        title='Select destination workspace'
        content={<Alert severity="warning">Be advised, copied entries that already exist in the destination workspace will be merged.</Alert>}
        workspaces={workspaces}
        refreshWorkspaces={mutateWorkspaces}
        onSelect={onWorkspaceCopySelect}
        onClose={() => setWorkspaceCopyOpen(false)}
      />
      <WorkspaceManageDialog
        isOpen={workspaceMoveOpen}
        title='Select destination workspace'
        content={<Alert severity="warning">Be advised, moved entries that already exist in the destination workspace will be merged.</Alert>}
        workspaces={workspaces}
        refreshWorkspaces={mutateWorkspaces}
        onSelect={onWorkspaceMoveSelect}
        onClose={() => setWorkspaceMoveOpen(false)}
      />
      <TagsInputDialog
        workspaceId={workspaceId}
        isOpen={addTagsDialogOpen}
        onSave={onSaveTags}
        onDelete={onDeleteTags}
        onClose={() => setAddTagsDialogOpen(false)}
      />
      <FailedListDialog
        isOpen={failedListDialogOpen}
        title="Failed to open items"
        content={failedItems}
        onClose={() => setFailedListDialogOpen(false)}
      />
      <OkCancelConfirm />
    </RootDiv>
  )
}

export default EntriesLister
