import React from "react"
import { useAuth0 } from "@auth0/auth0-react"
import { Box, Button, IconButton, Divider, Drawer, ListItem, Typography } from "@mui/material"
import { Add as AddIcon, Login as LoginIcon, Logout as LogoutIcon } from '@mui/icons-material'

import { LocationContext } from "./contexts/locationContext"
import WorkspaceEditDialog from "~/components/dialogs/workspaceEditDialog"
import { useWorkspaces } from "~/services/swr_hooks"

import { StyledLink } from "web-sliver-ui-components"
import { settings } from "~/config"
import { getQueryParamsString } from "./contexts/locationContextUtils"
import WorkspacesList from "./workspacesList"


const styles = {
  grow: {
    flexGrow: 1,
  },

  drawerContent: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    width: '100%',
    padding: '10px',
    gap: '10px',
    minWidth: '250px',
  },

  menuLink: {
    width: '90%'
  },
}

const appMenuOptions = [
  {
    key: 'entries',
    title: 'Entries',
    path: '/'
  },
  {
    key: 'tags',
    title: 'Tags',
    path: '/tags'
  },
  {
    key: 'preferences',
    title: 'Preferences',
    path: '/preferences'
  },
]

export const AppMenu = ({
  isOpen,
  onClose,
}: {
  isOpen?: boolean,
  onClose: () => void,
}) => {
  const { location, queryParams } = React.useContext(LocationContext)

  const { isAuthenticated, isLoading, loginWithRedirect, logout } = useAuth0()
  const { data: workspaces, isLoading: workspacesLoading, mutate: mutateWorkspaces, } = useWorkspaces()

  const [editDialogOpen, setEditDialogOpen] = React.useState(false)
  const [selectedWorkspace, setSelectedWorkspace] = React.useState(undefined)

  const toggleDrawer = (open: boolean) => (event: any) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return
    }

    if (!open) {
      onClose()
    }
  }

  const handleEdit = async (_event: any, workspace: any) => {
    setSelectedWorkspace(workspace)
    setEditDialogOpen(true)
  }

  const handleSave = async () => {
    setSelectedWorkspace(undefined)
    setEditDialogOpen(true)
  }

  return (
    <Drawer
      anchor="right"
      variant="temporary"
      open={isOpen}
      onClose={toggleDrawer(false)}
    >
      <Box sx={styles.drawerContent}>
        {isAuthenticated && appMenuOptions.map((option: any) => (
          <ListItem
            selected={option.path === location.pathname}
            key={option.key}
            onClick={toggleDrawer(false)}
            disableGutters
            sx={{
              p: 0,
              '&:hover': {
                backgroundColor: 'rgba(255, 255, 255, 0.08)'
              }
            }}
          >
            <StyledLink
              to={`${option.path}${getQueryParamsString({ workspaceId: queryParams.workspaceId })}`}
              onClick={() => toggleDrawer(false)}
              sx={{
                ...styles.menuLink,
                p: 1,
                color: 'white',
                textDecoration: 'none',
                w: '100%',
              }}
            >
              {option.title}
            </StyledLink>
          </ListItem>
        ))}

        {isAuthenticated && <Divider orientation="horizontal" light flexItem variant="fullWidth" sx={{ m: 1 }} />}

        {isAuthenticated && !workspacesLoading && <>
          <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', pl: 2, pr: 1 }}>
            <Typography>Workspaces</Typography>
            <IconButton color='primary' onClick={handleSave}>
              <AddIcon />
            </IconButton>
          </Box>


          <WorkspacesList
            workspaces={workspaces}
            renderLinks
            onItemClick={toggleDrawer(false)}
            onItemEdit={handleEdit}
            sx={{ width: '250px' }}
          />

          <WorkspaceEditDialog
            isOpen={editDialogOpen}
            item={selectedWorkspace}
            refreshWorkspaces={() => mutateWorkspaces()}
            onClose={() => setEditDialogOpen(false)}
          />
        </>}

        {isAuthenticated && <Divider orientation="horizontal" light flexItem variant="fullWidth" sx={{ m: 1 }} />}

        {!isLoading && <Button
          variant='contained'
          color='primary'
          onClick={() => isAuthenticated ? logout({ returnTo: settings.WEBSLIVER_APP_URL }) : loginWithRedirect({ appState: { returnTo: settings.WEBSLIVER_APP_URL } })}
          sx={{ justifyContent: 'space-between'}}
        >
          {isAuthenticated ? `Sign out` : 'Sign in'}
          {isAuthenticated ? <LogoutIcon /> :  <LoginIcon />}
        </Button>}
      </Box>
    </Drawer>
  )
}
