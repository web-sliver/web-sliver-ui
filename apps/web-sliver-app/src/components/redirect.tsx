import { Typography, styled } from "@mui/material"


const RootDiv = styled('div')({
  width: '100%'
})

const Redirect = () => {

  return (
    <RootDiv sx={{ height: '100px', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
      <Typography sx={{ fontSize: '1.5em' }}>Redirecting ..</Typography>
    </RootDiv>

  )
}

export default Redirect
