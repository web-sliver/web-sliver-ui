import React from "react"

import { ImportSection } from "./importSection/importSection"
import { ExportSection } from "./exportSection"


const Preferences = () => {
  const [expanded, setExpanded] = React.useState<string | false>(false)

  const handleChange = (panel: string | false) => (_event: any, isExpanded: boolean) => {
    setExpanded(isExpanded ? panel : false)
  }

  return (
    <>
      {/* <ControlledSection
        sectionKey="general"
        title="General"
        description="General user settings"
        expandedState={expanded}
        handleExpand={handleChange}
      >
        <Typography>
          Not implemented yet
        </Typography>
      </ControlledSection> */}

      <ImportSection
        expandedState={expanded}
        handleExpand={handleChange}
      />

      <ExportSection
        expandedState={expanded}
        handleExpand={handleChange}
      />
    </>

  )
}

export default Preferences
