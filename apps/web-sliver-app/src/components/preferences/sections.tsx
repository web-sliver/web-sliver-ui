import { ExpandMore as ExpandMoreIcon } from '@mui/icons-material'

import { Accordion, AccordionDetails, AccordionSummary, Typography } from "@mui/material"


const styles = {
  sectionTitle: {
    width: '33%',
    flexShrink: 0,
  },

  sectionDescription: {
    color: 'text.secondary'
  },
}


export const BaseSection = ({ sectionKey, title, description, isExpanded, onExpand, children }: {
  sectionKey: string,
  title: string,
  description: string,
  isExpanded: boolean,
  onExpand: (event: any, isExpanded: boolean) => void,
  children: any,
}) => {

  return (
    <Accordion expanded={isExpanded} onChange={onExpand}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls={`${sectionKey}-content`}
        id={`${sectionKey}-header`}
      >
        <Typography sx={styles.sectionTitle}>{title}</Typography>
        <Typography sx={styles.sectionDescription}>{description}</Typography>
      </AccordionSummary>
      <AccordionDetails id={`${sectionKey}-content`}>
        {children}
      </AccordionDetails>
    </Accordion>
  )
}

export const ControlledSection = ({ sectionKey, title, description, expandedState, handleExpand, children }: {
  sectionKey: string,
  title: string,
  description: string,
  expandedState: string | false,
  handleExpand: (panel: string | false) => any,
  children: any,
}) => {

  return (
    <BaseSection
      sectionKey={sectionKey}
      title={title}
      description={description}
      isExpanded={expandedState === sectionKey}
      onExpand={handleExpand(sectionKey)}
    >
      {children}
    </BaseSection>
  )
}
