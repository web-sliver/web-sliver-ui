import { Box, InputLabel, MenuItem, Select } from "@mui/material"


export enum ImportType {
  FIREFOX = 'Firefox',
  WEB_SLIVER = 'WebSliver',
  TEXT = 'Text',
}

export interface ImportOption {
  option: ImportType,
  fileExtension?: string,
}

export const ImportOptions: ImportOption[] = [
  {
    option: ImportType.WEB_SLIVER,
    fileExtension: '.json',
  },
  {
    option: ImportType.FIREFOX,
    fileExtension: '.json',
  },
  {
    option: ImportType.TEXT,
  }
]

export const styles = {
  sectionForm: {
    width: '100%',
    gap: '20px'
  },

  twoColumnContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
}

export const ImportOptionSelect = ({ selected, onClick }: {
  selected: ImportType,
  onClick: (option: ImportOption) => void,
}) => {
  return (<Box sx={styles.twoColumnContainer}>
    <InputLabel>Select import type</InputLabel>
    <Select sx={{ width: '15vw' }}  value={selected}>
      {ImportOptions.map((option, index) => {
        return (<MenuItem key={index} value={option.option} onClick={() => onClick(option)}>{option.option}</MenuItem>)
      })}
    </Select>
  </Box>)
}
