import { FormGroup, Alert } from "@mui/material"
import React from "react"

import * as dataService from 'web-sliver-data-interface'
import { useCheckbox } from 'web-sliver-ui-components'

import { useAuthToken } from "~/hooks/authTokenHook"
import { FirefoxJsonParser, TextParser, WebSliverJsonParser } from "~/libs/importUtilities"
import { ControlledSection } from "../sections"
import { settings } from "~/config"
import { ImportOption, ImportOptionSelect, ImportOptions, ImportType, styles } from "."
import { ProgressControl } from "./progressControl"
import FileImport from "./FileImport"
import TextImport from "./TextImport"
import { useWorkspaceSelect, useWorkspaceSelectOffline } from "~/components/inputs/workspaceSelect"


export const ImportSection = ({ expandedState, handleExpand }: {
  expandedState: string | false,
  handleExpand: (panel: string | false) => any,
}) => {

  const sectionKey = 'import-section'
  const [selectedOption, setSelectedOption] = React.useState<ImportOption>(ImportOptions[0])
  const [textValue, setTextValue] = React.useState<string>('')
  const [tags, setTags] = React.useState<string[]>([])
  const [selectedFile, setSelectedFile] = React.useState<{ content: Blob, name: string } | null>(null)
  const [canImport, setCanImport] = React.useState<boolean>(false)

  const allWorkspacesProps = useCheckbox(true)
  const srcWorkspaceSelectProps = useWorkspaceSelectOffline({ workspaces: [], })
  const destWorkspaceSelectProps = useWorkspaceSelect({})

  const { value: allWorkspaces } = allWorkspacesProps
  const {
    selectedWorkspace: selectedDestWorkspace,
    workspaces: destWorkspaces,
  } = destWorkspaceSelectProps
  const {
    selectedWorkspace: selectedSourceWorkspace,
  } = srcWorkspaceSelectProps

  const host = settings.WEBSLIVER_API_URL
  const { token } = useAuthToken()

  const {
    component: ProgressControlComponent,
    setStatus,
    resetState,
    updateProgress,
    setStartState,
    setFinishedState,
  } = ProgressControl()

  const fetchSourceWorkspaces = async ({ file }: { file: any }) => {
    if (selectedOption?.option === ImportType.WEB_SLIVER) {
      let workspaces = []
      try {
        const content = await (new WebSliverJsonParser()).parseRawJsonFile({ file })
        workspaces = content.data.workspaces.map((workspace: any) => ({
          ...workspace,
          entries: [], // don't need them here
          name: `${workspace.name} (${workspace.entries.length} entries)`
        }))

        setStatus('')
        return workspaces
      } catch (error: any) {
        if (error.name === 'SyntaxError') {
          setStatus('ERROR: File is not properly formatted.')

        } else {
          console.error(error)
          setStatus(`ERROR: ${error.message || error}`)
        }
      }
    } else {
      return []
    }
  }

  const setStart = ({ entries, workspaceName = '' }: { entries: dataService.InputEntry[], workspaceName?: string }) => {
    let message = `Finished processing ${entries.length | 0} entries.`

    switch (selectedOption?.option) {
      case ImportType.FIREFOX:
        message += ` Import (workspace: ${selectedDestWorkspace?.name}, additional tags: ${tags}) started at ${new Date().toLocaleString()} ..`
        break

      case ImportType.WEB_SLIVER:
        const selected = allWorkspaces ? `workspace: ${workspaceName}, ` : `from: ${selectedSourceWorkspace.name}, to: ${selectedDestWorkspace?.name}, `
        message += ` Import (${selected}additional tags: ${tags}) started at ${new Date().toLocaleString()} ..`
        break
    }

    setStartState({ message })
  }

  const hasWorkspace = (workspaceId: string) => !!destWorkspaces.find((workspace: any) => workspace.uid === workspaceId)

  const executeImport = async () => {
    resetState()

    let entries: dataService.InputEntry[]

    try {
      setStatus('Processing entries ..')

      switch (selectedOption.option) {
        case ImportType.FIREFOX:
          if (!selectedFile || !selectedDestWorkspace) {
            return
          }

          entries = await (new FirefoxJsonParser()).parseFile({ file: selectedFile.content, additionalTags: tags })

          setStart({ entries })

          await dataService.bulkUpsertEntries({
            entries,
            workspaceId: selectedDestWorkspace.uid,
            host,
            token,
            throwErrors: true,
            stepCallback: ({ step, total }) => {
              updateProgress({ step, total })
            }
          })

          setFinishedState()
          break

        case ImportType.WEB_SLIVER:
          if (!selectedFile || (!allWorkspaces && !selectedDestWorkspace)) {
            return
          }

          const selectedWorkspaces = allWorkspaces ? [] : [selectedSourceWorkspace.uid]
          const workspaces = await (new WebSliverJsonParser()).parseFile({ file: selectedFile.content, selectedWorkspaces, additionalTags: tags })

          let skippedWorkspaces = []

          for (const workspace of workspaces) {
            setStart({ entries: workspace.entries, workspaceName: workspace.name })

            if (allWorkspaces && !hasWorkspace(workspace.uid)) {
              // todo: perhaps create the workspace here if not exists?
              skippedWorkspaces.push(workspace.name)
              continue
            }

            await dataService.bulkUpsertEntries({
              entries: workspace.entries,
              workspaceId: allWorkspaces ? workspace.uid : selectedDestWorkspace?.uid,
              host,
              token,
              throwErrors: true,
              stepCallback: ({ step, total }) => {
                updateProgress({ step, total })
              }
            })
          }

          setFinishedState(skippedWorkspaces.length === 0 ? '' : ` Skipped workspaces in import file that were not found in account: ${skippedWorkspaces.join(', ')}`)
          break

        case ImportType.TEXT:
          if (!textValue || !selectedDestWorkspace) {
            return
          }

          entries = await (new TextParser()).parseText({ text: textValue, additionalTags: tags })

          setStart({ entries })

          await dataService.bulkUpsertEntries({
            entries,
            workspaceId: selectedDestWorkspace.uid,
            host,
            token,
            throwErrors: true,
            stepCallback: ({ step, total }) => {
              updateProgress({ step, total })
            }
          })

          setFinishedState(` ${entries.length} entries imported.`)
          break

        default:
          console.log(`Option ${selectedOption.option} not implemented yet.`)
          return
      }

    } catch (error: any) {
      if (error.name === 'SyntaxError') {
        setStatus('ERROR: File is not properly formatted.')

      } else {
        console.error(error)
        setStatus(`ERROR: ${error.message}`)
      }

      return
    }

  }

  return (
    <ControlledSection
      sectionKey={sectionKey}
      title="Import"
      description="Import tools for entries"
      expandedState={expandedState}
      handleExpand={handleExpand}
    >
      <FormGroup sx={styles.sectionForm} key="import-form-group">
        <Alert severity="info">
          Existing entries in the workspace that have the same title and location will be merged with the imported entries.
          <br />Please make sure this is what you want, or use an empty workspace for the import.
          <br />Please do not close this window while the import process is running.
          <br />Note: When importing a WebSliver export that has workspaces which no longer exist, they will be skipped,
          unless you import them one by one into new or existing workspaces.
        </Alert>

        <ImportOptionSelect selected={selectedOption.option} onClick={option => {
          setSelectedOption(option)
          setSelectedFile(null)
          setTextValue('')
        }} />
        {selectedOption?.option === ImportType.WEB_SLIVER && <FileImport
          fetchSourceWorkspaces={fetchSourceWorkspaces}
          fileExtension={selectedOption.fileExtension}
          showAllWorkspacesOption={true}
          allWorkspacesProps={allWorkspacesProps}
          srcWorkspaceSelectProps={srcWorkspaceSelectProps}
          destWorkspaceSelectProps={destWorkspaceSelectProps}
          setCanImport={setCanImport}
          tags={tags}
          setTags={setTags}
          selectedFile={selectedFile}
          setSelectedFile={setSelectedFile}
        />}
        {selectedOption?.option === ImportType.FIREFOX && <FileImport
          fetchSourceWorkspaces={fetchSourceWorkspaces}
          fileExtension={selectedOption.fileExtension}
          showAllWorkspacesOption={false}
          allWorkspacesProps={allWorkspacesProps}
          srcWorkspaceSelectProps={srcWorkspaceSelectProps}
          destWorkspaceSelectProps={destWorkspaceSelectProps}
          setCanImport={setCanImport}
          tags={tags}
          setTags={setTags}
          selectedFile={selectedFile}
          setSelectedFile={setSelectedFile}
        />}
        {selectedOption?.option === ImportType.TEXT && <TextImport
          textValue={textValue}
          onTextChange={setTextValue}
          setCanImport={setCanImport}
          tags={tags}
          setTags={setTags}
          destWorkspaceSelectProps={destWorkspaceSelectProps}
        />}

        <ProgressControlComponent disableImport={!canImport} onImport={executeImport} />
      </FormGroup>
    </ControlledSection>
  )
}
