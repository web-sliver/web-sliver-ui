import React from "react"
import { Box, Button, Checkbox, InputLabel } from "@mui/material"
import { EntrySearchInput, SearchInput, SearchOptionType } from "web-sliver-ui-components"

import { useCheckbox } from 'web-sliver-ui-components'

import { styles } from "."
import { useMatchingTags } from "~/services/swr_hooks"
import { LocationContext, useLocationWorkspaceId } from "~/components/contexts/locationContext"
import { useWorkspaceSelect, useWorkspaceSelectOffline } from "~/components/inputs/workspaceSelect"


const FileImport = ({
  fetchSourceWorkspaces,
  fileExtension = '',
  showAllWorkspacesOption = false,
  allWorkspacesProps,
  srcWorkspaceSelectProps,
  destWorkspaceSelectProps,
  setCanImport,
  tags,
  setTags,
  selectedFile,
  setSelectedFile,
}: {
  fetchSourceWorkspaces: ({ file }: { file: any }) => Promise<any[]>,
  fileExtension?: string,
  showAllWorkspacesOption?: boolean,
  allWorkspacesProps: ReturnType<typeof useCheckbox>,
  srcWorkspaceSelectProps: ReturnType<typeof useWorkspaceSelectOffline>,
  destWorkspaceSelectProps: ReturnType<typeof useWorkspaceSelect>,
  setCanImport: (canImport: boolean) => void,
  tags: string[],
  setTags: (tags: string[]) => void,
  selectedFile: { content: Blob, name: string } | null,
  setSelectedFile: (file: { content: Blob, name: string } | null) => void,
}) => {
  const importFileInputRef = React.useRef<any>()

  const queryContext = React.useContext(LocationContext)
  const { queryParams } = queryContext
  const { workspaceId } = useLocationWorkspaceId(queryParams)

  const { value: allWorkspaces, bind: bindAllWorkspaces } = allWorkspacesProps
  const {
    component: DestWorkspaceSelect,
    selectedWorkspace: selectedDestWorkspace,
    setSelectedWorkspace: setSelectedDestWorkspace,
    workspacesLoading: destWorkspacesLoading,
  } = destWorkspaceSelectProps
  const {
    component: SourceWorkspaceSelect,
    workspaces: sourceWorkspaces,
    setWorkspaces: setSourceWorkspaces,
  } = srcWorkspaceSelectProps
  const [sourceWorkspacesLoading, setSourceWorkspacesLoading] = React.useState(false)

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: (!(showAllWorkspacesOption && allWorkspaces) && selectedDestWorkspace?.uid) || workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []


  const fileHandler = async (event: any) => {
    const file = event.target.files[0]

    setSelectedFile({ content: file, name: file?.name || '' })

    setSourceWorkspacesLoading(true)
    setSourceWorkspaces(await fetchSourceWorkspaces({ file }))
    setSourceWorkspacesLoading(false)
  }

  React.useEffect(() => {
    if (!destWorkspacesLoading && workspaceId && !selectedDestWorkspace) {
      setSelectedDestWorkspace(workspaceId)
    }

  }, [destWorkspacesLoading, workspaceId, selectedDestWorkspace, setSelectedDestWorkspace])

  React.useEffect(() => {
    setCanImport(!sourceWorkspacesLoading && !!selectedFile)
  }, [ sourceWorkspacesLoading, selectedFile, setCanImport ])

  const setSearchInput = async (input: EntrySearchInput[]) => {
    setTags(input.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name))
  }

  return (<>
    <Box sx={styles.twoColumnContainer}>
      <InputLabel htmlFor="import-file">Select import file:</InputLabel>

      <InputLabel>
        {selectedFile ? ` ${selectedFile.name} ` : ''}

        &nbsp;&nbsp;

        <Button variant="contained" color="primary"
          onClick={() => importFileInputRef.current.click()}
        >
          Select file
        </Button>
      </InputLabel>
    </Box>

    <input
      accept={fileExtension}
      hidden
      ref={(node: any) => importFileInputRef.current = node}
      type="file"
      onChange={fileHandler}
    />

    {selectedFile && <>
      {showAllWorkspacesOption && !sourceWorkspacesLoading && <Box sx={styles.twoColumnContainer}>
        <InputLabel>Import all workspaces?</InputLabel>
        <Checkbox
          color="primary"
          {...bindAllWorkspaces}
        />
      </Box>}

      {sourceWorkspaces.length !== 0 && !allWorkspaces && !sourceWorkspacesLoading && <Box sx={styles.twoColumnContainer}>
        <InputLabel>Select source workspace:</InputLabel>
        <SourceWorkspaceSelect sx={{ width: '30%' }} />
      </Box>}

      {!(showAllWorkspacesOption && allWorkspaces) && !destWorkspacesLoading && <Box sx={styles.twoColumnContainer}>
        <InputLabel>Select destination workspace:</InputLabel>
        <DestWorkspaceSelect sx={{ width: '30%' }} />
      </Box>}
      <Box sx={styles.twoColumnContainer}>
        <InputLabel>Select additional tags:</InputLabel>
        <SearchInput
          sx={{ width: '30%' }}
          getOptions={useOptions}
          value={tags}
          label="Tags"
          setValue={setSearchInput}
          disabledTypes={[SearchOptionType.EXCLUSION, SearchOptionType.WILDCARD]}
        />
      </Box>
    </>}
  </>)

}

export default FileImport
