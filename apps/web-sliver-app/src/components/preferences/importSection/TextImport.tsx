import React from "react"
import { Alert, Box, InputLabel, TextField } from "@mui/material"
import { EntrySearchInput, SearchInput, SearchOptionType } from "web-sliver-ui-components"

import { styles } from "."
import { useMatchingTags } from "~/services/swr_hooks"
import { LocationContext, useLocationWorkspaceId } from "~/components/contexts/locationContext"
import { useWorkspaceSelect } from "~/components/inputs/workspaceSelect"

const TextImport = ({ textValue, onTextChange, destWorkspaceSelectProps: workspaceSelectProps, setCanImport, tags, setTags }: {
  textValue: string,
  onTextChange: (text: string) => void,
  destWorkspaceSelectProps: ReturnType<typeof useWorkspaceSelect>,
  setCanImport: (canImport: boolean) => void,
  tags: string[],
  setTags: (tags: string[]) => void,
}) => {

  const queryContext = React.useContext(LocationContext)
  const { queryParams } = queryContext
  const { workspaceId } = useLocationWorkspaceId(queryParams)

  const {
    component: DestWorkspaceSelect,
    selectedWorkspace: selectedDestWorkspace,
    setSelectedWorkspace: setSelectedDestWorkspace,
    workspacesLoading: destWorkspacesLoading,
  } = workspaceSelectProps

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: selectedDestWorkspace?.uid || workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []

  React.useEffect(() => {
    if (!destWorkspacesLoading && workspaceId && !selectedDestWorkspace) {
      setSelectedDestWorkspace(workspaceId)
    }

  }, [destWorkspacesLoading, workspaceId, selectedDestWorkspace, setSelectedDestWorkspace])

  React.useEffect(() => {
    setCanImport(!!textValue && !!selectedDestWorkspace)
  }, [textValue, selectedDestWorkspace, setCanImport])

  const setSearchInput = async (input: EntrySearchInput[]) => {
    setTags(input.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name))
  }

  return (<Box sx={{ display: "flex", flexDirection: "column", gap: "20px"}}>
    <Alert severity="info">
      Please specify one entry per line. Supported formats:
      <br />- Markdown links. ex: [title](https://example.com)
      <br />- Simple title and url. ex: title - https://example.com
      <br />- Url only (sorrounding text will be ignored). ex: https://example.com
    </Alert>
    <TextField
      multiline
      fullWidth
      value={textValue}
      label="Text to import"
      margin="dense"
      size="small"
      minRows={5}
      onChange={(event) => onTextChange(event.target.value)}
    />
    {!!textValue && <>
      {!destWorkspacesLoading && <Box sx={styles.twoColumnContainer}>
        <InputLabel>Select destination workspace:</InputLabel>
        <DestWorkspaceSelect sx={{ width: '30%' }} />
      </Box>}

      <Box sx={styles.twoColumnContainer}>
        <InputLabel>Select additional tags:</InputLabel>
        <SearchInput
          sx={{ width: '30%' }}
          getOptions={useOptions}
          value={tags}
          label="Tags"
          setValue={setSearchInput}
          disabledTypes={[SearchOptionType.EXCLUSION, SearchOptionType.WILDCARD]}
        />
      </Box>
    </>}
  </Box>)
}

export default TextImport
