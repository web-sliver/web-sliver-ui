import { Box, Button } from "@mui/material"
import LabeledLinearProgress from "~/components/labeledLinearProgress"
import { styles } from "."
import React from "react"


export const ProgressControl = () => {
  const [importStatus, setStatus] = React.useState<string>('')
  const [progress, setProgress] = React.useState(0)
  const [inProgress, setInProgress] = React.useState(false)

  const resetState = () => {
    setProgress(0)
    setStatus('')
    setInProgress(false)
  }

  const updateProgress = ({ step, total }: { step: number, total: number }) => {
    total === 0 ? setProgress(0) : setProgress(step * 100 / total)
  }

  const setStartState = ({ message }: { message: string }) => {
    setProgress(0)
    setStatus(message)
    setInProgress(true)
  }

  const setFinishedState = (endMessage: string = '') => {
    setProgress(100)
    setStatus(`Import finished at ${new Date().toLocaleString()}.${endMessage}`)
    setInProgress(false)
  }

  return {
    setStatus,
    resetState,
    updateProgress,
    setStartState,
    setFinishedState,
    component: ({ disableImport, onImport }: { disableImport: boolean, onImport: (event: any) => void }) => (<>

      {<Box sx={styles.twoColumnContainer}>
        <>Select action</>
        <Box sx={{ display: 'flex', width: '14vw', justifyContent: 'space-between' }}>
          <Button
            variant="contained"
            color="error"
            disabled={!inProgress}
            onClick={() => window.location.reload() /* hack to interrupt an import, until a better method is found */}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={onImport}
            disabled={disableImport || inProgress}
          >
            Import entries!
          </Button>
        </Box>
      </Box>}
      {inProgress && <LabeledLinearProgress value={progress} />}
      <span>{importStatus}</span>
    </>)
  }
}
