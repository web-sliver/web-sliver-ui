import { Box, Button, FormGroup, InputLabel, Alert, Checkbox } from "@mui/material"
import React from 'react'
import JSZip from 'jszip'
import FileSaver from 'file-saver'
import moment from 'moment'

import * as dataService from 'web-sliver-data-interface'
import { useCheckbox } from 'web-sliver-ui-components'
import { useAuthToken } from "~/hooks/authTokenHook"

import { ControlledSection } from "./sections"
import { useWorkspaceSelect } from "~/components/inputs/workspaceSelect"
import { settings } from "~/config"


const EXPORT_FILE_VERSION = '1.0'


const styles = {
  sectionForm: {
    width: '100%',
    gap: '20px'
  },

  twoColumnContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
}

export const ExportSection = ({ expandedState, handleExpand }: {
  expandedState: string | false,
  handleExpand: (panel: string | false) => any,
}) => {

  const [enableExport, setEnableExport] = React.useState<Boolean>(true)

  const sectionKey = 'export-section'
  const { token } = useAuthToken()
  const host = settings.WEBSLIVER_API_URL

  const { component: WorkspaceSelect, selectedWorkspace, } = useWorkspaceSelect({ allowEmpty: true, emptyName: 'All Workspaces' })

  const { value: disableFavicons, bind: bindDisableFavicons } = useCheckbox(false)
  const { value: disableIndentation, bind: bindDisableIndentation } = useCheckbox(true)
  const { value: disableArchive, bind: bindDisableArchive } = useCheckbox(false)

  const executeExport = async () => {
    setEnableExport(false)

    let data: any = await dataService.getExportData({ workspaceId: selectedWorkspace?.uid, disableFavicons, host, token, })

    const exportData = {
      exportVersion: EXPORT_FILE_VERSION,
      exportDate: moment().format('YYYY-MM-DD hh:mm:ss ZZ'),
      data: {
        workspaces: data?.websliver_workspace || [],
        favicons: data?.websliver_favicon || [],
      }
    }

    const workspaceId = selectedWorkspace?.uid || 'all'
    const fileName = `WebSliver-Entries-${workspaceId}-${moment().format('YYYYMMDD-hhmmssZZ')}`
    const fileContent = JSON.stringify(exportData, null, disableIndentation ? 0 : 2)

    if (disableArchive) {
      const blobContent = new Blob([fileContent], { type: 'text/json' })

      FileSaver.saveAs(blobContent, `${fileName}.json`)

    } else {
      const zip = new JSZip()
      zip.file(`${fileName}.json`, fileContent)
      const zipContent = await zip.generateAsync({
        type: 'blob',
        compression: 'DEFLATE',
        compressionOptions: { level: 1 }
      })

      FileSaver.saveAs(zipContent, `${fileName}.zip`)
    }

    setEnableExport(true)
  }


  return (
    <ControlledSection
      sectionKey={sectionKey}
      title="Export"
      description="Export tools for entries"
      expandedState={expandedState}
      handleExpand={handleExpand}
    >
      <FormGroup sx={styles.sectionForm}>
        <Alert severity="info">Please do not close this window while the export process is running.</Alert>

        <Box sx={styles.twoColumnContainer}>
          <InputLabel>Select workspace:</InputLabel>
          <WorkspaceSelect sx={{ width: '25vw' }} />
        </Box>

        <Box sx={styles.twoColumnContainer}>
          <InputLabel>Disable export favicons?</InputLabel>
          <Checkbox
            color="primary"
            {...bindDisableFavicons}
          />
        </Box>

        <Box sx={styles.twoColumnContainer}>
          <InputLabel>Disable export indentation?</InputLabel>
          <Checkbox
            color="primary"
            {...bindDisableIndentation}
          />
        </Box>

        <Box sx={styles.twoColumnContainer}>
          <InputLabel>Disable export archiving?</InputLabel>
          <Checkbox
            color="primary"
            {...bindDisableArchive}
          />
        </Box>

        <Button variant="contained" color="primary" onClick={executeExport} disabled={!enableExport}>Generate</Button>
      </FormGroup>
    </ControlledSection>
  )
}
