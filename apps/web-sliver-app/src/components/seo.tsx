import { Helmet, HelmetProvider } from "react-helmet-async"


function SEO({ description = '', lang = 'en', meta = [], title }: {
  description?: string,
  lang?: string,
  meta?: any[],
  title: string
}) {
  const metaDescription = description || 'WebSliver Web Application'

  const pageTitle = title ? `${title} | WebSliver` : 'WebSliver'

  return (
    <HelmetProvider>
      <Helmet
        htmlAttributes={{
          lang,
        }}
        title={pageTitle}
        meta={[
          {
            name: `description`,
            content: metaDescription,
          },
          {
            property: `og:title`,
            content: pageTitle,
          },
          {
            property: `og:description`,
            content: metaDescription,
          },
          {
            property: `og:type`,
            content: `website`,
          },
          {
            name: `twitter:card`,
            content: `summary`,
          },
          {
            name: `twitter:creator`,
            content: 'Xyder',
          },
          {
            name: `twitter:title`,
            content: pageTitle,
          },
          {
            name: `twitter:description`,
            content: metaDescription,
          },
        ].concat(meta)}
      />
    </HelmetProvider>
  )
}

export default SEO
