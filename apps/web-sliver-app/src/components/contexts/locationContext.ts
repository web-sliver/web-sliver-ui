import React from "react"
import _ from 'lodash'

import { Location, NavigateFunction } from "react-router-dom"

import { navigateByQueryParams, parseQueryString, QueryParams, queryStringToSearchInput } from "./locationContextUtils"
import { EntrySearchInput, ILocationContext } from "web-sliver-ui-components"


export const buildLocationContext = ({ location, navigate }: { location: Location, navigate: NavigateFunction }) => {
  const existingQueryParams = parseQueryString(location.search)

  const navigateQuery = async (queryParams: any, path: string = location.pathname, reset: boolean = false) => {
    const newQueryParams = reset ? queryParams : Object.assign({}, existingQueryParams, queryParams)

    navigateByQueryParams({ queryParams: newQueryParams, path, navigate })
  }

  return {
    location,
    queryParams: existingQueryParams,
    navigateQuery
  }
}

// export const useQueryParams = (queryParams: QueryParams) => {
//   const [queryParamsState, setQueryParams] = React.useState(queryParams)


//   const patchQueryParams = (props: any) => {
//     setQueryParams({
//       ...queryParamsState,
//       ...props,
//     })
//   }

//   React.useEffect(() => {
//     // sync query params
//     if (queryParams !== queryParamsState) {
//       setQueryParams(queryParams)
//     }
//   }, [queryParams])

//   return {
//     queryParams: queryParamsState,
//     setQueryParams,
//     patchQueryParams
//   }
// }

export const useLocationPaging = (queryParams: QueryParams) => {
  const [paging, setPaging] = React.useState({ page: queryParams.page, pageSize: queryParams.pageSize })

  React.useEffect(() => {
    if (paging.page !== queryParams.page || paging.pageSize !== queryParams.pageSize) {
      setPaging({ page: queryParams.page, pageSize: queryParams.pageSize })
    }
  }, [queryParams, paging.page, paging.pageSize])

  return {
    paging,
    setPaging
  }
}

export const useLocationSearchInput = (queryParams: QueryParams) => {
  const [searchInput, setSearchInputState] = React.useState<EntrySearchInput[]>(queryStringToSearchInput(queryParams))

  React.useEffect(() => {
    const newInput = queryStringToSearchInput(queryParams)

    if (!_.isEqual(searchInput, newInput)) {
      setSearchInputState(queryStringToSearchInput(queryParams))
    }
  }, [queryParams, searchInput])

  return {
    searchInput,
    setSearchInputState,
  }
}

export const useLocationSorting = (queryParams: QueryParams, defaultField: string = '') => {
  const [sorting, setSortingState] = React.useState({ sortField: queryParams?.sortField || defaultField, sortDirection: queryParams?.sortDirection || 'asc' })

  React.useEffect(() => {
    // update inner state from query params
    const sortDirection = queryParams.sortDirection || 'asc'
    const sortField = queryParams.sortField || defaultField

    if (sorting.sortDirection !== sortDirection || sorting.sortField !== sortField) {
      setSortingState({ sortField, sortDirection })
    }
  }, [queryParams, defaultField, sorting.sortDirection, sorting.sortField])

  return {
    sorting,
    setSortingState
  }
}

export const useLocationWorkspaceId = (queryParams: QueryParams) => {
  const [workspaceId, setWorkspaceId] = React.useState(queryParams.workspaceId)

  React.useEffect(() => {
    if (workspaceId !== queryParams.workspaceId) {
      setWorkspaceId(queryParams.workspaceId)
    }
  }, [queryParams, workspaceId])

  return {
    workspaceId,
    setWorkspaceId
  }
}

export const LocationContext = React.createContext<ILocationContext>({ location: undefined, queryParams: {}, navigateQuery: async () => { } })
