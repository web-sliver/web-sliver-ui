import queryString from 'query-string'
import { NavigateFunction } from 'react-router-dom'


export interface QueryParams {
  tags: string[],
  query: string[],
  workspaceId: string,
  sortField: string,
  sortDirection: 'asc' | 'desc',
  page: number,
  pageSize: number,
}

export interface QueryContext {
  queryParams: QueryParams,
  navigateQuery: Function,
}


export const queryStringToSearchInput = ({ tags, query }: {
  tags: any[],
  query: string[],
}) => {

  return [
    ...tags.map((tag: string) => ({ name: tag })),
    ...query
  ]
}

export const parseQueryString = (search: any) => {
  if (!search) {
    return {
      tags: [],
      query: [],
      workspaceId: '',
      page: 0,
      pageSize: 0,
    }
  }

  let {
    tag: tags,
    q: query,
    ws: workspaceId,
    p: page,
    ps: pageSize,
    sort: sortField,
    dir: sortDirection,
  } = queryString.parse(search)

  const normalizeArray = (input: string | Array<string | null> | null): string[] => {
    if (!input) {
      return []
    }

    if (typeof input === 'string') {
      return input ? [input] : []
    }

    return input.filter((item: string | null) => !!item) as string[]
  }

  const normalizeInt = (input: string | Array<string | null> | null, fallback: number): number => {
    if (!input) {
      return fallback
    }

    if (typeof input === 'string') {
      return input ? parseInt(input) : fallback
    }

    const items = input.filter((item: string | null) => !!item) as string[]
    return items.length !== 0 ? parseInt(items[0]) : fallback
  }

  const ret: QueryParams = {
    query: normalizeArray(query),
    tags: normalizeArray(tags),
    workspaceId: workspaceId as string || '',
    page: normalizeInt(page, 0),
    pageSize: normalizeInt(pageSize, 0),
    sortField: sortField as string || '',
    sortDirection: sortDirection as 'asc' | 'desc' || ''
  }

  return ret
}

export const getQueryParamsString = (queryParams: any) => {

  const normalizedQueryParams = {
    q: queryParams.query,
    tag: queryParams.tags,
    ws: queryParams.workspaceId || null,
    p: queryParams.page || null,
    ps: queryParams.pageSize || null,
    sort: queryParams.sortField || null,
    dir: queryParams.sortDirection || null
  }

  const queryParamsStr = queryString.stringify(normalizedQueryParams, { skipNull: true, })
  return queryParamsStr ? `?${queryParamsStr}` : ''
}


export const navigateByQueryParams = ({ queryParams, path, navigate  }: { queryParams: any, path: string, navigate: NavigateFunction  }) => {
  let queryString = ''

  if (Object.keys(queryParams).length !== 0) {
    queryString = getQueryParamsString(queryParams)
  }

  navigate(`${path}${queryString}`)
}
