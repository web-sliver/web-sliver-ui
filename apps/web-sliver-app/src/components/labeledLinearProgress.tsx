import { Box, LinearProgress, Typography } from '@mui/material'


const styles = {
  root: {
    display: 'flex',
    alignItems: 'center'
  },

  progressBox: {
    width: '100%',
    marginRight: 10,
  },

  percentBox: {
    minWidth: 35,
  }
}


const LabeledLinearProgress = <T extends { value: number }>({ value, ...props }: T) => {
  return (
    <Box sx={styles.root}>

      <Box sx={styles.progressBox}>
        <LinearProgress variant="determinate" value={value} {...props} />
      </Box>

      <Box sx={styles.percentBox}>
        <Typography variant="body2">{`${Math.round(value)}%`}</Typography>
      </Box>
    </Box>
  )
}

export default LabeledLinearProgress
