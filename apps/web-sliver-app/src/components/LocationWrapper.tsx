import { Route, Routes, useLocation, useNavigate } from 'react-router-dom'
import { CssBaseline, ThemeProvider } from '@mui/material'

import { PrivateRoute } from '~/components/routes/PrivateRoute'
import theme from '~/theme'
import { buildLocationContext, LocationContext } from '~/components/contexts/locationContext'
import { EntriesPage } from '~/pages/EntriesPage'
import { TagsPage } from '~/pages/TagsPage'
import { PreferencesPage } from '~/pages/PreferencesPage'
import { useAuth0 } from '@auth0/auth0-react'
import { ErrorPage } from '~/pages/ErrorPage'
import Redirect from './redirect'


export const LocationWrapper = () => {
  const location = useLocation()
  const navigate = useNavigate()
  const { error } = useAuth0()

  return (
    <LocationContext.Provider value={buildLocationContext({ location, navigate })}>
      <ThemeProvider theme={theme} >
        <CssBaseline />
        <Routes>
          <Route
            path="/"
            element={error ? <ErrorPage /> : <PrivateRoute component={EntriesPage} onRedirecting={() => <Redirect />} />}
          />
          <Route
            path="/tags"
            element={error ? <ErrorPage /> : <PrivateRoute component={TagsPage} />}
          />
          <Route
            path="/preferences"
            element={error ? <ErrorPage /> : <PrivateRoute component={PreferencesPage} />}
          />
        </Routes>
      </ThemeProvider>
    </LocationContext.Provider>
  )
}
