import React from "react"
import { FormControl, InputLabel, MenuItem, Select, SxProps } from "@mui/material"

import { useWorkspaces } from "~/services/swr_hooks"
import { useInput } from "web-sliver-ui-components"


const styles = {
  inputField: {
    // width: '500px',
  },
}


export interface IWorkspaceSelect {
  sx?: SxProps,
  allowEmpty?: boolean,
  emptyName?: string,
}

const WorkspaceSelect = ({
  sx = {},
  workspaces,
  inputProps,
  allowEmpty = false,
  emptyName = '<empty>',
}: IWorkspaceSelect & {
  workspaces: any[],
  inputProps: any,
}) => (
  <FormControl sx={{...sx}}>
    <InputLabel id="workspace-select-label">Workspace</InputLabel>
    <Select
      sx={{ ...styles.inputField, }}
      labelId='workspace-select-label'
      label='Workspace'
      variant='outlined'
      {...inputProps.bindWorkspace}
    >
      {allowEmpty && <MenuItem
        key={-1}
        disabled={-1 === inputProps.selectedWorkspaceIndex}
        selected={-1 === inputProps.selectedWorkspaceIndex}
        onClick={(_event) => inputProps.setSelectedWorkspaceIndex(-1)}
        value={-1}
      >
        {emptyName}
      </MenuItem>}
      {(workspaces || []).map((option: any, index: number) => (
        <MenuItem
          key={option.uid}
          disabled={index === inputProps.selectedWorkspaceIndex}
          selected={index === inputProps.selectedWorkspaceIndex}
          onClick={(_event) => inputProps.setSelectedWorkspaceIndex(index)}
          value={index}
        >
          {`${option.name}${option.default ? ' (default)' : ''}`}
        </MenuItem>
      ))}
    </Select>
  </FormControl>
)

export const useWorkspaceSelectOffline = ({
  workspaces: initialWorkspaces = [],
  allowEmpty = false,
  emptyName = '<empty>',
}: {
  workspaces?: any[],
  allowEmpty?: boolean,
  emptyName?: string,
}) => {
  const [workspaces, setWorkspaces] = React.useState<any[]>(initialWorkspaces)
  const { value: selectedWorkspaceIndex, setValue: setSelectedWorkspaceIndex, bind: bindWorkspace } = useInput('')
  const inputProps = { selectedWorkspaceIndex, setSelectedWorkspaceIndex, bindWorkspace }

  return {
    selectedWorkspaceIndex,
    workspaces,
    setSelectedWorkspaceIndex,
    setWorkspaces,
    selectedWorkspace: selectedWorkspaceIndex === -1 ? null : workspaces[selectedWorkspaceIndex],
    setSelectedWorkspace: (workspaceId: string) => {
      const workspaceIndex = (workspaces as any[]).map((workspace: any) => workspace.uid).indexOf(workspaceId)

      setSelectedWorkspaceIndex(workspaceIndex === -1 ? 0 : workspaceIndex)
    },

    component: (props: IWorkspaceSelect) => WorkspaceSelect({ allowEmpty, emptyName, workspaces, inputProps, ...props })
  }
}

export const useWorkspaceSelect = ({
  allowEmpty = false,
  emptyName = '<empty>',
}: {
  allowEmpty?: boolean,
  emptyName?: string,
}) => {
  const { data: workspaces, isLoading: workspacesLoading } = useWorkspaces()
  const { value: selectedWorkspaceIndex, setValue: setSelectedWorkspaceIndex, bind: bindWorkspace } = useInput('')
  const inputProps = { selectedWorkspaceIndex, setSelectedWorkspaceIndex, bindWorkspace }

  return {
    workspaces,
    workspacesLoading,
    selectedWorkspaceIndex,
    setSelectedWorkspaceIndex,
    selectedWorkspace: selectedWorkspaceIndex === -1 ? null : workspaces[selectedWorkspaceIndex],
    setSelectedWorkspace: (workspaceId: string) => {

      if (workspacesLoading) {
        setSelectedWorkspaceIndex('')
        return
      }

      const workspaceIndex = (workspaces as any[]).map((workspace: any) => workspace.uid).indexOf(workspaceId)

      setSelectedWorkspaceIndex(workspaceIndex === -1 ? 0 : workspaceIndex)
    },

    component: (props: IWorkspaceSelect) => workspacesLoading ? null : WorkspaceSelect({ allowEmpty, emptyName, workspaces, inputProps, ...props })
  }
}
