import { withAuthenticationRequired } from "@auth0/auth0-react"


export const PrivateRoute = ({ component, ...args }: any) => {
  const Component = withAuthenticationRequired(component, args)
  return <Component />
}
