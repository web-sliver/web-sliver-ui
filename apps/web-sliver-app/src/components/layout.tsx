import Header from "~/components/header"
import { Box, Container, Link } from "@mui/material"
import { settings } from "~/config"


const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
  },

  content: {
    flexGrow: 1,
    padding: 1,
    maxWidth: '100%',
  },
}


const Layout = ({ children, subTitle, }: {
  children: any,
  subTitle: string,
}) => {

  return (
    <Container maxWidth="xl">
      <Box my={1}>
        <Box sx={styles.root}>
          <Header siteTitle='WebSliver' subTitle={subTitle} />

          <Box sx={styles.content}>
            {children}
          </Box>

          <footer>
            Copyright © 2019-{new Date().getFullYear()}
            {` `}
            <Link href={settings.WEBSLIVER_APP_URL}>Dystopian Side</Link>
          </footer>
        </Box>
      </Box>
    </Container>
  )
}

export default Layout
