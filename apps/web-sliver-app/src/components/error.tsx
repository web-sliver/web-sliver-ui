import { Typography, styled } from "@mui/material"
import { useAuth0 } from "@auth0/auth0-react"


const RootDiv = styled('div')({
  width: '100%'
})

const Error = ({ message = '' }: { message?: string }) => {

  const { error } = useAuth0()
  return (
    <RootDiv sx={{ height: '100px', display: 'flex', justifyContent: 'center', alignItems: 'center', flexDirection: 'column' }}>
      <Typography sx={{ fontSize: '1.5em' }}>An error has occurred: </Typography>
      <Typography sx={{ color: 'text.secondary', fontSize: '2em' }}>{message || error?.message || 'Server fault.'}</Typography>
    </RootDiv>

  )
}

export default Error
