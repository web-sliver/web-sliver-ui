import React from "react"
import { Dialog, DialogTitle, DialogContent, DialogActions, Button, Typography, List, Link } from "@mui/material"

export const FailedListDialog = ({ isOpen, title = 'Failed to open items', content = [], onClose }: {
  isOpen: boolean,
  title?: string,
  content?: any[],
  onClose: () => void
}) => {

  return content.length === 0 ? null : (
    <Dialog open={isOpen} onClose={onClose} aria-labelledby="ws-failed-dialog-title">
      <DialogTitle id="ws-failed-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <Typography variant="body1">The following links could not be opened. Please open them manually:</Typography>
        <List>
          {content
            .filter(item => !!item.location)
            .map((item, index) => (
              <Link
                href={item.location}
                sx={{ color: 'inherit', textDecoration: 'none' }}
                rel="_noreferrer"
                key={index}
                onClick={(e: React.MouseEvent) => e.stopPropagation()}
              >
                <Typography color="primary" sx={{ fontSize: 18, fontWeight: 'medium', pb: 0.5 }}>{item.location}</Typography>
              </Link>
            ))}
        </List>

      </DialogContent>

      <DialogActions>
        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
