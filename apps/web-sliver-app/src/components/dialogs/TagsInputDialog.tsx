import React from "react"
import { Button, Dialog, DialogActions, DialogContent } from "@mui/material"

import { EntrySearchInput, SearchInput, SearchOptionType } from 'web-sliver-ui-components'

import { useMatchingTags } from "~/services/swr_hooks"


const styles = {
  dialogRoot: {
    width: '1000px',
  },
}

export const TagsInputDialog = ({ isOpen, workspaceId, onSave, onDelete, onClose }: {
  isOpen: boolean,
  workspaceId: string,
  onSave: (tags: string[]) => any,
  onDelete: (tags: string[]) => any,
  onClose: () => any,
}) => {
  const [tags, setTags] = React.useState([] as any[])

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []

  const setSearchInput = async (input: EntrySearchInput[]) => {
    setTags(input.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name))
  }

  return (
    <Dialog maxWidth="xl" open={isOpen} onClose={onClose} aria-labelledby="dialog-title">
      <DialogContent sx={styles.dialogRoot}>
        <SearchInput
          label="Tags"
          getOptions={useOptions}
          value={tags.map((tag: string) => ({ name: tag, type: SearchOptionType.TAG }))}
          setValue={setSearchInput}
        />
      </DialogContent>

      <DialogActions>
        <Button color="inherit" onClick={() => onSave(tags)}>
          Add
        </Button>
        <Button color="inherit" onClick={() => onDelete(tags)}>
          Delete
        </Button>

        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
