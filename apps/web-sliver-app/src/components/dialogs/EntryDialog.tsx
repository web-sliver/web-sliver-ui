import React from "react"
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormGroup, TextField, Typography } from "@mui/material"
import _ from 'lodash'

import { EntrySearchInput, SearchInput, SearchOptionType, useInput } from 'web-sliver-ui-components'

import { useMatchingTags } from "~/services/swr_hooks"
import { dateToString } from "~/utils"


const styles = {
  contentForm: {
    display: 'flex',
    flexDirection: 'column',
    gap: 1,
  },

  inputField: {
    width: 1000,
  },
}

export const EntryDialog = ({ isOpen, item, workspaceId, onSave, onDelete, onClose }: {
  isOpen: boolean,
  item?: any,
  workspaceId: string,
  onSave: Function,
  onDelete?: (uid: string) => void,
  onClose: () => void,
}) => {
  const { value: title, setValue: setTitle, bind: bindTitle } = useInput(item?.title || '')
  const { value: location, setValue: setLocation, bind: bindLocation } = useInput(item?.location || '')
  const { value: description, setValue: setDescription, bind: bindDescription } = useInput(item?.description || '')
  const [showClearAll, setShowClearAll] = React.useState(false)
  const isEditing = !!item

  const [tags, setTags] = React.useState([] as any[])

  React.useEffect(() => {
    // set/reset fields on load and on re-open
    if (isOpen) {
      setTitle(item?.title || '')
      setLocation(item?.location || '')
      setDescription(item?.description || '')
      setTags(_.sortBy(item ? item.tags : []))
    }

  }, [item, isOpen, setTitle, setLocation, setDescription, setTags])

  React.useEffect(() => {
    setShowClearAll(title || location || description || tags.length !== 0)
  }, [title, location, description, tags])

  const useOptions = (query: string) => useMatchingTags({
    workspaceId: workspaceId || '',
    value: query ? `.*${query}.*` : ''
  })?.data || []

  const setSearchInput = async (input: EntrySearchInput[]) => {
    setTags(input.map((item: EntrySearchInput) => typeof item === 'string' ? item : item.name))
  }

  const save = () => {
    onSave({ uid: item?.uid, title, location, description, workspace: workspaceId, tags })
  }

  const clearAllFields = () => {
    setTitle('')
    setLocation('')
    setTags([])
  }

  //? Autocomplete alternative options:
  //?   InputProps={{ ...params.InputProps, disableUnderline: true }}

  return (
    <Dialog maxWidth="xl" open={isOpen} onClose={onClose} aria-labelledby="dialog-title">
      <DialogTitle id="dialog-title">
        {isEditing ? 'Edit item' : 'Add new item'}
      </DialogTitle>
      <DialogContent>
        <FormGroup sx={styles.contentForm}>
          <TextField
            sx={styles.inputField}
            margin='dense'
            label="Title"
            variant="outlined"
            {...bindTitle}
          />
          <TextField
            sx={styles.inputField}
            margin='dense'
            label="Location"
            variant="outlined"
            {...bindLocation}
          />

          <TextField
            sx={styles.inputField}
            margin='dense'
            label="Description"
            variant="outlined"
            multiline
            maxRows={5}
            {...bindDescription}
          />

          <SearchInput
            getOptions={useOptions}
            value={tags.map((tag: string) => ({ name: tag, type: SearchOptionType.TAG }))}
            label="Tags"
            disabledTypes={[SearchOptionType.EXCLUSION, SearchOptionType.WILDCARD]}
            setValue={setSearchInput}
            sx={{ pt: 1, ...styles.inputField }}
          />

          {showClearAll && <Button color="inherit" onClick={clearAllFields}>
            Clear all fields
          </Button>}
        </FormGroup>

        <br />

        {isEditing && <>
          <Typography variant="body2" color="textSecondary" component="span">Created: {dateToString(item?.created_at)}</Typography>
          <br />
          <Typography variant="body2" color="textSecondary" component="span">Modified: {dateToString(item?.updated_at)}</Typography>
        </>}
      </DialogContent>

      <DialogActions>
        <Button color="inherit" onClick={save}>
          Save
        </Button>

        {isEditing && <Button color="inherit" onClick={() => onDelete && onDelete(item)}>
          Delete
        </Button>}

        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
