import React from "react"
import { Dialog, DialogTitle, DialogContent, DialogActions, Button } from "@mui/material"

import WorkspaceEditDialog from "./workspaceEditDialog"
import WorkspacesList from "~/components/workspacesList"


export const WorkspaceManageDialog = ({ isOpen, title = 'Manage workspaces', content = null, workspaces, onSelect, onClose, refreshWorkspaces }: {
  isOpen: boolean,
  title?: string,
  content?: any,
  workspaces: any[],
  refreshWorkspaces: () => void,
  onSelect: (workspaceId: string) => void,
  onClose: () => void
}) => {

  const [editDialogOpen, setEditDialogOpen] = React.useState(false)
  const [selectedWorkspace, setSelectedWorkspace] = React.useState(null)

  const handleEdit = async (_ev: any, workspace: any) => {
    setSelectedWorkspace(workspace)
    setEditDialogOpen(true)
  }

  const handleSave = async () => {
    setSelectedWorkspace(null)
    setEditDialogOpen(true)
  }

  return (
    <Dialog open={isOpen} onClose={onClose} aria-labelledby="ws-mgr-dialog-title">
      <DialogTitle id="ws-mgr-dialog-title">{title}</DialogTitle>
      <DialogContent>

        {content}

        <WorkspacesList
          workspaces={workspaces}
          onItemClick={(_ev: any, workspace: any) => onSelect(workspace)}
          onItemEdit={handleEdit}
        />

        <WorkspaceEditDialog
          isOpen={editDialogOpen}
          item={selectedWorkspace}
          refreshWorkspaces={refreshWorkspaces}
          onClose={() => setEditDialogOpen(false)} />
      </DialogContent>

      <DialogActions>
        <Button color="inherit" onClick={handleSave}>
          Add New Workspace
        </Button>

        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
