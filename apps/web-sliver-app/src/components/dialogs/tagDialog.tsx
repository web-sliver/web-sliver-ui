import React from "react"
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, FormGroup, TextField, Typography } from "@mui/material"

import { useInput } from "web-sliver-ui-components"


const styles = {
  inputField: {
    width: 1000,
  },
}

export const TagDialog = ({ isOpen, item, onSave, onDelete, onClose }: {
  isOpen: boolean,
  item?: any,
  onSave: Function,
  onDelete?: (uid: string) => void,
  onClose: () => void,
}) => {
  const { value: name, setValue: setName, bind: bindName } = useInput(item?.name)

  React.useEffect(() => {
    // set/reset fields on load and on re-open
    if (isOpen) {
      setName(item?.name || '')
    }
  }, [item, isOpen, setName])  // using isOpen as trigger to wipe the dialog on re-open

  return (
    <Dialog maxWidth="xl" open={isOpen} onClose={onClose} aria-labelledby="dialog-title">
      <DialogTitle id="dialog-title">Edit item</DialogTitle>

      <DialogContent>
        <Typography color='primary' sx={{ p: 2 }}>
          Note: renaming a tag the same as an existing tag will merge them.
        </Typography>
        <FormGroup>
          <TextField
            sx={styles.inputField}
            margin='dense'
            label="Name"
            variant="outlined"
            {...bindName}
          />
        </FormGroup>
        <br />
        <Typography variant="body2" color="textSecondary" component="span">{item?.uses} entries using this tag.</Typography>
      </DialogContent>

      <DialogActions>
        <Button color="inherit" onClick={() => onSave({ oldTag: item, newTag: { name } })}>
          Save
        </Button>

        {item && <Button color="inherit" onClick={() => onDelete && onDelete(item)}>
          Delete
        </Button>}

        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}
