import React from "react"
import { Checkbox, Dialog, DialogTitle, DialogContent, TextField, DialogActions, FormControlLabel, FormGroup, Button, Tooltip } from '@mui/material'


import { useInput, useCheckbox } from 'web-sliver-ui-components'
import * as dataService from 'web-sliver-data-interface'
import { useAuthToken } from '~/hooks/authTokenHook'
import { settings } from "~/config"
import { useOkCancelConfirmationPopover } from '~/components/dialogs/confirmationPopover'


const styles = {
  inputField: {
    width: 300,
  },
}

const WorkspaceEditDialog = ({ isOpen, item, onClose, refreshWorkspaces }: {
  isOpen: boolean,
  item: any,
  refreshWorkspaces: () => void
  onClose: () => void
}) => {
  const { token } = useAuthToken()
  const { value: itemName, setValue: setName, bind: bindItemName } = useInput(item?.name || '')
  const { value: isDefault, setValue: setIsDefault, bind: bindIsDefault } = useCheckbox(!!item?.default)
  const host = settings.WEBSLIVER_API_URL

  const [OkCancelState, setOkCancelState] = React.useState<{ action: Function, message: string }>({ action: () => { }, message: '' })
  const { component: OkCancelConfirm, openFor: openOkCancelConfirm } = useOkCancelConfirmationPopover({
    onOK: OkCancelState.action, options: [], message: OkCancelState.message,
  })

  React.useEffect(() => {
    if (item) {
      setName(item?.name)
      setIsDefault(item?.default)
    }
  }, [item, setName, setIsDefault])

  const handleSave = async () => {
    if (isDefault) {
      await dataService.resetDefaultWorkspace({ host, token })
    }

    if (item) {
      await dataService.updateWorkspace({
        workspace: {
          uid: item.uid,
          name: itemName,
          default: isDefault,
        },
        host,
        token
      })

    } else {
      await dataService.insertWorkspace({
        workspace: {
          name: itemName,
          default: isDefault,
        },
        host,
        token
      })
    }

    refreshWorkspaces()
    onClose()
  }

  const handleDelete = async (event: any) => {
    setOkCancelState({
      message: 'Are you sure you want to delete this workspace?',
      action: async () => {
        await dataService.deleteWorkspace({ workspaceId: item.uid, host, token })

        refreshWorkspaces()
        onClose()
      }
    })

    openOkCancelConfirm(event?.currentTarget)
  }

  return (
    <Dialog open={isOpen} onClose={onClose} aria-labelledby="ws-edit-dialog-title">
      <DialogTitle id="ws-edit-dialog-title">
        {item ? `Edit: ${item.name}` : 'Add new workspace'}
      </DialogTitle>
      <DialogContent>
        <FormGroup>
          <TextField
            sx={styles.inputField}
            margin='dense'
            label="Name"
            variant="outlined"
            {...bindItemName}
          />
          <FormControlLabel
            control={
              <Checkbox
                color="primary"
                {...bindIsDefault}
              />
            }
            label="Default"
          />
        </FormGroup>
        <OkCancelConfirm />
      </DialogContent>
      <DialogActions>
        <Tooltip title="Save workspace">
          <Button color="inherit" onClick={handleSave}>
            Save
          </Button>
        </Tooltip>

        {item &&
          <Tooltip title="Delete workspace">
            <Button color="inherit" onClick={handleDelete}>
              Delete
            </Button>
          </Tooltip>
        }

        <Button color="inherit" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  )
}


export default WorkspaceEditDialog
