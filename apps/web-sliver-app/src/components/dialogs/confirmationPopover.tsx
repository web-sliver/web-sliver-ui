import React from 'react'
import { Box, Button, Popover, Typography } from "@mui/material"


export interface ConfirmationOption {
  title: string,
  action: Function,
  keepOpen?: boolean,
  color?: any,
}

export interface IConfirmation {
  options: ConfirmationOption[],
  message: string,
  anchorEl?: Element | null,
  onClose?: Function,
}

const Confirmation = ({ options, message, anchorEl, onClose }: IConfirmation) => {

  return (
    <Popover
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      onClose={(props: any) => onClose && onClose(props)}
    >
      <Box sx={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'end',
        maxWidth: 250,
      }}>
        <Typography sx={{ p: 2, pb: 1, overflowWrap: 'break-word', textAlign: 'justify' }}>{message}</Typography>

        <Box sx={{
          p: 1,
          display: 'flex',
          gap: 1,
        }}>
          {options.map((option: ConfirmationOption, index: number) => {
            return (
              <Button
                key={index}
                size='small'
                color={option.color || 'inherit'}
                variant='contained'
                onClick={() => option.action()}
              >
                {option.title}
              </Button>
            )
          })}
        </Box>
      </Box>
    </Popover>
  )
}

export const useConfirmationPopover = ({ anchorEl: initialAnchorEl, onClose, ...props }: IConfirmation) => {
  const [anchorEl, setAnchorEl] = React.useState<Element | null>(initialAnchorEl || null)

  const close = (props?: any) => {
    onClose && onClose(props || {})

    setAnchorEl(null)
  }

  const openFor = (anchor: Element) => setAnchorEl(anchor)

  return {
    close,
    openFor,
    component: () => (
      <Confirmation
        {...props}
        onClose={close}
        anchorEl={anchorEl}
      />
    )
  }
}

export const useOkConfirmationPopover = (props: IConfirmation) => {
  const { options, ...rest } = props

  const { close, ...outputRest } = useConfirmationPopover({
    options: [
      ...options,
      { title: 'Ok', action: () => close(), color: 'primary' },
    ],
    ...rest
  })

  return { close, ...outputRest }
}

export const useOkCancelConfirmationPopover = ({ onOK, ...props }: {
  onOK: Function,
} & IConfirmation) => {

  const { options, ...rest } = props

  const { close, ...outputRest } = useConfirmationPopover({
    options: [
      ...options,
      { title: 'Cancel', action: () => close(), color: 'error' },
      { title: 'Ok', action: () => { onOK(); close() }, color: 'primary' },
    ],
    ...rest
  })

  return { close, ...outputRest }
}

export default Confirmation
