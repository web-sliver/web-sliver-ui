import React from "react"
import { IconButton, List, ListItem, ListItemSecondaryAction, SxProps, Tooltip } from "@mui/material"
import { Edit as EditIcon, Star as DefaultWorkspaceIcon } from "@mui/icons-material"

import { StyledLink } from 'web-sliver-ui-components'

import { LocationContext } from "./contexts/locationContext"

import { getQueryParamsString } from "./contexts/locationContextUtils"


const styles = {
  workspacesList: {
    maxHeight: '100%',
  },

  defaultWorkspaceIcon: {
    minWidth: '25px'
  },

  workspaceTextOverflow: {
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis'
  },

  menuLink: {
    width: '90%'
  },
}

const WorkspacesList = ({ workspaces, renderLinks = false, onItemClick, onItemEdit, sx = {} }: {
  workspaces: any[],
  renderLinks?: boolean,
  onItemClick: (event: any, workspace: any) => any,
  onItemEdit: (event: any, workspace: any) => any,
  sx?: SxProps
}) => {
  const { location, queryParams } = React.useContext(LocationContext)

  return (
    <List sx={{ ...styles.workspacesList, ...sx }} dense >
      {workspaces.map((workspace: any) => (

        <ListItem
          selected={workspace.uid === queryParams.workspaceId}
          key={workspace.uid}
          onClick={(event: any) => onItemClick(event, workspace)}
          sx={{
            '&:hover': {
              backgroundColor: 'rgba(255, 255, 255, 0.08)'
            }
          }}
        >
          {workspace.default && <DefaultWorkspaceIcon sx={styles.defaultWorkspaceIcon} />}
          <Tooltip title={`${workspace.entriesCount || 0} entries`}>
            <StyledLink
              to={renderLinks ? `${location.pathname}${getQueryParamsString({ workspaceId: workspace.uid })}` : `${location.pathname}${location.search}`}
              style={{
                color: `white`,
                textDecoration: `none`,
              }}
              sx={{ ...styles.workspaceTextOverflow, ...styles.menuLink }}
            >
              {workspace.name}
            </StyledLink>
          </Tooltip>

          <ListItemSecondaryAction key={workspace.uid}>
            <Tooltip title="Edit workspace">
              <IconButton edge="end" aria-label="edit" onClick={(ev: any) => { ev.preventDefault(); onItemEdit(ev, workspace) }}>
                <EditIcon />
              </IconButton>
            </Tooltip>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  )
}

export default WorkspacesList
