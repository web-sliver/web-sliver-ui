import { useAuth0 } from '@auth0/auth0-react'
import useSWR from 'swr'

import * as dataService from 'web-sliver-data-interface'
import { IWorkspace } from 'web-sliver-ui-components'
import { settings } from '~/config'
import { getToken } from '~/services/auth'


export enum SWRKey {
  ENTRIES = '/api/entries',
  ENTRIES_COUNT = '/api/entries/count',
  TAGS = '/api/tags',
  TAGS_COUNT = '/api/tags/count',
  WORKSPACES = '/api/workspaces',
}


export const fetcher = async (key: SWRKey, ...params: [any]) => {
  const token = getToken()
  const host = settings.WEBSLIVER_API_URL

  // console.debug(key, params)

  if (!token) {
    console.debug('Token not found.')
  }

  switch(key) {
    case SWRKey.ENTRIES: {
      const [workspaceId, searchTerm, offset, limit, sortField, sortDirection, isAuthenticated] = params as any
      if  ( !isAuthenticated ) {
        return []
      }

      return await dataService.getEntries({
        query: { workspaceId, searchTerm, offset, limit, sortField, sortDirection }, host, token
      })
    }

    case SWRKey.ENTRIES_COUNT: {
      const [workspaceId, searchTerm, isAuthenticated] = params as any
      if  ( !isAuthenticated ) {
        return 0
      }

      return (await dataService.getEntriesCount({ workspaceId, searchTerm, host, token }))
    }

    case SWRKey.TAGS: {
      const [workspaceId, value, offset, limit, sortField, sortDirection, isAuthenticated] = params as any
      if  ( !isAuthenticated ) {
        return []
      }

      return (await dataService.getMatchingTags({
        workspaceId, value, offset, limit, sortField, sortDirection, host, token
      })).sort()
    }

    case SWRKey.TAGS_COUNT: {
      const [workspaceId, value, isAuthenticated ] = params as any
      if  ( !isAuthenticated ) {
        return 0
      }

      return (await dataService.getMatchingTagsCount({ workspaceId, value, host, token }))
    }

    case SWRKey.WORKSPACES:
      const [ isAuthenticated ] = params as any
      if  ( !isAuthenticated ) {
        return []
      }

      return await dataService.getWorkspaces({ host, token })

    default:
      console.error(`Undefined SWR key: ${key}`)
      return undefined
  }
}


export const useEntries = (
  { workspaceId, searchTerm, offset, limit, sortField, sortDirection }:
  {
    workspaceId: string,
    searchTerm?: dataService.EntrySearchInput[],
    offset?: number,
    limit?: number,
    sortField?: string,
    sortDirection?: string
  }) => {
  const { isAuthenticated } = useAuth0()
  const { data, error, mutate } = useSWR([SWRKey.ENTRIES, workspaceId, searchTerm, offset, limit, sortField, sortDirection, isAuthenticated], fetcher)

  return {
    data: data as dataService.Entry[] || [],
    isLoading: !error && !data,
    isError: error,
    mutate
  }
}


export const useMatchingTags = (
  { workspaceId, value, offset, limit, sortField, sortDirection }: {
    workspaceId: string,
    value: string,
    offset?: number,
    limit?: number,
    sortField?: string,
    sortDirection?: string
  }) => {
  const { isAuthenticated } = useAuth0()
  const { data, error, mutate } = useSWR([SWRKey.TAGS, workspaceId, value, offset, limit, sortField, sortDirection, isAuthenticated], fetcher)

  return {
    data: data as dataService.TagType[] || [],
    isLoading: !error && !data,
    isError: error,
    mutate
  }
}

export const useMatchingTagsCount = ({ workspaceId, value }: { workspaceId: string, value: string }) => {
  const { isAuthenticated } = useAuth0()
  const { data, error, mutate } = useSWR([SWRKey.TAGS_COUNT, workspaceId, value, isAuthenticated], fetcher)

  return {
    data: data === undefined ? 0 : data as number,
    isLoading: !error && data === undefined,
    isError: error,
    mutate
  }
}

export const useEntriesCount = (workspaceId: string, searchTerm?: dataService.EntrySearchInput[]) => {
  const { isAuthenticated } = useAuth0()
  const { data, error, mutate } = useSWR([SWRKey.ENTRIES_COUNT, workspaceId, searchTerm, isAuthenticated], fetcher)

  return {
    data: data === undefined ? 0 : data as number,
    isLoading: !error && data === undefined,
    isError: error,
    mutate
  }
}


export const useWorkspaces = () => {
  const { isAuthenticated } = useAuth0()
  const { data, error, mutate } = useSWR([SWRKey.WORKSPACES, isAuthenticated], fetcher)

  return {
    data: data as IWorkspace[] || [],
    defaultWorkspace: (data as any[] || []).find((workspace: any) => workspace.default),
    isLoading: !error && !data,
    isError: error,
    mutate
  }
}
