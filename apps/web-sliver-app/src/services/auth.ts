import { settings } from "~/config"


export const getToken = () => {
  if (typeof window !== 'undefined') {
    const authStorage = localStorage.getItem(`@@auth0spajs@@::${settings.WEBSLIVER_AUTH_CLIENT_ID}::default::openid profile email`) || '{"body": {}}'
    return JSON.parse(authStorage)?.body.id_token
  }
}
