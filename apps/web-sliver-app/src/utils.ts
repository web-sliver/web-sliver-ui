import moment from "moment"


export const sleep = (ms: number) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

export const DEFAULT_PAGE_SIZE = 25

export const boundsConverter = ({ total, page, pageSize }: {total: number, page: number, pageSize: number}) => {
  pageSize = pageSize || DEFAULT_PAGE_SIZE
  let lastPageSize = total % pageSize
  lastPageSize = lastPageSize === 0 ? pageSize : lastPageSize

  let maxPage = (total - lastPageSize) / pageSize
  maxPage = maxPage <= 0 ? 1 : maxPage

  page = page < 0 ? 0 : page > maxPage ? maxPage : page

  let offset = page * pageSize

  return {
    offset,
    limit: pageSize,
    maxPage,
    lastPageSize,
    pageStart: offset,
    pageEnd: page === maxPage ? offset + lastPageSize : offset + pageSize
  }
}

export const dateToString = (date: any) => moment(date).format('D MMM YYYY, HH:mm:ss')
export const dateFromNow = (date: any) => moment(date).fromNow()

export const toggleDirection = (direction: string) => direction === 'asc' ? 'desc' : 'asc'
export const determineSortingDirection = (direction: string, fieldChanged: boolean) => fieldChanged ? 'asc' : toggleDirection(direction)
