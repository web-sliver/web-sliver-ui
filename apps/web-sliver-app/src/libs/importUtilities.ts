import { InputEntry, Entry } from "web-sliver-data-interface"


const readFile = async ({ file, parseJSON = true }: { file: Blob, parseJSON?: boolean }) => {
  return new Promise<any>((resolve, reject) => {
    const reader = new FileReader()

    reader.onload = (event) => {
      try {

        let content = (event.target?.result || '') as string

        if (parseJSON) {
          resolve(JSON.parse(content))
        } else {
          resolve(content)
        }

      } catch (error) {
        reject(error)
      }
    }
    reader.onerror = (error) => {
      reject(error)
    }

    reader.readAsText(file)
  })
}

export class WebSliverJsonParser {

  processContent = ({
    content,
    selectedWorkspaces = [],
    additionalTags
  }: {
    content: any,
    selectedWorkspaces?: string[],
    additionalTags: string[]
  }): any[] => {

    let results: InputEntry[] = []

    const workspaces = selectedWorkspaces.length === 0
      ? content.data.workspaces
      : content.data.workspaces.filter((workspace: any) => selectedWorkspaces.includes(workspace.uid))

    for (const workspace of workspaces) {

      results.push({
        ...workspace,
        // eslint-disable-next-line
        entries: workspace.entries.map((entry: Entry) => ({
          ...entry,
          workspace_id: workspace.uid,
          tags: Array.from(new Set([...entry.tags, ...additionalTags]))
        })),
      })
    }

    return results
  }

  public parseFile = async ({
    file, selectedWorkspaces, additionalTags
  }: {
    file: Blob, selectedWorkspaces?: string[], additionalTags: string[]
  }): Promise<any[]> => {
    const content = await readFile({ file })

    return this.processContent({ content, selectedWorkspaces, additionalTags })
  }

  public parseRawJsonFile = async ({ file }: { file: Blob }) => {
    return await readFile({ file })
  }
}

export class FirefoxJsonParser {

  formatEntry = ({ item, currentTag, additionalTags }: {
    item: any,
    currentTag: any,
    additionalTags: string[]
  }): InputEntry => ({
    title: (item.title || '').trim(),
    description: (item.description || '').trim(),
    location: (item.uri || '').trim(),
    favIconUrl: (item.iconuri || '').trim(),
    workspace_id: '',
    tags: [...additionalTags, currentTag],
    _raw: item,
    ...!item.dateAdded ? null : {
      created_at: new Date(item.dateAdded / 1000)
    },
    ...!item.lastModified ? null : {
      updated_at: new Date(item.lastModified / 1000)
    },
  })

  processContent = ({
    item,
    currentTag = '',
    additionalTags
  }: {
    item: any,
    currentTag?: string,
    additionalTags: string[]
  }): InputEntry[] => {
    // converts an import to a list of entries

    switch (item.type) {
      case 'text/x-moz-place-container':
        const title = item.title.replaceAll('.', ' ').trim()
        const innerTag = currentTag ? `${currentTag}.${title}` : title
        let result: any[] = []

        if (item.children) {
          item.children.forEach((child: any) => result = result.concat(this.processContent({
            item: child,
            currentTag: innerTag,
            additionalTags
          })))
        } else {
          // this is sometimes a tag, an empty folder or a weird bookmark. import them as entries and let the user decide
          // what to do with them.
          result = [this.formatEntry({ item, currentTag, additionalTags })]
        }

        return result

      case 'text/x-moz-place':
        return [this.formatEntry({ item, currentTag, additionalTags })]

      case 'text/x-moz-place-separator':
        // do nothing for now
        return []

      default:
        console.error(`Unknown bookmark type ${item.type} found.`)
        return []
    }
  }

  public parseFile = async ({
    file, additionalTags
  }: {
    file: Blob, additionalTags: string[]
  }): Promise<InputEntry[]> => {
    const content = await readFile({ file })

    return this.processContent({ item: content, additionalTags })
  }
}

export class TextParser {

  formatEntry = ({ item, additionalTags }: {
    item: any,
    additionalTags: string[]
  }): InputEntry => ({
    title: (item.title || '').trim(),
    description: '',
    location: (item.uri || '').trim(),
    favIconUrl: '',
    workspace_id: '',
    tags: additionalTags,
    _raw: item,
    ...!item.dateAdded ? null : {
      created_at: new Date(item.dateAdded / 1000)
    },
    ...!item.lastModified ? null : {
      updated_at: new Date(item.lastModified / 1000)
    },
  })

  tryParseMarkdown = (content: string): { title: string, uri: string } | null => {
    const matches = content.match(/\[(.*)\]\((.*)\)/)

    if (matches) {
      return {
        title: matches[1],
        uri: matches[2]
      }
    }

    return null
  }

  trySimpleTitleUrl = (content: string): { title: string, uri: string } | null => {
    // finds: <title> - <url>
    const matches = content.match(/\s*(.*)\s+-\s+(https?:\/\/[^\s]+)\s*/)

    if (matches) {
      return {
        title: matches[1],
        uri: matches[2]
      }
    }

    return null
  }

  trySimpleUrl = (content: string): { title: string, uri: string } | null => {
    // finds: <url>
    const matches = content.match(/(https?:\/\/[^\s]+)/)

    if (matches) {
      return {
        title: '',
        uri: matches[1]
      }
    }

    return null
  }

  processContent = ({
    content,
    additionalTags
  }: {
    content: any,
    currentTag?: string,
    additionalTags: string[]
  }): InputEntry[] => {

    let lines = content.split('\n').filter((line: string) => line.trim().length > 0)

    const items: { title: string, uri: string }[] = lines.map((line: string) => {
      // todo: maybe implement a way to also extract tags
      const markdownResult = this.tryParseMarkdown(line)

      if (markdownResult) {
        return markdownResult
      }

      const simpleTitleUrlResult = this.trySimpleTitleUrl(line)

      if (simpleTitleUrlResult) {
        return simpleTitleUrlResult
      }

      const trySimpleUrl = this.trySimpleUrl(line)

      // todo: fetch title from url eagerly, or implement an option to fetch it in UI or in the background
      if (trySimpleUrl) {
        return trySimpleUrl
      }

      return null

    }).filter((item: any) => item !== null)

    return items.map((item: any) => this.formatEntry({ item, additionalTags }))
  }

  public parseText = async ({
    text, additionalTags
  }: {
    text: string, additionalTags: string[]
  }): Promise<InputEntry[]> => {

    return this.processContent({ content: text, additionalTags })
  }
}
