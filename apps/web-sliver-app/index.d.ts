type AugmentedWindow = Window & typeof globalThis & {
  openItems: ({ items, newWindow = false, incognito = false }: {
    items: any[]
    newWindow?: boolean
    incognito?: boolean
  }) => void
  failedItems: (items: any[]) => void
  extensionContext: {
    enabled: boolean,
  }
}
