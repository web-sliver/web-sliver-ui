import { SearchOptionType } from "./components/inputs/SearchInput"
import { EntrySearchInput, SearchOption } from "./types"

export const SLICE_LENGTH = 20


export const cleanValue = (value: string) => {
  if (value.startsWith('-')) {
    return value.slice(1)
  }

  return value
}


export const convertFromSearchInput = (searchInput: EntrySearchInput[]) => {
  return searchInput.map((item: EntrySearchInput) => {
    if (typeof item === 'string') {
      if (item.startsWith('-')) {
        return { name: cleanValue(item), type: SearchOptionType.EXCLUSION}
      }
      if (item.includes('*')) {
        return { name: item, type: SearchOptionType.WILDCARD}
      }

      return { name: item, type: SearchOptionType.INPUT}
    }

    return { name: item.name, type: SearchOptionType.TAG}
  })
}

export const convertToSearchInput = (searchOptions: SearchOption[]) => {
  return searchOptions.map((item: SearchOption) => {
    switch (item.type) {
      case SearchOptionType.TAG:
        return { name: item.name }

      case SearchOptionType.EXCLUSION:
        return `-${item.name}`

      default:
        return item.name
    }
  })
}

export const normalizeTagText = (text: string) => {
  const MAX_LENGTH = SLICE_LENGTH * 2
  if (text.length > MAX_LENGTH) {
    return `${text.slice(0, SLICE_LENGTH)} \u2026 ${text.slice(text.length - SLICE_LENGTH, text.length)}`
  }

  return text
}
