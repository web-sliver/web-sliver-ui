import React from 'react'

import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import { styled } from "@mui/material/styles"
import { Link } from "react-router-dom"
import { TextField } from '@mui/material'


export const Span = styled('span')({})
export const StyledLink = styled(Link)({})
export const StyledUL = styled('ul')({})

export const StyledTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: 'rgba(18, 18, 18, 0.95)',
    minWidth: 350,
    maxWidth: '90%',
    maxHeight: '90%',
    fontSize: theme.typography.pxToRem(16),
    border: '1px solid #101010',
    overflow: 'auto'
  },
}))

export const StyledTextField = styled(TextField)({})
