import React from 'react'


export const useTextInput = (initialValue: string, initialLabel: string) => {
  const [value, setValue] = React.useState(initialValue)
  const [label, setLabel] = React.useState(initialLabel)

  const updateValue = (v: string) => {
    setValue(v)

    v === initialValue ? setLabel(initialLabel) : setLabel(initialLabel + ' *')
  }

  return {
    value,
    setValue: updateValue,
    label,
    setLabel,
    reset: () => setValue(""),
    bind: {
      value,
      label,
      onChange: (event: any) => {
        updateValue(event.target.value)
      }
    }
  }
}

export const useInput = (initialValue: any) => {
  const [value, setValue] = React.useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(""),
    bind: {
      value,
      onChange: (event: any) => {
        setValue(event.target.value)
      }
    }
  }
}


export const useCheckbox = (initialValue: any) => {
  const [value, setValue] = React.useState(initialValue);

  return {
    value,
    setValue,
    reset: () => setValue(false),
    bind: {
      checked: value,
      onChange: (event: any) => {
        setValue(event.target.checked);
      }
    }
  };
};
