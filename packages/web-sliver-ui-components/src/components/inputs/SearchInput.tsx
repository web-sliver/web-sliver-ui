import React from 'react'

import { Autocomplete, Avatar, Chip, SxProps } from '@mui/material'
import { cleanValue, convertFromSearchInput, convertToSearchInput, normalizeTagText } from '../../utils'
import { StyledTextField } from '../StylingPrimitives'
import { EntrySearchInput, SearchOption, TagType } from '../../types'


export enum SearchOptionType {
  TAG = 'tag',
  INPUT = 'input',
  WILDCARD = 'wildcard',
  EXCLUSION = 'exclusion',
}


const SearchInput = ({
  getOptions,
  value: externalValue = [],
  label = 'Search',
  setValue: setExternalValue = (_: EntrySearchInput[]) => null,
  sx = {},
  inputSx = {},
  hideIcons = false,
  disabledTypes = [],
  onKeyDown = undefined,
  inputProps = {},
}: {
  getOptions: (search: string) => TagType[],
  value: EntrySearchInput[],
  label?: string,
  setValue: (value: EntrySearchInput[]) => any,
  sx?: SxProps,
  inputSx?: SxProps,
  hideIcons?: boolean,
  disabledTypes?: SearchOptionType[],
  onKeyDown?: (e: React.KeyboardEvent<HTMLDivElement>, inputValue: any, value: any) => any,
  inputProps?: any,
}) => {
  const [value, setValueState] = React.useState<SearchOption[]>(convertFromSearchInput(externalValue))
  const [inputValue, setInputValue] = React.useState<string>('')
  const [debouncedValue, setDebouncedValue] = React.useState('')
  const options: SearchOption[] = getOptions(debouncedValue.length > 2 ? debouncedValue : '')
    .map((item: TagType) => ({ name: item.name, type: SearchOptionType.TAG, uses: item.uses }))  // min valid length is 3

  const setValue = (v: SearchOption[]) => {
    setValueState(v)
    setExternalValue(convertToSearchInput(v))
  }

  // manual debounce
  React.useEffect(() => {
    const timeoutId = setTimeout(() => setDebouncedValue(cleanValue(inputValue)), 500)

    return () => clearTimeout(timeoutId)
  }, [inputValue])

  // sync external with internal
  React.useEffect(() => {
    setValueState(convertFromSearchInput(externalValue))

  }, [externalValue])

  const getTagAvatar = (option: SearchOption) => {
    switch (option.type) {
      case SearchOptionType.INPUT:
        return 'I'
      case SearchOptionType.TAG:
        return 'T'
      case SearchOptionType.WILDCARD:
        return 'W'
      case SearchOptionType.EXCLUSION:
        return '-'
    }
  }

  const processInputValue = (item: SearchOption | string) => {
    if (typeof item === 'string') {

      if (item.startsWith('-')) {
        return { name: item.slice(1), type: SearchOptionType.EXCLUSION }
      }

      if (item.includes('*')) {
        return { name: item, type: SearchOptionType.WILDCARD }
      }

      return { name: item, type: SearchOptionType.INPUT }
    }

    return item
  }

  return (
    <Autocomplete
      freeSolo  // allows arbitrary values
      selectOnFocus  // selects input on focus
      // clearOnBlur  // clears input when losing focus
      // handleHomeEndKeys  // to allow movement within the options list
      multiple  // allow tags
      autoComplete  // show suggestion inline
      includeInputInList // if true, highlight can move to the input

      sx={sx}
      options={options}

      value={value}
      onChange={(_ev: any, newValue: (string | SearchOption)[]) => {
        const processedValue = newValue
          .map(processInputValue)
          .filter(tag => disabledTypes.every(type => type !== tag.type))

        setValue(processedValue)
      }}
      onKeyDown={(e) => onKeyDown && onKeyDown(e, inputValue, value)}

      inputValue={inputValue}
      onInputChange={(_ev: any, newInputValue: string) => {
        setInputValue(newInputValue)
      }}

      getOptionLabel={(option: string | SearchOption) => typeof option === 'string' ? option : option.name}
      renderInput={(params) =>
        <StyledTextField {...params} variant='outlined' label={label} sx={inputSx} {...inputProps} />
      }
      renderOption={(props, option: any) => <li {...props}>{`${option.name} (${option.uses} entries)`}</li>}

      // tags
      renderTags={(value: readonly SearchOption[], getTagProps) =>
        value.map((option: SearchOption, index: number) => (
          <Chip
            variant='filled'
            label={normalizeTagText(option.name)}
            title={option.name}
            avatar={hideIcons ? undefined : <Avatar>{getTagAvatar(option)}</Avatar>}
            {...getTagProps({ index })}
          />
        ))
      }

      // filtering
      filterSelectedOptions
      filterOptions={(x: any) => x}  // disabled so search as you type can work
      isOptionEqualToValue={(option: SearchOption, value: string | SearchOption) => {
        if (typeof value === 'string') {
          return option.name === cleanValue(value)
        }

        return option.name === value.name
      }}
    />
  )
}

export default SearchInput
