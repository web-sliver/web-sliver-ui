export interface TagType {
  name: string,
  uses?: number,
}

export type EntrySearchInput = TagType | string

export interface SearchOption {
  name: string,
  type: string,
  uses?: number,
}


export interface IWorkspace {
  uid: string,
  name: string,
  default: boolean
}

export type IWorkspaceMaybe = IWorkspace | undefined

export interface ILocationContext {
  location: any,
  queryParams: any,
  navigateQuery: (queryParams: any, path?: string, reset?: boolean) => Promise<void>
}

export interface ListerSorting {
  sortField: string,
  sortDirection: 'asc' | 'desc',
}
