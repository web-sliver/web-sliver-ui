import { InputEntry } from '../types'
import {
  websliver_favicon_constraint, websliver_favicon_update_column
} from '../apiClient/zeus'
import { getHash } from '../utils'


export const parseBaseEntry = (workspaceId: string, item: InputEntry) => {
  return {
    title: item.title,
    description: item.description,
    workspace_id: workspaceId || item.workspace_id || '',
    location: item.location,
    tags: Array.from(new Set(item.tags)),
    ...!item.created_at ? null : {
      created_at: item.created_at
    },
    ...!item.updated_at ? null : {
      updated_at: item.updated_at
    },
  }
}

export const parseEntries = (workspaceId: string, entries: InputEntry[]) => {
  return entries.map((item: any) => ({
    ...parseBaseEntry(workspaceId, item),
    ...(item.favIconUrl ? {
      favicon: {
        data: {
          hash: getHash(item.favIconUrl),
          data: item.favIconUrl
        },
        on_conflict: {
          constraint: websliver_favicon_constraint.favicon_hash_key,
          update_columns: [
            websliver_favicon_update_column.updated_at,
          ]
        }
      }
    } : null)
  }))
}
