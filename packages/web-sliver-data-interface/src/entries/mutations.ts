import {
  useZeusVariables, websliver_entry_constraint, websliver_entry_update_column,
} from '../apiClient/zeus'
import lodash from 'lodash'

import { getEntriesByIds } from '../entries/queries'
import { Client } from '../client'
import { getEntriesQParams, getHash, parseSearchQuery } from '../utils'
import { insertFavicons } from '../favicons/mutations'
import { parseBaseEntry, parseEntries } from './parsers'
import { InputEntry, PatchEntry, SearchQuery } from '../types'


export const insertEntries = async ({ workspaceId, entries, host, token, throwErrors = false }: {
  workspaceId: string, // default workspace id if one is not found on the entry
  entries: InputEntry[],
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const parsedEntries = parseEntries(workspaceId, entries)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    insert_websliver_entry: [
      {
        objects: parsedEntries,
        on_conflict: {
          constraint: websliver_entry_constraint.entry_title_location_workspace_id_key,
          // skipping title, location, workspace_id as they're involved in the conflict
          update_columns: [
            websliver_entry_update_column.description,
            websliver_entry_update_column.favicon_id,
            websliver_entry_update_column.tags,
          ]
        }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.insert_websliver_entry?.affected_rows

  } catch (e) {
    if (throwErrors) {
      throw e
    } else {
      console.error(e)
    }
    return
  }
}

export const insertEntry = async ({
  workspaceId, entry, host, token
}: {
  workspaceId: string,
  entry: InputEntry,
  host: string,
  token: string,
}) => await insertEntries({ workspaceId, entries: [entry], host, token })


export const upsertEntries = async ({ workspaceId, entries, host, token, throwErrors = false }: {
  workspaceId: string, // default workspace id if one is not found on the entry
  entries: InputEntry[],
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const favicons = Array.from(new Set(entries
    .filter((entry: any) => !entry.favicon_id && entry.favIconUrl)
    .map((entry: any) => ({
      hash: getHash(entry.favIconUrl),
      data: entry.favIconUrl
    }))
  ))

  let faviconsMap = Object.assign({})

  const savedFavicons = await insertFavicons({ favicons, host, token, throwErrors })

  if (savedFavicons === undefined) {
    console.error('Favicon insert failed')
  } else {
    savedFavicons.forEach((favicon: any) => {
      faviconsMap[favicon.hash] = favicon
    })
  }

  const getFavIconId = (item: any) => {
    if (item.favicon_id) {
      return {
        favicon_id: item.favicon_id
      }
    }

    const favicon = item.favIconUrl && faviconsMap[getHash(item.favIconUrl)] || null

    return favicon ? {
      favicon_id: favicon.uid
    } : null
  }

  let parsedEntries = entries.map((item: any) => ({
    ...parseBaseEntry(workspaceId, item),
    ...getFavIconId(item)
  }))

  // merge entries that conflict within the list
  const entriesMap = Object.assign({})

  parsedEntries.forEach((entry: any) => {
    const key = `${entry.title}, ${entry.location}`

    // manage description for conflicts within the list
    const existing = entriesMap[key]
    let description = entry.description

    if (existing) {
      if (existing.description && entry.description) {
        const entryDate = entry.updated_at || entry.created_at
        const existingDate = existing.updated_at || existing.created_at

        if (entryDate && existingDate && existingDate > entryDate) {
          description = existing.description
        } else {
          description = (entryDate && entry.description) || (existingDate && existing.description)
        }
      } else {
        description = entry.description || existing.description
      }
    }

    entriesMap[key] = {
      ...entry,
      description,
      tags: Array.from(new Set((entry.tags || []).concat(entriesMap[key]?.tags || [])))
    }
  })

  parsedEntries = Object.values(entriesMap)

  const variables = useZeusVariables({ entries: 'jsonb' })({ entries: parsedEntries })
  const { $ } = variables

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    websliver_upsert_entries_aff_rows: [
      {
        args: {
          entries: $(`entries`),
          workspace_id: workspaceId,
        },
      },
      {
        affected_rows: true
      }
    ]
  }, {
    variables
  })

  try {
    const resp = await executeQuery()
    return resp.websliver_upsert_entries_aff_rows.length == 1 ? resp.websliver_upsert_entries_aff_rows[0].affected_rows as number : undefined

  } catch (e) {
    if (throwErrors) {
      throw e
    } else {
      console.error(e)
    }
    return
  }
}

export const upsertEntry = async ({
  workspaceId, entry, host, token,
}: {
  workspaceId: string,
  entry: InputEntry,
  host: string,
  token: string,
}) => await upsertEntries({ workspaceId, entries: [entry], host, token })

export const bulkUpsertEntries = async ({
  workspaceId,
  entries,
  host,
  token,
  throwErrors = false,
  chunkSize = 1000,
  stepCallback,
}: {
  workspaceId: string, // default workspace id if one is not found on the entry
  entries: InputEntry[],
  host: string,
  token: string,
  throwErrors?: boolean,
  chunkSize?: number,
  stepCallback?: ({ step, total, }: { step: number, total: number, }) => any,
}) => {
  const chunks = lodash.chunk(entries, chunkSize)

  let counter = 0
  const total = chunks.length

  stepCallback && stepCallback({ step: counter, total, })

  for (const chunk of chunks) {
    await upsertEntries({ entries: chunk, workspaceId, host, token, throwErrors })

    counter += 1
    stepCallback && stepCallback({ step: counter, total, })
  }
}

export const deleteEntries = async ({ entryIds, host, token }: { entryIds: string[], host: string, token: string, }) => {
  // no need for workspace since authorization is handled by backend

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    delete_websliver_entry: [
      {
        where: {
          uid: { _in: entryIds }
        }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.delete_websliver_entry?.affected_rows

  } catch (e) {
    console.error(e)
    return
  }
}

export const deleteEntriesByQuery = async ({
  query, host, token
}: {
  query: SearchQuery, host: string, token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const params = getEntriesQParams(query)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    delete_websliver_entry: [
      params,
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.delete_websliver_entry?.affected_rows

  } catch (e) {
    console.error(e)
    return
  }
}

export const updateEntry = async ({ entry, normalize = true, host, token, throwErrors = false }: {
  entry: PatchEntry,
  normalize?: boolean,
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  let { uid: entryId, workspace, ...entryData } = entry

  if (!entryId) {
    console.error('Cannot update entry without a specified uid.')
    return
  }

  if (workspace && !entryData.workspace_id) {
    entryData.workspace_id = workspace.uid
  }

  if (normalize) {
    // filter and normalize fields
    entryData = {
      description: entryData.description || '',
      location: entryData.location || '',
      tags: Array.from(new Set(entryData.tags || [])),
      title: entryData.title || '',
      workspace_id: entryData.workspace_id
    }
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    update_websliver_entry_by_pk: [
      {
        _set: entryData,
        pk_columns: { uid: entryId }
      },
      {
        uid: true,
        title: true,
        description: true,
        location: true,
        workspace_id: true,
        tags: [{}, true],
        created_at: true,
        updated_at: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.update_websliver_entry_by_pk

  } catch (e) {
    if (throwErrors) {
      throw e
    } else {
      console.error(e)
    }
    return
  }
}

export const updateEntries = async ({
  entries, normalize = true, host, token, throwErrors = false
}: {
  entries: PatchEntry[], normalize?: boolean, host: string, token: string, throwErrors?: boolean
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  let result = []

  for (const entry of entries) {
    const updated = await updateEntry({ entry, normalize, host, token, throwErrors })

    if (!updated) {
      continue
    }

    result.push(updated)
  }

  return result
}

export const copyEntriesToWorkspace = async ({ entryIds, sourceWorkspaceId, destinationWorkspaceId, host, token, }: {
  entryIds: string[],
  sourceWorkspaceId: string,
  destinationWorkspaceId: string,
  host: string,
  token: string,
}) => {
  // this method will copy entries from source to destination and merge existing entries

  if (!token) {
    console.error('Token not provided.')
    return
  }

  let entries = await getEntriesByIds({ entryIds, workspaceId: sourceWorkspaceId, host, token })

  // strip source workspace id and replace with destination destination workspace
  entries = entries.map(({ workspace_id, ...rest }: any) => ({ ...rest, workspace_id: destinationWorkspaceId }))

  return await upsertEntries({ workspaceId: destinationWorkspaceId, entries, host, token })
}


export const copyEntriesToWorkspaceByQuery = async ({ query, destinationWorkspaceId, host, token, throwErrors, }: {
  query: SearchQuery,
  destinationWorkspaceId: string,
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {
  // this method will copy entries from source to destination and merge existing entries

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const { workspaceId: sourceWorkspaceId, compiledSearchTerm, searchExclusions, searchTags } = parseSearchQuery(query)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    websliver_copy_entries_aff_rows: [
      {
        args: {
          source_workspace_id: sourceWorkspaceId,
          destination_workspace_id: destinationWorkspaceId,
          search_tags: searchTags,
          search_exclusions: searchExclusions,
          search_term: compiledSearchTerm
        },
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.websliver_copy_entries_aff_rows.length == 1 ? resp.websliver_copy_entries_aff_rows[0].affected_rows as number : undefined

  } catch (e) {
    if (throwErrors) {
      throw e
    } else {
      console.error(e)
    }
    return
  }
}


export const moveEntriesToWorkspace = async ({ entryIds, sourceWorkspaceId, destinationWorkspaceId, host, token, }: {
  entryIds: string[],
  sourceWorkspaceId: string,
  destinationWorkspaceId: string,
  host: string,
  token: string,
}) => {

  // this method will move entries from source to destination and merge existing entries

  if (!token) {
    console.error('Token not provided.')
    return
  }
  // does an upsert and then a delete

  let entries = await getEntriesByIds({ entryIds, workspaceId: sourceWorkspaceId, host, token })

  // strip source workspace id
  entries = entries.map(({ workspace_id, ...rest }: any) => rest)

  const upsertResponse = await upsertEntries({ workspaceId: destinationWorkspaceId, entries, host, token })

  if (upsertResponse !== undefined) {
    // delete old entries
    return await deleteEntries({ entryIds, host, token })
  }

  return
}


export const moveEntriesToWorkspaceByQuery = async ({ query, destinationWorkspaceId, host, token, throwErrors, }: {
  query: SearchQuery,
  destinationWorkspaceId: string,
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {
  // this method will move entries from source to destination and merge existing entries
  // performs a copy and a delete after

  if (!token) {
    console.error('Token not provided.')
    return
  }

  // the copy
  const result = await copyEntriesToWorkspaceByQuery({ query, destinationWorkspaceId, host, token, throwErrors })

  // the delete
  if ( result && result > 0) {
    // note: uses the workspace from the query
    return await deleteEntriesByQuery({ query, host, token })
  }
}
