import { Entry, EntrySearchInput, SearchQuery } from '../types'
import { Client } from '../client'

import { DEFAULT_ENTRY_LIMIT, getEntriesQParams, } from '../utils'


export const getEntries = async ({
  query = {
    workspaceId: '',
    searchTerm: [],
    offset: 0,
    limit: DEFAULT_ENTRY_LIMIT,
    sortField: 'title',
    sortDirection: 'asc',
  },
  fields = null,
  host,
  token,
}: {
  query: SearchQuery,
  fields?: null | object,
  host: string,
  token: string,
}) => {

  query.sortField = query.sortField || 'title'
  query.sortDirection = query.sortDirection || 'asc'

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  if (query.workspaceId.length == 0) {
    return []
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      {
        where: { uid: { _eq: query.workspaceId } }
      },
      {
        entries: [
          getEntriesQParams(query),
          fields || {
            uid: true,
            title: true,
            description: true,
            location: true,
            tags: [{}, true],
            workspace: { uid: true, name: true, default: true },
            created_at: true,
            updated_at: true,
            favicon_id: true,
          }
        ]
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return (resp.websliver_workspace[0] || {}).entries || []

  } catch (e) {
    console.error(e)
    return []
  }
}

export const getEntriesByLocations = async ({
  query = {
    workspaceId: '',
    locations: [],
    offset: 0,
    limit: DEFAULT_ENTRY_LIMIT,
    sortField: 'title',
    sortDirection: 'asc',
  },
  host,
  token,
}: {
  query: SearchQuery,
  host: string,
  token: string,
}) => {

  query.sortField = query.sortField || 'title'
  query.sortDirection = query.sortDirection || 'asc'

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  if (query.workspaceId.length == 0) {
    return []
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      {
        where: { uid: { _eq: query.workspaceId } }
      },
      {
        entries: [
          getEntriesQParams(query),
          {
            uid: true,
            title: true,
            description: true,
            location: true,
            tags: [{}, true],
            workspace: { uid: true, name: true, default: true },
            created_at: true,
            updated_at: true,
            favicon_id: true,
          }
        ]
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return (resp.websliver_workspace[0] || {}).entries || []

  } catch (e) {
    console.error(e)
    return []
  }
}

export const getEntriesByIds = async ({ entryIds, workspaceId, host, token }: {
  entryIds: string[],
  workspaceId: string,
  host: string,
  token: string,
}): Promise<Entry[]> => {

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      {
        where: { uid: { _eq: workspaceId } }
      },
      {
        entries: [
          {
            where: { uid: { _in: entryIds } }
          },
          {
            uid: true,
            title: true,
            description: true,
            location: true,
            tags: [{}, true],
            workspace_id: true,
            workspace: { uid: true, name: true, default: true },
            created_at: true,
            updated_at: true,
            favicon_id: true,
          }
        ]
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return (resp.websliver_workspace[0] || {}).entries || []

  } catch (e) {
    console.error(e)
    return []
  }
}

export const getEntriesByPK = async ({ entries, workspaceId, host, token }: {
  entries: any[],
  workspaceId: string,
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  const entriesPKs = entries.map(({ title, location, workspace }: any) => ({
    title: { _eq: title },
    location: { _eq: location },
    workspaceId: { _eq: workspace?.uid || workspaceId }
  }))

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_entry: [
      {
        where: { _and: entriesPKs }
      },
      {
        uid: true,
        title: true,
        description: true,
        location: true,
        tags: [{}, true],
        workspace: { uid: true, name: true, default: true },
        created_at: true,
        updated_at: true,
        favicon_id: true,
      }
    ],
  })

  try {
    const resp = await executeQuery()

    return resp.websliver_entry || []

  } catch (e) {
    console.error(e)
    return []
  }
}

export const getEntriesCount = async ({ workspaceId, searchTerm, host, token }: {
  workspaceId: string,
  searchTerm?: EntrySearchInput[],
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return 0
  }

  if (workspaceId.length == 0) {
    return 0
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      {
        where: { uid: { _eq: workspaceId } }
      },
      {
        entries_aggregate: [
          getEntriesQParams({ workspaceId, searchTerm }),
          {
            aggregate: {
              count: [{}, true]
            }
          }
        ]
      }
    ]
  })

  try {
    const resp = await executeQuery()
    const extracted = (resp.websliver_workspace[0] || {}).entries_aggregate?.aggregate?.count

    return extracted === undefined ? 0 : extracted

  } catch (e) {
    console.error(e)
    return -1
  }
}
