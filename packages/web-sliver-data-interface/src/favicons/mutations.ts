import {
  websliver_favicon_constraint, websliver_favicon_update_column
} from '../apiClient/zeus'

import { Client } from '../client'


export const insertFavicons = async ({ favicons, host, token, throwErrors = false }: {
  favicons: any[],
  host: string,
  token: string,
  throwErrors?: boolean,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  // logic to filter out duplicate hashes and/or duplicate favicons
  const faviconsMap = Object.assign({})

  favicons.forEach((item: any) => {
    faviconsMap[item.hash] = item
  })

  favicons = Object.values(faviconsMap)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    insert_websliver_favicon: [
      {
        objects: favicons, // array of { hash, data }
        on_conflict: {
          constraint: websliver_favicon_constraint.favicon_hash_key,
          update_columns: [
            websliver_favicon_update_column.updated_at,
          ]
        }
      },
      {
        returning: {
          hash: true,
          uid: true,
        }
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.insert_websliver_favicon?.returning

  } catch (e) {
    if (throwErrors) {
      throw e
    } else {
      console.error(e)
    }
    return
  }
}
