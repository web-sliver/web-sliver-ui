import * as crypto from 'crypto'
import { order_by } from "./apiClient/zeus"
import { TagType, EntrySearchInput, SearchQuery } from './types'


export const DEFAULT_ENTRY_LIMIT = 25

export const parseSortDirection = (direction: string) => direction === 'asc' ? order_by.asc_nulls_first : order_by.desc_nulls_last

export const parseSearchQuery = ({
  workspaceId,
  searchTerm: searchInput = [],
  locations = [],
  offset,
  limit,
  sortField,
  sortDirection
}: SearchQuery) => {
  let searchExclusions: string[] = []
  let searchTerms: string[] = []
  let searchTags: string[] = []

  searchInput.forEach((item: EntrySearchInput) => {
    if (typeof item === 'string') {
      if (item.startsWith('-')) {
        item = item.slice(1).trim()

        if (item.length !== 0) {
          searchExclusions.push(item)
        }

      } else {
        item = item.trim()
        if (item.length !== 0) {
          searchTerms.push(item)
        }
      }

    } else {
      searchTags.push((item as TagType).name)
    }
  })

  return {
    workspaceId,
    compiledSearchTerm: searchTerms.length === 0 ? '' : `.*${searchTerms.join('.*')}.*`,
    compiledExclusionsTerm: searchExclusions.length === 0 ? '' : `%(${searchExclusions.join('|')})%`,
    searchTerms,
    searchTags,
    searchExclusions,
    locations,
    offset,
    limit,
    sortField,
    sortDirection
  }
}

export const getEntriesQParams = (query: SearchQuery) => {

  const {
    workspaceId,
    compiledSearchTerm,
    compiledExclusionsTerm,
    searchTags,
    locations = [],
    offset,
    limit,
    sortField,
    sortDirection
  } = parseSearchQuery(query)

  let params = { where: { workspace_id: { _eq: workspaceId } } }

  // append sorting if any
  if (sortField !== undefined && sortDirection !== undefined) {
    const sortDirectionParsed = parseSortDirection(sortDirection)

    params = Object.assign({}, params, {
      order_by: [
        // sort by the given field and fallback to location otherwise
        { [sortField]: sortDirectionParsed },
        { location: sortDirectionParsed },
      ]
    })
  }

  // append paging if any
  if (offset !== undefined && limit !== undefined) {
    params = Object.assign({}, params, { limit, offset })
  }

  // search params
  if (query.searchTerm && query.searchTerm.length !== 0) {

    // note: overwrites the previous where clause
    params = Object.assign({}, params, {
      where: {
        _and: [
          { workspace_id: { _eq: workspaceId } },
          {
            _or: [
              { description: { _iregex: compiledSearchTerm } },
              { title: { _iregex: compiledSearchTerm } },
              { location: { _iregex: compiledSearchTerm } },
            ]
          },
          {
            tags: {
              _has_keys_all: searchTags,
              _cast: {
                String: {
                  _nsimilar: compiledExclusionsTerm
                }
              }
            }
          }
        ],
      }
    })
  } else {
    // at this point we search for locations only if search input was not specified as there is no current need to search for both
    if (locations && locations.length !== 0) {
      // note: overwrites the previous where clause
      params = Object.assign({}, params, {
        where: {
          _and: [
            { workspace_id: { _eq: workspaceId } },
            { location: { _in: locations } }
          ]
        }
      })
    }
  }

  return params
}

export const getHash = (text: string) => {
  let hash = crypto.createHash('sha256')
  hash.update(text)
  return hash.digest('hex')
}
