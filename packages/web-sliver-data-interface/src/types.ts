export interface TagType {
  name: string,
  uses?: number,
}

export type EntrySearchInput = TagType | string

export interface SearchQuery {
  workspaceId: string,
  searchTerm?: EntrySearchInput[],
  locations?: string[],
  offset?: number,
  limit?: number,
  sortField?: string,
  sortDirection?: string
}

export interface Entry {
  uid?: string,
  title: string,
  description: string,
  workspace_id: string,
  workspace?: any,
  location: string,
  favicon_id?: string | null,
  tags: string[],
  created_at?: Date | null,
  updated_at?: Date | null,
}

export type InputEntry = Entry & {
  favIconUrl?: string,
  _raw?: any | null,
}

export interface PatchEntry {
  uid?: string,
  title?: string,
  description?: string,
  workspace_id?: string,
  workspace?: any,
  location?: string,
  favicon_id?: string,
  tags?: string[],
  created_at?: Date,
  updated_at?: Date,
}
