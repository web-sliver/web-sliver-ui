/* eslint-disable */

import { AllTypesProps, ReturnTypes, Ops } from './const';
export const HOST = "http://127.0.0.1:8080/v1/graphql"



const handleFetchResponse = (response: Response): Promise<GraphQLResponse> => {
  if (!response.ok) {
    return new Promise((_, reject) => {
      response
        .text()
        .then((text) => {
          try {
            reject(JSON.parse(text));
          } catch (err) {
            reject(text);
          }
        })
        .catch(reject);
    });
  }
  return response.json();
};

export const apiFetch = (options: fetchOptions) => (query: string, variables: Record<string, unknown> = {}) => {
  const fetchOptions = options[1] || {};
  if (fetchOptions.method && fetchOptions.method === 'GET') {
    return fetch(`${options[0]}?query=${encodeURIComponent(query)}`, fetchOptions)
      .then(handleFetchResponse)
      .then((response: GraphQLResponse) => {
        if (response.errors) {
          throw new GraphQLError(response);
        }
        return response.data;
      });
  }
  return fetch(`${options[0]}`, {
    body: JSON.stringify({ query, variables }),
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    ...fetchOptions,
  })
    .then(handleFetchResponse)
    .then((response: GraphQLResponse) => {
      if (response.errors) {
        throw new GraphQLError(response);
      }
      return response.data;
    });
};




export const apiSubscription = (options: chainOptions) => (query: string) => {
  try {
    const queryString = options[0] + '?query=' + encodeURIComponent(query);
    const wsString = queryString.replace('http', 'ws');
    const host = (options.length > 1 && options[1]?.websocket?.[0]) || wsString;
    const webSocketOptions = options[1]?.websocket || [host];
    const ws = new WebSocket(...webSocketOptions);
    return {
      ws,
      on: (e: (args: any) => void) => {
        ws.onmessage = (event: any) => {
          if (event.data) {
            const parsed = JSON.parse(event.data);
            const data = parsed.data;
            return e(data);
          }
        };
      },
      off: (e: (args: any) => void) => {
        ws.onclose = e;
      },
      error: (e: (args: any) => void) => {
        ws.onerror = e;
      },
      open: (e: () => void) => {
        ws.onopen = e;
      },
    };
  } catch {
    throw new Error('No websockets implemented');
  }
};






export const InternalsBuildQuery = (
  props: AllTypesPropsType,
  returns: ReturnTypesType,
  ops: Operations,
  options?: OperationOptions,
) => {
  const ibb = (k: string, o: InputValueType | VType, p = '', root = true): string => {
    const keyForPath = purifyGraphQLKey(k);
    const newPath = [p, keyForPath].join(SEPARATOR);
    if (!o) {
      return '';
    }
    if (typeof o === 'boolean' || typeof o === 'number') {
      return k;
    }
    if (typeof o === 'string') {
      return `${k} ${o}`;
    }
    if (Array.isArray(o)) {
      const args = InternalArgsBuilt(props, returns, ops, options?.variables?.values)(o[0], newPath);
      return `${ibb(args ? `${k}(${args})` : k, o[1], p, false)}`;
    }
    if (k === '__alias') {
      return Object.entries(o)
        .map(([alias, objectUnderAlias]) => {
          if (typeof objectUnderAlias !== 'object' || Array.isArray(objectUnderAlias)) {
            throw new Error(
              'Invalid alias it should be __alias:{ YOUR_ALIAS_NAME: { OPERATION_NAME: { ...selectors }}}',
            );
          }
          const operationName = Object.keys(objectUnderAlias)[0];
          const operation = objectUnderAlias[operationName];
          return ibb(`${alias}:${operationName}`, operation, p, false);
        })
        .join('\n');
    }
    const hasOperationName = root && options?.operationName ? ' ' + options.operationName : '';
    const hasVariables = root && options?.variables?.$params ? `(${options.variables?.$params})` : '';
    const keyForDirectives = o.__directives ? `${k} ${o.__directives}` : k;
    return `${keyForDirectives}${hasOperationName}${hasVariables}{${Object.entries(o)
      .map((e) => ibb(...e, [p, `field<>${keyForPath}`].join(SEPARATOR), false))
      .join('\n')}}`;
  };
  return ibb;
};










export const Thunder = (fn: FetchFunction) => <
  O extends keyof typeof Ops,
  R extends keyof ValueTypes = GenericOperation<O>
>(
  operation: O,
) => <Z extends ValueTypes[R]>(o: Z | ValueTypes[R], ops?: OperationOptions) =>
  fullChainConstruct(fn)(operation)(o as any, ops) as Promise<InputType<GraphQLTypes[R], Z>>;

export const Chain = (...options: chainOptions) => Thunder(apiFetch(options));

export const SubscriptionThunder = (fn: SubscriptionFunction) => <
  O extends keyof typeof Ops,
  R extends keyof ValueTypes = GenericOperation<O>
>(
  operation: O,
) => <Z extends ValueTypes[R]>(o: Z | ValueTypes[R], ops?: OperationOptions) =>
  fullSubscriptionConstruct(fn)(operation)(o as any, ops) as SubscriptionToGraphQL<Z, GraphQLTypes[R]>;

export const Subscription = (...options: chainOptions) => SubscriptionThunder(apiSubscription(options));
export const Zeus = <
  Z extends ValueTypes[R],
  O extends keyof typeof Ops,
  R extends keyof ValueTypes = GenericOperation<O>
>(
  operation: O,
  o: Z | ValueTypes[R],
  ops?: OperationOptions,
) => InternalsBuildQuery(AllTypesProps, ReturnTypes, Ops, ops)(operation, o as any);
export const Selector = <T extends keyof ValueTypes>(key: T) => ZeusSelect<ValueTypes[T]>();

export const Gql = Chain(HOST);






export const fullChainConstruct = (fn: FetchFunction) => (t: 'query' | 'mutation' | 'subscription') => (
  o: Record<any, any>,
  options?: OperationOptions,
) => {
  const builder = InternalsBuildQuery(AllTypesProps, ReturnTypes, Ops, options);
  return fn(builder(t, o), options?.variables?.values);
};






export const fullSubscriptionConstruct = (fn: SubscriptionFunction) => (t: 'query' | 'mutation' | 'subscription') => (
  o: Record<any, any>,
  options?: OperationOptions,
) => {
  const builder = InternalsBuildQuery(AllTypesProps, ReturnTypes, Ops, options);
  return fn(builder(t, o));
};





export type AllTypesPropsType = {
  [x: string]:
    | undefined
    | boolean
    | {
        [x: string]:
          | undefined
          | string
          | {
              [x: string]: string | undefined;
            };
      };
};

export type ReturnTypesType = {
  [x: string]:
    | {
        [x: string]: string | undefined;
      }
    | undefined;
};
export type InputValueType = {
  [x: string]: undefined | boolean | string | number | [any, undefined | boolean | InputValueType] | InputValueType;
};
export type VType =
  | undefined
  | boolean
  | string
  | number
  | [any, undefined | boolean | InputValueType]
  | InputValueType;

export type PlainType = boolean | number | string | null | undefined;
export type ZeusArgsType =
  | PlainType
  | {
      [x: string]: ZeusArgsType;
    }
  | Array<ZeusArgsType>;

export type Operations = Record<string, string | undefined>;

export type VariableDefinition = {
  [x: string]: unknown;
};

export const SEPARATOR = '|';

export type fetchOptions = Parameters<typeof fetch>;
type websocketOptions = typeof WebSocket extends new (...args: infer R) => WebSocket ? R : never;
export type chainOptions = [fetchOptions[0], fetchOptions[1] & { websocket?: websocketOptions }] | [fetchOptions[0]];
export type FetchFunction = (query: string, variables?: Record<string, any>) => Promise<any>;
export type SubscriptionFunction = (query: string) => any;
type NotUndefined<T> = T extends undefined ? never : T;
export type ResolverType<F> = NotUndefined<F extends [infer ARGS, any] ? ARGS : undefined>;

export type OperationOptions = {
  variables?: VariableInput;
  operationName?: string;
};
export interface GraphQLResponse {
  data?: Record<string, any>;
  errors?: Array<{
    message: string;
  }>;
}
export class GraphQLError extends Error {
  constructor(public response: GraphQLResponse) {
    super('');
    console.error(response);
  }
  toString() {
    return 'GraphQL Response Error';
  }
}
export type GenericOperation<O> = O extends keyof typeof Ops ? typeof Ops[O] : never;


export const purifyGraphQLKey = (k: string) => k.replace(/\([^)]*\)/g, '').replace(/^[^:]*\:/g, '');




const mapPart = (p: string) => {
  const [isArg, isField] = p.split('<>');
  if (isField) {
    return {
      v: isField,
      __type: 'field',
    } as const;
  }
  return {
    v: isArg,
    __type: 'arg',
  } as const;
};

type Part = ReturnType<typeof mapPart>;

export const ResolveFromPath = (props: AllTypesPropsType, returns: ReturnTypesType, ops: Operations) => {
  const ResolvePropsType = (mappedParts: Part[]) => {
    const oKey = ops[mappedParts[0].v];
    const propsP1 = oKey ? props[oKey] : props[mappedParts[0].v];
    if (typeof propsP1 === 'boolean' && mappedParts.length === 1) {
      return 'enum';
    }
    if (typeof propsP1 === 'object') {
      const propsP2 = propsP1[mappedParts[1].v];
      if (typeof propsP2 === 'string') {
        return rpp(
          `${propsP2}${SEPARATOR}${mappedParts
            .slice(2)
            .map((mp) => mp.v)
            .join(SEPARATOR)}`,
        );
      }
      if (typeof propsP2 === 'object') {
        const propsP3 = propsP2[mappedParts[2].v];
        if (propsP3 && mappedParts[2].__type === 'arg') {
          return rpp(
            `${propsP3}${SEPARATOR}${mappedParts
              .slice(3)
              .map((mp) => mp.v)
              .join(SEPARATOR)}`,
          );
        }
      }
    }
  };
  const ResolveReturnType = (mappedParts: Part[]) => {
    const oKey = ops[mappedParts[0].v];
    const returnP1 = oKey ? returns[oKey] : returns[mappedParts[0].v];
    if (typeof returnP1 === 'object') {
      const returnP2 = returnP1[mappedParts[1].v];
      if (returnP2) {
        return rpp(
          `${returnP2}${SEPARATOR}${mappedParts
            .slice(2)
            .map((mp) => mp.v)
            .join(SEPARATOR)}`,
        );
      }
    }
  };
  const rpp = (path: string): 'enum' | 'not' => {
    const parts = path.split(SEPARATOR).filter((l) => l.length > 0);
    const mappedParts = parts.map(mapPart);
    const propsP1 = ResolvePropsType(mappedParts);
    if (propsP1) {
      return propsP1;
    }
    const returnP1 = ResolveReturnType(mappedParts);
    if (returnP1) {
      return returnP1;
    }
    return 'not';
  };
  return rpp;
};

export const InternalArgsBuilt = (
  props: AllTypesPropsType,
  returns: ReturnTypesType,
  ops: Operations,
  variables?: Record<string, unknown>,
) => {
  const arb = (a: ZeusArgsType, p = '', root = true): string => {
    if (Array.isArray(a)) {
      return `[${a.map((arr) => arb(arr, p, false)).join(', ')}]`;
    }
    if (typeof a === 'string') {
      if (a.startsWith('$') && variables?.[a.slice(1)]) {
        return a;
      }
      const checkType = ResolveFromPath(props, returns, ops)(p);
      if (checkType === 'enum') {
        return a;
      }
      return `${JSON.stringify(a)}`;
    }
    if (typeof a === 'object') {
      if (a === null) {
        return `null`;
      }
      const returnedObjectString = Object.entries(a)
        .filter(([, v]) => typeof v !== 'undefined')
        .map(([k, v]) => `${k}: ${arb(v, [p, k].join(SEPARATOR), false)}`)
        .join(',\n');
      if (!root) {
        return `{${returnedObjectString}}`;
      }
      return returnedObjectString;
    }
    return `${a}`;
  };
  return arb;
};




export const resolverFor = <X, T extends keyof ValueTypes, Z extends keyof ValueTypes[T]>(
  type: T,
  field: Z,
  fn: (
    args: Required<ValueTypes[T]>[Z] extends [infer Input, any] ? Input : any,
    source: any,
  ) => Z extends keyof ModelTypes[T] ? ModelTypes[T][Z] | Promise<ModelTypes[T][Z]> | X : any,
) => fn as (args?: any, source?: any) => any;


export type SelectionFunction<V> = <T>(t: T | V) => T;
export const ZeusSelect = <T>() => ((t: unknown) => t) as SelectionFunction<T>;




export type UnwrapPromise<T> = T extends Promise<infer R> ? R : T;
export type ZeusState<T extends (...args: any[]) => Promise<any>> = NonNullable<UnwrapPromise<ReturnType<T>>>;
export type ZeusHook<
  T extends (...args: any[]) => Record<string, (...args: any[]) => Promise<any>>,
  N extends keyof ReturnType<T>
> = ZeusState<ReturnType<T>[N]>;

export type WithTypeNameValue<T> = T & {
  __typename?: boolean;
  __directives?: string;
};
export type AliasType<T> = WithTypeNameValue<T> & {
  __alias?: Record<string, WithTypeNameValue<T>>;
};
type DeepAnify<T> = {
  [P in keyof T]?: any;
};
type IsPayLoad<T> = T extends [any, infer PayLoad] ? PayLoad : T;
type IsArray<T, U> = T extends Array<infer R> ? InputType<R, U>[] : InputType<T, U>;
type FlattenArray<T> = T extends Array<infer R> ? R : T;
type BaseZeusResolver = boolean | 1 | string;

type IsInterfaced<SRC extends DeepAnify<DST>, DST> = FlattenArray<SRC> extends ZEUS_INTERFACES | ZEUS_UNIONS
  ? {
      [P in keyof SRC]: SRC[P] extends '__union' & infer R
        ? P extends keyof DST
          ? IsArray<R, '__typename' extends keyof DST ? DST[P] & { __typename: true } : DST[P]>
          : Record<string, unknown>
        : never;
    }[keyof DST] &
      {
        [P in keyof Omit<
          Pick<
            SRC,
            {
              [P in keyof DST]: SRC[P] extends '__union' & infer R ? never : P;
            }[keyof DST]
          >,
          '__typename'
        >]: IsPayLoad<DST[P]> extends BaseZeusResolver ? SRC[P] : IsArray<SRC[P], DST[P]>;
      }
  : {
      [P in keyof Pick<SRC, keyof DST>]: IsPayLoad<DST[P]> extends BaseZeusResolver ? SRC[P] : IsArray<SRC[P], DST[P]>;
    };

export type MapType<SRC, DST> = SRC extends DeepAnify<DST> ? IsInterfaced<SRC, DST> : never;
export type InputType<SRC, DST> = IsPayLoad<DST> extends { __alias: infer R }
  ? {
      [P in keyof R]: MapType<SRC, R[P]>[keyof MapType<SRC, R[P]>];
    } &
      MapType<SRC, Omit<IsPayLoad<DST>, '__alias'>>
  : MapType<SRC, IsPayLoad<DST>>;
export type SubscriptionToGraphQL<Z, T> = {
  ws: WebSocket;
  on: (fn: (args: InputType<T, Z>) => void) => void;
  off: (fn: (e: { data?: InputType<T, Z>; code?: number; reason?: string; message?: string }) => void) => void;
  error: (fn: (e: { data?: InputType<T, Z>; errors?: string[] }) => void) => void;
  open: () => void;
};


export const useZeusVariables = <T>(variables: T) => <
  Z extends {
    [P in keyof T]: unknown;
  }
>(
  values: Z,
) => {
  return {
    $params: Object.keys(variables)
      .map((k) => `$${k}: ${variables[k as keyof T]}`)
      .join(', '),
    $: <U extends keyof Z>(variable: U) => {
      return (`$${variable}` as unknown) as Z[U];
    },
    values,
  };
};

export type VariableInput = {
  $params: ReturnType<ReturnType<typeof useZeusVariables>>['$params'];
  values: Record<string, unknown>;
};


type ZEUS_INTERFACES = never
type ZEUS_UNIONS = never

export type ValueTypes = {
    ["Boolean_cast_exp"]: {
	String?: ValueTypes["String_comparison_exp"] | undefined | null
};
	/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
["Boolean_comparison_exp"]: {
	_cast?: ValueTypes["Boolean_cast_exp"] | undefined | null,
	_eq?: boolean | undefined | null,
	_gt?: boolean | undefined | null,
	_gte?: boolean | undefined | null,
	_in?: Array<boolean> | undefined | null,
	_is_null?: boolean | undefined | null,
	_lt?: boolean | undefined | null,
	_lte?: boolean | undefined | null,
	_neq?: boolean | undefined | null,
	_nin?: Array<boolean> | undefined | null
};
	/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
["String_comparison_exp"]: {
	_eq?: string | undefined | null,
	_gt?: string | undefined | null,
	_gte?: string | undefined | null,
	/** does the column match the given case-insensitive pattern */
	_ilike?: string | undefined | null,
	_in?: Array<string> | undefined | null,
	/** does the column match the given POSIX regular expression, case insensitive */
	_iregex?: string | undefined | null,
	_is_null?: boolean | undefined | null,
	/** does the column match the given pattern */
	_like?: string | undefined | null,
	_lt?: string | undefined | null,
	_lte?: string | undefined | null,
	_neq?: string | undefined | null,
	/** does the column NOT match the given case-insensitive pattern */
	_nilike?: string | undefined | null,
	_nin?: Array<string> | undefined | null,
	/** does the column NOT match the given POSIX regular expression, case insensitive */
	_niregex?: string | undefined | null,
	/** does the column NOT match the given pattern */
	_nlike?: string | undefined | null,
	/** does the column NOT match the given POSIX regular expression, case sensitive */
	_nregex?: string | undefined | null,
	/** does the column NOT match the given SQL regular expression */
	_nsimilar?: string | undefined | null,
	/** does the column match the given POSIX regular expression, case sensitive */
	_regex?: string | undefined | null,
	/** does the column match the given SQL regular expression */
	_similar?: string | undefined | null
};
	["bigint"]:unknown;
	["bigint_cast_exp"]: {
	String?: ValueTypes["String_comparison_exp"] | undefined | null
};
	/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
["bigint_comparison_exp"]: {
	_cast?: ValueTypes["bigint_cast_exp"] | undefined | null,
	_eq?: ValueTypes["bigint"] | undefined | null,
	_gt?: ValueTypes["bigint"] | undefined | null,
	_gte?: ValueTypes["bigint"] | undefined | null,
	_in?: Array<ValueTypes["bigint"]> | undefined | null,
	_is_null?: boolean | undefined | null,
	_lt?: ValueTypes["bigint"] | undefined | null,
	_lte?: ValueTypes["bigint"] | undefined | null,
	_neq?: ValueTypes["bigint"] | undefined | null,
	_nin?: Array<ValueTypes["bigint"]> | undefined | null
};
	["jsonb"]:unknown;
	["jsonb_cast_exp"]: {
	String?: ValueTypes["String_comparison_exp"] | undefined | null
};
	/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
["jsonb_comparison_exp"]: {
	_cast?: ValueTypes["jsonb_cast_exp"] | undefined | null,
	/** is the column contained in the given json value */
	_contained_in?: ValueTypes["jsonb"] | undefined | null,
	/** does the column contain the given json value at the top level */
	_contains?: ValueTypes["jsonb"] | undefined | null,
	_eq?: ValueTypes["jsonb"] | undefined | null,
	_gt?: ValueTypes["jsonb"] | undefined | null,
	_gte?: ValueTypes["jsonb"] | undefined | null,
	/** does the string exist as a top-level key in the column */
	_has_key?: string | undefined | null,
	/** do all of these strings exist as top-level keys in the column */
	_has_keys_all?: Array<string> | undefined | null,
	/** do any of these strings exist as top-level keys in the column */
	_has_keys_any?: Array<string> | undefined | null,
	_in?: Array<ValueTypes["jsonb"]> | undefined | null,
	_is_null?: boolean | undefined | null,
	_lt?: ValueTypes["jsonb"] | undefined | null,
	_lte?: ValueTypes["jsonb"] | undefined | null,
	_neq?: ValueTypes["jsonb"] | undefined | null,
	_nin?: Array<ValueTypes["jsonb"]> | undefined | null
};
	/** mutation root */
["mutation_root"]: AliasType<{
delete_websliver_entry?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_entry_bool_exp"]},ValueTypes["websliver_entry_mutation_response"]],
delete_websliver_entry_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_entry"]],
delete_websliver_favicon?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_favicon_bool_exp"]},ValueTypes["websliver_favicon_mutation_response"]],
delete_websliver_favicon_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_favicon"]],
delete_websliver_return_type_affected_rows?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_return_type_affected_rows_bool_exp"]},ValueTypes["websliver_return_type_affected_rows_mutation_response"]],
delete_websliver_return_type_affected_rows_by_pk?: [{	affected_rows: ValueTypes["bigint"]},ValueTypes["websliver_return_type_affected_rows"]],
delete_websliver_return_type_tag?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_return_type_tag_bool_exp"]},ValueTypes["websliver_return_type_tag_mutation_response"]],
delete_websliver_return_type_tag_by_pk?: [{	name: string},ValueTypes["websliver_return_type_tag"]],
delete_websliver_user?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_user_bool_exp"]},ValueTypes["websliver_user_mutation_response"]],
delete_websliver_user_by_pk?: [{	uid: string},ValueTypes["websliver_user"]],
delete_websliver_visit?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_visit_bool_exp"]},ValueTypes["websliver_visit_mutation_response"]],
delete_websliver_visit_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_visit"]],
delete_websliver_workspace?: [{	/** filter the rows which have to be deleted */
	where: ValueTypes["websliver_workspace_bool_exp"]},ValueTypes["websliver_workspace_mutation_response"]],
delete_websliver_workspace_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_workspace"]],
insert_websliver_entry?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_entry_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_entry_on_conflict"] | undefined | null},ValueTypes["websliver_entry_mutation_response"]],
insert_websliver_entry_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_entry_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_entry_on_conflict"] | undefined | null},ValueTypes["websliver_entry"]],
insert_websliver_favicon?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_favicon_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_favicon_on_conflict"] | undefined | null},ValueTypes["websliver_favicon_mutation_response"]],
insert_websliver_favicon_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_favicon_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_favicon_on_conflict"] | undefined | null},ValueTypes["websliver_favicon"]],
insert_websliver_return_type_affected_rows?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_return_type_affected_rows_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_return_type_affected_rows_on_conflict"] | undefined | null},ValueTypes["websliver_return_type_affected_rows_mutation_response"]],
insert_websliver_return_type_affected_rows_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_return_type_affected_rows_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_return_type_affected_rows_on_conflict"] | undefined | null},ValueTypes["websliver_return_type_affected_rows"]],
insert_websliver_return_type_tag?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_return_type_tag_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_return_type_tag_on_conflict"] | undefined | null},ValueTypes["websliver_return_type_tag_mutation_response"]],
insert_websliver_return_type_tag_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_return_type_tag_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_return_type_tag_on_conflict"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
insert_websliver_user?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_user_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_user_on_conflict"] | undefined | null},ValueTypes["websliver_user_mutation_response"]],
insert_websliver_user_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_user_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_user_on_conflict"] | undefined | null},ValueTypes["websliver_user"]],
insert_websliver_visit?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_visit_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_visit_on_conflict"] | undefined | null},ValueTypes["websliver_visit_mutation_response"]],
insert_websliver_visit_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_visit_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_visit_on_conflict"] | undefined | null},ValueTypes["websliver_visit"]],
insert_websliver_workspace?: [{	/** the rows to be inserted */
	objects: Array<ValueTypes["websliver_workspace_insert_input"]>,	/** upsert condition */
	on_conflict?: ValueTypes["websliver_workspace_on_conflict"] | undefined | null},ValueTypes["websliver_workspace_mutation_response"]],
insert_websliver_workspace_one?: [{	/** the row to be inserted */
	object: ValueTypes["websliver_workspace_insert_input"],	/** upsert condition */
	on_conflict?: ValueTypes["websliver_workspace_on_conflict"] | undefined | null},ValueTypes["websliver_workspace"]],
update_websliver_entry?: [{	/** append existing jsonb value of filtered columns with new jsonb value */
	_append?: ValueTypes["websliver_entry_append_input"] | undefined | null,	/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
	_delete_at_path?: ValueTypes["websliver_entry_delete_at_path_input"] | undefined | null,	/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
	_delete_elem?: ValueTypes["websliver_entry_delete_elem_input"] | undefined | null,	/** delete key/value pair or string element. key/value pairs are matched based on their key value */
	_delete_key?: ValueTypes["websliver_entry_delete_key_input"] | undefined | null,	/** prepend existing jsonb value of filtered columns with new jsonb value */
	_prepend?: ValueTypes["websliver_entry_prepend_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_entry_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_entry_bool_exp"]},ValueTypes["websliver_entry_mutation_response"]],
update_websliver_entry_by_pk?: [{	/** append existing jsonb value of filtered columns with new jsonb value */
	_append?: ValueTypes["websliver_entry_append_input"] | undefined | null,	/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
	_delete_at_path?: ValueTypes["websliver_entry_delete_at_path_input"] | undefined | null,	/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
	_delete_elem?: ValueTypes["websliver_entry_delete_elem_input"] | undefined | null,	/** delete key/value pair or string element. key/value pairs are matched based on their key value */
	_delete_key?: ValueTypes["websliver_entry_delete_key_input"] | undefined | null,	/** prepend existing jsonb value of filtered columns with new jsonb value */
	_prepend?: ValueTypes["websliver_entry_prepend_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_entry_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_entry_pk_columns_input"]},ValueTypes["websliver_entry"]],
update_websliver_favicon?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_favicon_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_favicon_bool_exp"]},ValueTypes["websliver_favicon_mutation_response"]],
update_websliver_favicon_by_pk?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_favicon_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_favicon_pk_columns_input"]},ValueTypes["websliver_favicon"]],
update_websliver_return_type_affected_rows?: [{	/** increments the numeric columns with given value of the filtered values */
	_inc?: ValueTypes["websliver_return_type_affected_rows_inc_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_return_type_affected_rows_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_return_type_affected_rows_bool_exp"]},ValueTypes["websliver_return_type_affected_rows_mutation_response"]],
update_websliver_return_type_affected_rows_by_pk?: [{	/** increments the numeric columns with given value of the filtered values */
	_inc?: ValueTypes["websliver_return_type_affected_rows_inc_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_return_type_affected_rows_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_return_type_affected_rows_pk_columns_input"]},ValueTypes["websliver_return_type_affected_rows"]],
update_websliver_return_type_tag?: [{	/** increments the numeric columns with given value of the filtered values */
	_inc?: ValueTypes["websliver_return_type_tag_inc_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_return_type_tag_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_return_type_tag_bool_exp"]},ValueTypes["websliver_return_type_tag_mutation_response"]],
update_websliver_return_type_tag_by_pk?: [{	/** increments the numeric columns with given value of the filtered values */
	_inc?: ValueTypes["websliver_return_type_tag_inc_input"] | undefined | null,	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_return_type_tag_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_return_type_tag_pk_columns_input"]},ValueTypes["websliver_return_type_tag"]],
update_websliver_user?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_user_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_user_bool_exp"]},ValueTypes["websliver_user_mutation_response"]],
update_websliver_user_by_pk?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_user_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_user_pk_columns_input"]},ValueTypes["websliver_user"]],
update_websliver_visit?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_visit_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_visit_bool_exp"]},ValueTypes["websliver_visit_mutation_response"]],
update_websliver_visit_by_pk?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_visit_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_visit_pk_columns_input"]},ValueTypes["websliver_visit"]],
update_websliver_workspace?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_workspace_set_input"] | undefined | null,	/** filter the rows which have to be updated */
	where: ValueTypes["websliver_workspace_bool_exp"]},ValueTypes["websliver_workspace_mutation_response"]],
update_websliver_workspace_by_pk?: [{	/** sets the columns of the filtered rows to the given values */
	_set?: ValueTypes["websliver_workspace_set_input"] | undefined | null,	pk_columns: ValueTypes["websliver_workspace_pk_columns_input"]},ValueTypes["websliver_workspace"]],
websliver_copy_entries?: [{	/** input parameters for function "websliver_copy_entries" */
	args: ValueTypes["websliver_copy_entries_args"],	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
websliver_copy_entries_aff_rows?: [{	/** input parameters for function "websliver_copy_entries_aff_rows" */
	args: ValueTypes["websliver_copy_entries_aff_rows_args"],	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows"]],
websliver_replace_tags?: [{	/** input parameters for function "websliver_replace_tags" */
	args: ValueTypes["websliver_replace_tags_args"],	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
websliver_upsert_entries?: [{	/** input parameters for function "websliver_upsert_entries" */
	args: ValueTypes["websliver_upsert_entries_args"],	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
websliver_upsert_entries_aff_rows?: [{	/** input parameters for function "websliver_upsert_entries_aff_rows" */
	args: ValueTypes["websliver_upsert_entries_aff_rows_args"],	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows"]],
		__typename?: boolean | `@${string}`
}>;
	/** column ordering options */
["order_by"]:order_by;
	["query_root"]: AliasType<{
websliver_entry?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
websliver_entry_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry_aggregate"]],
websliver_entry_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_entry"]],
websliver_favicon?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_favicon_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_favicon_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null},ValueTypes["websliver_favicon"]],
websliver_favicon_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_favicon_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_favicon_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null},ValueTypes["websliver_favicon_aggregate"]],
websliver_favicon_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_favicon"]],
websliver_return_type_affected_rows?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows"]],
websliver_return_type_affected_rows_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows_aggregate"]],
websliver_return_type_affected_rows_by_pk?: [{	affected_rows: ValueTypes["bigint"]},ValueTypes["websliver_return_type_affected_rows"]],
websliver_return_type_tag?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
websliver_return_type_tag_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag_aggregate"]],
websliver_return_type_tag_by_pk?: [{	name: string},ValueTypes["websliver_return_type_tag"]],
websliver_search_tags?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
websliver_search_tags_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag_aggregate"]],
websliver_user?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_user_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_user_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_user_bool_exp"] | undefined | null},ValueTypes["websliver_user"]],
websliver_user_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_user_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_user_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_user_bool_exp"] | undefined | null},ValueTypes["websliver_user_aggregate"]],
websliver_user_by_pk?: [{	uid: string},ValueTypes["websliver_user"]],
websliver_visit?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit"]],
websliver_visit_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit_aggregate"]],
websliver_visit_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_visit"]],
websliver_workspace?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace"]],
websliver_workspace_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace_aggregate"]],
websliver_workspace_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_workspace"]],
		__typename?: boolean | `@${string}`
}>;
	["subscription_root"]: AliasType<{
websliver_entry?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
websliver_entry_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry_aggregate"]],
websliver_entry_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_entry"]],
websliver_favicon?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_favicon_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_favicon_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null},ValueTypes["websliver_favicon"]],
websliver_favicon_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_favicon_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_favicon_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null},ValueTypes["websliver_favicon_aggregate"]],
websliver_favicon_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_favicon"]],
websliver_return_type_affected_rows?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows"]],
websliver_return_type_affected_rows_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_affected_rows_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_affected_rows_aggregate"]],
websliver_return_type_affected_rows_by_pk?: [{	affected_rows: ValueTypes["bigint"]},ValueTypes["websliver_return_type_affected_rows"]],
websliver_return_type_tag?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
websliver_return_type_tag_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag_aggregate"]],
websliver_return_type_tag_by_pk?: [{	name: string},ValueTypes["websliver_return_type_tag"]],
websliver_search_tags?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
websliver_search_tags_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag_aggregate"]],
websliver_user?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_user_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_user_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_user_bool_exp"] | undefined | null},ValueTypes["websliver_user"]],
websliver_user_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_user_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_user_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_user_bool_exp"] | undefined | null},ValueTypes["websliver_user_aggregate"]],
websliver_user_by_pk?: [{	uid: string},ValueTypes["websliver_user"]],
websliver_visit?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit"]],
websliver_visit_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit_aggregate"]],
websliver_visit_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_visit"]],
websliver_workspace?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace"]],
websliver_workspace_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace_aggregate"]],
websliver_workspace_by_pk?: [{	uid: ValueTypes["uuid"]},ValueTypes["websliver_workspace"]],
		__typename?: boolean | `@${string}`
}>;
	["timestamptz"]:unknown;
	["timestamptz_cast_exp"]: {
	String?: ValueTypes["String_comparison_exp"] | undefined | null
};
	/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
["timestamptz_comparison_exp"]: {
	_cast?: ValueTypes["timestamptz_cast_exp"] | undefined | null,
	_eq?: ValueTypes["timestamptz"] | undefined | null,
	_gt?: ValueTypes["timestamptz"] | undefined | null,
	_gte?: ValueTypes["timestamptz"] | undefined | null,
	_in?: Array<ValueTypes["timestamptz"]> | undefined | null,
	_is_null?: boolean | undefined | null,
	_lt?: ValueTypes["timestamptz"] | undefined | null,
	_lte?: ValueTypes["timestamptz"] | undefined | null,
	_neq?: ValueTypes["timestamptz"] | undefined | null,
	_nin?: Array<ValueTypes["timestamptz"]> | undefined | null
};
	["uuid"]:unknown;
	["uuid_cast_exp"]: {
	String?: ValueTypes["String_comparison_exp"] | undefined | null
};
	/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
["uuid_comparison_exp"]: {
	_cast?: ValueTypes["uuid_cast_exp"] | undefined | null,
	_eq?: ValueTypes["uuid"] | undefined | null,
	_gt?: ValueTypes["uuid"] | undefined | null,
	_gte?: ValueTypes["uuid"] | undefined | null,
	_in?: Array<ValueTypes["uuid"]> | undefined | null,
	_is_null?: boolean | undefined | null,
	_lt?: ValueTypes["uuid"] | undefined | null,
	_lte?: ValueTypes["uuid"] | undefined | null,
	_neq?: ValueTypes["uuid"] | undefined | null,
	_nin?: Array<ValueTypes["uuid"]> | undefined | null
};
	["websliver_copy_entries_aff_rows_args"]: {
	destination_workspace_id?: ValueTypes["uuid"] | undefined | null,
	search_exclusions?: ValueTypes["jsonb"] | undefined | null,
	search_tags?: ValueTypes["jsonb"] | undefined | null,
	search_term?: string | undefined | null,
	source_workspace_id?: ValueTypes["uuid"] | undefined | null
};
	["websliver_copy_entries_args"]: {
	destination_workspace_id?: ValueTypes["uuid"] | undefined | null,
	search_exclusions?: ValueTypes["jsonb"] | undefined | null,
	search_tags?: ValueTypes["jsonb"] | undefined | null,
	search_term?: string | undefined | null,
	source_workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** An entry represents a saved location meta object. */
["websliver_entry"]: AliasType<{
	created_at?:boolean | `@${string}`,
	description?:boolean | `@${string}`,
	/** An object relationship */
	favicon?:ValueTypes["websliver_favicon"],
	favicon_id?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
tags?: [{	/** JSON select path */
	path?: string | undefined | null},boolean | `@${string}`],
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	/** An object relationship */
	workspace?:ValueTypes["websliver_workspace"],
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.entry" */
["websliver_entry_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_entry_aggregate_fields"],
	nodes?:ValueTypes["websliver_entry"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.entry" */
["websliver_entry_aggregate_fields"]: AliasType<{
count?: [{	columns?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_entry_max_fields"],
	min?:ValueTypes["websliver_entry_min_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** order by aggregate values of table "websliver.entry" */
["websliver_entry_aggregate_order_by"]: {
	count?: ValueTypes["order_by"] | undefined | null,
	max?: ValueTypes["websliver_entry_max_order_by"] | undefined | null,
	min?: ValueTypes["websliver_entry_min_order_by"] | undefined | null
};
	/** append existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_append_input"]: {
	tags?: ValueTypes["jsonb"] | undefined | null
};
	/** input type for inserting array relation for remote table "websliver.entry" */
["websliver_entry_arr_rel_insert_input"]: {
	data: Array<ValueTypes["websliver_entry_insert_input"]>,
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_entry_on_conflict"] | undefined | null
};
	/** Boolean expression to filter rows from the table "websliver.entry". All fields are combined with a logical 'AND'. */
["websliver_entry_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_entry_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_entry_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_entry_bool_exp"]> | undefined | null,
	created_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	description?: ValueTypes["String_comparison_exp"] | undefined | null,
	favicon?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null,
	favicon_id?: ValueTypes["uuid_comparison_exp"] | undefined | null,
	location?: ValueTypes["String_comparison_exp"] | undefined | null,
	tags?: ValueTypes["jsonb_comparison_exp"] | undefined | null,
	title?: ValueTypes["String_comparison_exp"] | undefined | null,
	uid?: ValueTypes["uuid_comparison_exp"] | undefined | null,
	updated_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null,
	workspace_id?: ValueTypes["uuid_comparison_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.entry" */
["websliver_entry_constraint"]:websliver_entry_constraint;
	/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
["websliver_entry_delete_at_path_input"]: {
	tags?: Array<string> | undefined | null
};
	/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
["websliver_entry_delete_elem_input"]: {
	tags?: number | undefined | null
};
	/** delete key/value pair or string element. key/value pairs are matched based on their key value */
["websliver_entry_delete_key_input"]: {
	tags?: string | undefined | null
};
	/** input type for inserting data into table "websliver.entry" */
["websliver_entry_insert_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	description?: string | undefined | null,
	favicon?: ValueTypes["websliver_favicon_obj_rel_insert_input"] | undefined | null,
	favicon_id?: ValueTypes["uuid"] | undefined | null,
	location?: string | undefined | null,
	tags?: ValueTypes["jsonb"] | undefined | null,
	title?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_obj_rel_insert_input"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** aggregate max on columns */
["websliver_entry_max_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	description?:boolean | `@${string}`,
	favicon_id?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by max() on columns of table "websliver.entry" */
["websliver_entry_max_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	description?: ValueTypes["order_by"] | undefined | null,
	favicon_id?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate min on columns */
["websliver_entry_min_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	description?:boolean | `@${string}`,
	favicon_id?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by min() on columns of table "websliver.entry" */
["websliver_entry_min_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	description?: ValueTypes["order_by"] | undefined | null,
	favicon_id?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** response of any mutation on the table "websliver.entry" */
["websliver_entry_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_entry"],
		__typename?: boolean | `@${string}`
}>;
	/** on_conflict condition type for table "websliver.entry" */
["websliver_entry_on_conflict"]: {
	constraint: ValueTypes["websliver_entry_constraint"],
	update_columns: Array<ValueTypes["websliver_entry_update_column"]>,
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.entry". */
["websliver_entry_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	description?: ValueTypes["order_by"] | undefined | null,
	favicon?: ValueTypes["websliver_favicon_order_by"] | undefined | null,
	favicon_id?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	tags?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_entry */
["websliver_entry_pk_columns_input"]: {
	uid: ValueTypes["uuid"]
};
	/** prepend existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_prepend_input"]: {
	tags?: ValueTypes["jsonb"] | undefined | null
};
	/** select columns of table "websliver.entry" */
["websliver_entry_select_column"]:websliver_entry_select_column;
	/** input type for updating data in table "websliver.entry" */
["websliver_entry_set_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	description?: string | undefined | null,
	favicon_id?: ValueTypes["uuid"] | undefined | null,
	location?: string | undefined | null,
	tags?: ValueTypes["jsonb"] | undefined | null,
	title?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** update columns of table "websliver.entry" */
["websliver_entry_update_column"]:websliver_entry_update_column;
	/** The favicon for a location. Reason for hash is that some favicon data is longer than what PG allows for unique constraints */
["websliver_favicon"]: AliasType<{
	created_at?:boolean | `@${string}`,
	data?:boolean | `@${string}`,
entries?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
entries_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry_aggregate"]],
	hash?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.favicon" */
["websliver_favicon_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_favicon_aggregate_fields"],
	nodes?:ValueTypes["websliver_favicon"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.favicon" */
["websliver_favicon_aggregate_fields"]: AliasType<{
count?: [{	columns?: Array<ValueTypes["websliver_favicon_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_favicon_max_fields"],
	min?:ValueTypes["websliver_favicon_min_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** Boolean expression to filter rows from the table "websliver.favicon". All fields are combined with a logical 'AND'. */
["websliver_favicon_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_favicon_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_favicon_bool_exp"]> | undefined | null,
	created_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	data?: ValueTypes["String_comparison_exp"] | undefined | null,
	entries?: ValueTypes["websliver_entry_bool_exp"] | undefined | null,
	hash?: ValueTypes["String_comparison_exp"] | undefined | null,
	uid?: ValueTypes["uuid_comparison_exp"] | undefined | null,
	updated_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.favicon" */
["websliver_favicon_constraint"]:websliver_favicon_constraint;
	/** input type for inserting data into table "websliver.favicon" */
["websliver_favicon_insert_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	data?: string | undefined | null,
	entries?: ValueTypes["websliver_entry_arr_rel_insert_input"] | undefined | null,
	hash?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null
};
	/** aggregate max on columns */
["websliver_favicon_max_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	data?:boolean | `@${string}`,
	hash?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate min on columns */
["websliver_favicon_min_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	data?:boolean | `@${string}`,
	hash?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** response of any mutation on the table "websliver.favicon" */
["websliver_favicon_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_favicon"],
		__typename?: boolean | `@${string}`
}>;
	/** input type for inserting object relation for remote table "websliver.favicon" */
["websliver_favicon_obj_rel_insert_input"]: {
	data: ValueTypes["websliver_favicon_insert_input"],
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_favicon_on_conflict"] | undefined | null
};
	/** on_conflict condition type for table "websliver.favicon" */
["websliver_favicon_on_conflict"]: {
	constraint: ValueTypes["websliver_favicon_constraint"],
	update_columns: Array<ValueTypes["websliver_favicon_update_column"]>,
	where?: ValueTypes["websliver_favicon_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.favicon". */
["websliver_favicon_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	data?: ValueTypes["order_by"] | undefined | null,
	entries_aggregate?: ValueTypes["websliver_entry_aggregate_order_by"] | undefined | null,
	hash?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_favicon */
["websliver_favicon_pk_columns_input"]: {
	uid: ValueTypes["uuid"]
};
	/** select columns of table "websliver.favicon" */
["websliver_favicon_select_column"]:websliver_favicon_select_column;
	/** input type for updating data in table "websliver.favicon" */
["websliver_favicon_set_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	data?: string | undefined | null,
	hash?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null
};
	/** update columns of table "websliver.favicon" */
["websliver_favicon_update_column"]:websliver_favicon_update_column;
	["websliver_replace_tags_args"]: {
	new_tags?: ValueTypes["jsonb"] | undefined | null,
	old_tags?: ValueTypes["jsonb"] | undefined | null,
	workspace?: ValueTypes["uuid"] | undefined | null
};
	/** columns and relationships of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_return_type_affected_rows_aggregate_fields"],
	nodes?:ValueTypes["websliver_return_type_affected_rows"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate_fields"]: AliasType<{
	avg?:ValueTypes["websliver_return_type_affected_rows_avg_fields"],
count?: [{	columns?: Array<ValueTypes["websliver_return_type_affected_rows_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_return_type_affected_rows_max_fields"],
	min?:ValueTypes["websliver_return_type_affected_rows_min_fields"],
	stddev?:ValueTypes["websliver_return_type_affected_rows_stddev_fields"],
	stddev_pop?:ValueTypes["websliver_return_type_affected_rows_stddev_pop_fields"],
	stddev_samp?:ValueTypes["websliver_return_type_affected_rows_stddev_samp_fields"],
	sum?:ValueTypes["websliver_return_type_affected_rows_sum_fields"],
	var_pop?:ValueTypes["websliver_return_type_affected_rows_var_pop_fields"],
	var_samp?:ValueTypes["websliver_return_type_affected_rows_var_samp_fields"],
	variance?:ValueTypes["websliver_return_type_affected_rows_variance_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate avg on columns */
["websliver_return_type_affected_rows_avg_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** Boolean expression to filter rows from the table "websliver.return_type_affected_rows". All fields are combined with a logical 'AND'. */
["websliver_return_type_affected_rows_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_return_type_affected_rows_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_return_type_affected_rows_bool_exp"]> | undefined | null,
	affected_rows?: ValueTypes["bigint_comparison_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_constraint"]:websliver_return_type_affected_rows_constraint;
	/** input type for incrementing numeric columns in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_inc_input"]: {
	affected_rows?: ValueTypes["bigint"] | undefined | null
};
	/** input type for inserting data into table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_insert_input"]: {
	affected_rows?: ValueTypes["bigint"] | undefined | null
};
	/** aggregate max on columns */
["websliver_return_type_affected_rows_max_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate min on columns */
["websliver_return_type_affected_rows_min_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** response of any mutation on the table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_return_type_affected_rows"],
		__typename?: boolean | `@${string}`
}>;
	/** on_conflict condition type for table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_on_conflict"]: {
	constraint: ValueTypes["websliver_return_type_affected_rows_constraint"],
	update_columns: Array<ValueTypes["websliver_return_type_affected_rows_update_column"]>,
	where?: ValueTypes["websliver_return_type_affected_rows_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.return_type_affected_rows". */
["websliver_return_type_affected_rows_order_by"]: {
	affected_rows?: ValueTypes["order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_return_type_affected_rows */
["websliver_return_type_affected_rows_pk_columns_input"]: {
	affected_rows: ValueTypes["bigint"]
};
	/** select columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_select_column"]:websliver_return_type_affected_rows_select_column;
	/** input type for updating data in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_set_input"]: {
	affected_rows?: ValueTypes["bigint"] | undefined | null
};
	/** aggregate stddev on columns */
["websliver_return_type_affected_rows_stddev_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate stddev_pop on columns */
["websliver_return_type_affected_rows_stddev_pop_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate stddev_samp on columns */
["websliver_return_type_affected_rows_stddev_samp_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate sum on columns */
["websliver_return_type_affected_rows_sum_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** update columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_update_column"]:websliver_return_type_affected_rows_update_column;
	/** aggregate var_pop on columns */
["websliver_return_type_affected_rows_var_pop_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate var_samp on columns */
["websliver_return_type_affected_rows_var_samp_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate variance on columns */
["websliver_return_type_affected_rows_variance_fields"]: AliasType<{
	affected_rows?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** Only used as a return type for replace_tags and search_tags */
["websliver_return_type_tag"]: AliasType<{
	name?:boolean | `@${string}`,
	uses?:boolean | `@${string}`,
	/** An object relationship */
	workspace?:ValueTypes["websliver_workspace"],
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_return_type_tag_aggregate_fields"],
	nodes?:ValueTypes["websliver_return_type_tag"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_fields"]: AliasType<{
	avg?:ValueTypes["websliver_return_type_tag_avg_fields"],
count?: [{	columns?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_return_type_tag_max_fields"],
	min?:ValueTypes["websliver_return_type_tag_min_fields"],
	stddev?:ValueTypes["websliver_return_type_tag_stddev_fields"],
	stddev_pop?:ValueTypes["websliver_return_type_tag_stddev_pop_fields"],
	stddev_samp?:ValueTypes["websliver_return_type_tag_stddev_samp_fields"],
	sum?:ValueTypes["websliver_return_type_tag_sum_fields"],
	var_pop?:ValueTypes["websliver_return_type_tag_var_pop_fields"],
	var_samp?:ValueTypes["websliver_return_type_tag_var_samp_fields"],
	variance?:ValueTypes["websliver_return_type_tag_variance_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** order by aggregate values of table "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_order_by"]: {
	avg?: ValueTypes["websliver_return_type_tag_avg_order_by"] | undefined | null,
	count?: ValueTypes["order_by"] | undefined | null,
	max?: ValueTypes["websliver_return_type_tag_max_order_by"] | undefined | null,
	min?: ValueTypes["websliver_return_type_tag_min_order_by"] | undefined | null,
	stddev?: ValueTypes["websliver_return_type_tag_stddev_order_by"] | undefined | null,
	stddev_pop?: ValueTypes["websliver_return_type_tag_stddev_pop_order_by"] | undefined | null,
	stddev_samp?: ValueTypes["websliver_return_type_tag_stddev_samp_order_by"] | undefined | null,
	sum?: ValueTypes["websliver_return_type_tag_sum_order_by"] | undefined | null,
	var_pop?: ValueTypes["websliver_return_type_tag_var_pop_order_by"] | undefined | null,
	var_samp?: ValueTypes["websliver_return_type_tag_var_samp_order_by"] | undefined | null,
	variance?: ValueTypes["websliver_return_type_tag_variance_order_by"] | undefined | null
};
	/** input type for inserting array relation for remote table "websliver.return_type_tag" */
["websliver_return_type_tag_arr_rel_insert_input"]: {
	data: Array<ValueTypes["websliver_return_type_tag_insert_input"]>,
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_return_type_tag_on_conflict"] | undefined | null
};
	/** aggregate avg on columns */
["websliver_return_type_tag_avg_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by avg() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_avg_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** Boolean expression to filter rows from the table "websliver.return_type_tag". All fields are combined with a logical 'AND'. */
["websliver_return_type_tag_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_return_type_tag_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_return_type_tag_bool_exp"]> | undefined | null,
	name?: ValueTypes["String_comparison_exp"] | undefined | null,
	uses?: ValueTypes["bigint_comparison_exp"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null,
	workspace_id?: ValueTypes["uuid_comparison_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.return_type_tag" */
["websliver_return_type_tag_constraint"]:websliver_return_type_tag_constraint;
	/** input type for incrementing numeric columns in table "websliver.return_type_tag" */
["websliver_return_type_tag_inc_input"]: {
	uses?: ValueTypes["bigint"] | undefined | null
};
	/** input type for inserting data into table "websliver.return_type_tag" */
["websliver_return_type_tag_insert_input"]: {
	name?: string | undefined | null,
	uses?: ValueTypes["bigint"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_obj_rel_insert_input"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** aggregate max on columns */
["websliver_return_type_tag_max_fields"]: AliasType<{
	name?:boolean | `@${string}`,
	uses?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by max() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_max_order_by"]: {
	name?: ValueTypes["order_by"] | undefined | null,
	uses?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate min on columns */
["websliver_return_type_tag_min_fields"]: AliasType<{
	name?:boolean | `@${string}`,
	uses?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by min() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_min_order_by"]: {
	name?: ValueTypes["order_by"] | undefined | null,
	uses?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** response of any mutation on the table "websliver.return_type_tag" */
["websliver_return_type_tag_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_return_type_tag"],
		__typename?: boolean | `@${string}`
}>;
	/** on_conflict condition type for table "websliver.return_type_tag" */
["websliver_return_type_tag_on_conflict"]: {
	constraint: ValueTypes["websliver_return_type_tag_constraint"],
	update_columns: Array<ValueTypes["websliver_return_type_tag_update_column"]>,
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.return_type_tag". */
["websliver_return_type_tag_order_by"]: {
	name?: ValueTypes["order_by"] | undefined | null,
	uses?: ValueTypes["order_by"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_return_type_tag */
["websliver_return_type_tag_pk_columns_input"]: {
	name: string
};
	/** select columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_select_column"]:websliver_return_type_tag_select_column;
	/** input type for updating data in table "websliver.return_type_tag" */
["websliver_return_type_tag_set_input"]: {
	name?: string | undefined | null,
	uses?: ValueTypes["bigint"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** aggregate stddev on columns */
["websliver_return_type_tag_stddev_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by stddev() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate stddev_pop on columns */
["websliver_return_type_tag_stddev_pop_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by stddev_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_pop_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate stddev_samp on columns */
["websliver_return_type_tag_stddev_samp_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by stddev_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_samp_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate sum on columns */
["websliver_return_type_tag_sum_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by sum() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_sum_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** update columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_update_column"]:websliver_return_type_tag_update_column;
	/** aggregate var_pop on columns */
["websliver_return_type_tag_var_pop_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by var_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_pop_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate var_samp on columns */
["websliver_return_type_tag_var_samp_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by var_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_samp_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate variance on columns */
["websliver_return_type_tag_variance_fields"]: AliasType<{
	uses?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by variance() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_variance_order_by"]: {
	uses?: ValueTypes["order_by"] | undefined | null
};
	["websliver_upsert_entries_aff_rows_args"]: {
	entries?: ValueTypes["jsonb"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	["websliver_upsert_entries_args"]: {
	entries?: ValueTypes["jsonb"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** The owner of one or more devices and one or more workspaces */
["websliver_user"]: AliasType<{
	created_at?:boolean | `@${string}`,
	/** managed by auth */
	email?:boolean | `@${string}`,
	last_login_at?:boolean | `@${string}`,
	name?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	/** managed by auth */
	username?:boolean | `@${string}`,
workspaces?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace"]],
workspaces_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_workspace_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null},ValueTypes["websliver_workspace_aggregate"]],
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.user" */
["websliver_user_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_user_aggregate_fields"],
	nodes?:ValueTypes["websliver_user"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.user" */
["websliver_user_aggregate_fields"]: AliasType<{
count?: [{	columns?: Array<ValueTypes["websliver_user_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_user_max_fields"],
	min?:ValueTypes["websliver_user_min_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** Boolean expression to filter rows from the table "websliver.user". All fields are combined with a logical 'AND'. */
["websliver_user_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_user_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_user_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_user_bool_exp"]> | undefined | null,
	created_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	email?: ValueTypes["String_comparison_exp"] | undefined | null,
	last_login_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	name?: ValueTypes["String_comparison_exp"] | undefined | null,
	uid?: ValueTypes["String_comparison_exp"] | undefined | null,
	updated_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	username?: ValueTypes["String_comparison_exp"] | undefined | null,
	workspaces?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.user" */
["websliver_user_constraint"]:websliver_user_constraint;
	/** input type for inserting data into table "websliver.user" */
["websliver_user_insert_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	/** managed by auth */
	email?: string | undefined | null,
	last_login_at?: ValueTypes["timestamptz"] | undefined | null,
	name?: string | undefined | null,
	uid?: string | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	/** managed by auth */
	username?: string | undefined | null,
	workspaces?: ValueTypes["websliver_workspace_arr_rel_insert_input"] | undefined | null
};
	/** aggregate max on columns */
["websliver_user_max_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	/** managed by auth */
	email?:boolean | `@${string}`,
	last_login_at?:boolean | `@${string}`,
	name?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	/** managed by auth */
	username?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregate min on columns */
["websliver_user_min_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	/** managed by auth */
	email?:boolean | `@${string}`,
	last_login_at?:boolean | `@${string}`,
	name?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	/** managed by auth */
	username?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** response of any mutation on the table "websliver.user" */
["websliver_user_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_user"],
		__typename?: boolean | `@${string}`
}>;
	/** input type for inserting object relation for remote table "websliver.user" */
["websliver_user_obj_rel_insert_input"]: {
	data: ValueTypes["websliver_user_insert_input"],
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_user_on_conflict"] | undefined | null
};
	/** on_conflict condition type for table "websliver.user" */
["websliver_user_on_conflict"]: {
	constraint: ValueTypes["websliver_user_constraint"],
	update_columns: Array<ValueTypes["websliver_user_update_column"]>,
	where?: ValueTypes["websliver_user_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.user". */
["websliver_user_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	email?: ValueTypes["order_by"] | undefined | null,
	last_login_at?: ValueTypes["order_by"] | undefined | null,
	name?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	username?: ValueTypes["order_by"] | undefined | null,
	workspaces_aggregate?: ValueTypes["websliver_workspace_aggregate_order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_user */
["websliver_user_pk_columns_input"]: {
	uid: string
};
	/** select columns of table "websliver.user" */
["websliver_user_select_column"]:websliver_user_select_column;
	/** input type for updating data in table "websliver.user" */
["websliver_user_set_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	/** managed by auth */
	email?: string | undefined | null,
	last_login_at?: ValueTypes["timestamptz"] | undefined | null,
	name?: string | undefined | null,
	uid?: string | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	/** managed by auth */
	username?: string | undefined | null
};
	/** update columns of table "websliver.user" */
["websliver_user_update_column"]:websliver_user_update_column;
	/** A historical visit of a location. 'source_id' represents the parent visit if any. */
["websliver_visit"]: AliasType<{
	created_at?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
	source_title?:boolean | `@${string}`,
	source_url?:boolean | `@${string}`,
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	/** An object relationship */
	workspace?:ValueTypes["websliver_workspace"],
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.visit" */
["websliver_visit_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_visit_aggregate_fields"],
	nodes?:ValueTypes["websliver_visit"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.visit" */
["websliver_visit_aggregate_fields"]: AliasType<{
count?: [{	columns?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_visit_max_fields"],
	min?:ValueTypes["websliver_visit_min_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** order by aggregate values of table "websliver.visit" */
["websliver_visit_aggregate_order_by"]: {
	count?: ValueTypes["order_by"] | undefined | null,
	max?: ValueTypes["websliver_visit_max_order_by"] | undefined | null,
	min?: ValueTypes["websliver_visit_min_order_by"] | undefined | null
};
	/** input type for inserting array relation for remote table "websliver.visit" */
["websliver_visit_arr_rel_insert_input"]: {
	data: Array<ValueTypes["websliver_visit_insert_input"]>,
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_visit_on_conflict"] | undefined | null
};
	/** Boolean expression to filter rows from the table "websliver.visit". All fields are combined with a logical 'AND'. */
["websliver_visit_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_visit_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_visit_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_visit_bool_exp"]> | undefined | null,
	created_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	location?: ValueTypes["String_comparison_exp"] | undefined | null,
	source_title?: ValueTypes["String_comparison_exp"] | undefined | null,
	source_url?: ValueTypes["String_comparison_exp"] | undefined | null,
	title?: ValueTypes["String_comparison_exp"] | undefined | null,
	uid?: ValueTypes["uuid_comparison_exp"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null,
	workspace_id?: ValueTypes["uuid_comparison_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.visit" */
["websliver_visit_constraint"]:websliver_visit_constraint;
	/** input type for inserting data into table "websliver.visit" */
["websliver_visit_insert_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	location?: string | undefined | null,
	source_title?: string | undefined | null,
	source_url?: string | undefined | null,
	title?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_obj_rel_insert_input"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** aggregate max on columns */
["websliver_visit_max_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
	source_title?:boolean | `@${string}`,
	source_url?:boolean | `@${string}`,
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by max() on columns of table "websliver.visit" */
["websliver_visit_max_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	source_title?: ValueTypes["order_by"] | undefined | null,
	source_url?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate min on columns */
["websliver_visit_min_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	location?:boolean | `@${string}`,
	source_title?:boolean | `@${string}`,
	source_url?:boolean | `@${string}`,
	title?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	workspace_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by min() on columns of table "websliver.visit" */
["websliver_visit_min_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	source_title?: ValueTypes["order_by"] | undefined | null,
	source_url?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** response of any mutation on the table "websliver.visit" */
["websliver_visit_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_visit"],
		__typename?: boolean | `@${string}`
}>;
	/** on_conflict condition type for table "websliver.visit" */
["websliver_visit_on_conflict"]: {
	constraint: ValueTypes["websliver_visit_constraint"],
	update_columns: Array<ValueTypes["websliver_visit_update_column"]>,
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.visit". */
["websliver_visit_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	location?: ValueTypes["order_by"] | undefined | null,
	source_title?: ValueTypes["order_by"] | undefined | null,
	source_url?: ValueTypes["order_by"] | undefined | null,
	title?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	workspace?: ValueTypes["websliver_workspace_order_by"] | undefined | null,
	workspace_id?: ValueTypes["order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_visit */
["websliver_visit_pk_columns_input"]: {
	uid: ValueTypes["uuid"]
};
	/** select columns of table "websliver.visit" */
["websliver_visit_select_column"]:websliver_visit_select_column;
	/** input type for updating data in table "websliver.visit" */
["websliver_visit_set_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	location?: string | undefined | null,
	source_title?: string | undefined | null,
	source_url?: string | undefined | null,
	title?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	workspace_id?: ValueTypes["uuid"] | undefined | null
};
	/** update columns of table "websliver.visit" */
["websliver_visit_update_column"]:websliver_visit_update_column;
	/** A workspace represents an isolated group of tags and bookmarks */
["websliver_workspace"]: AliasType<{
	created_at?:boolean | `@${string}`,
	default?:boolean | `@${string}`,
entries?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry"]],
entries_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_entry_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_entry_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_entry_bool_exp"] | undefined | null},ValueTypes["websliver_entry_aggregate"]],
	name?:boolean | `@${string}`,
return_type_tags?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag"]],
return_type_tags_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_return_type_tag_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_return_type_tag_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null},ValueTypes["websliver_return_type_tag_aggregate"]],
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	/** An object relationship */
	user?:ValueTypes["websliver_user"],
	user_id?:boolean | `@${string}`,
visits?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit"]],
visits_aggregate?: [{	/** distinct select on columns */
	distinct_on?: Array<ValueTypes["websliver_visit_select_column"]> | undefined | null,	/** limit the number of rows returned */
	limit?: number | undefined | null,	/** skip the first n rows. Use only with order_by */
	offset?: number | undefined | null,	/** sort the rows by one or more columns */
	order_by?: Array<ValueTypes["websliver_visit_order_by"]> | undefined | null,	/** filter the rows returned */
	where?: ValueTypes["websliver_visit_bool_exp"] | undefined | null},ValueTypes["websliver_visit_aggregate"]],
		__typename?: boolean | `@${string}`
}>;
	/** aggregated selection of "websliver.workspace" */
["websliver_workspace_aggregate"]: AliasType<{
	aggregate?:ValueTypes["websliver_workspace_aggregate_fields"],
	nodes?:ValueTypes["websliver_workspace"],
		__typename?: boolean | `@${string}`
}>;
	/** aggregate fields of "websliver.workspace" */
["websliver_workspace_aggregate_fields"]: AliasType<{
count?: [{	columns?: Array<ValueTypes["websliver_workspace_select_column"]> | undefined | null,	distinct?: boolean | undefined | null},boolean | `@${string}`],
	max?:ValueTypes["websliver_workspace_max_fields"],
	min?:ValueTypes["websliver_workspace_min_fields"],
		__typename?: boolean | `@${string}`
}>;
	/** order by aggregate values of table "websliver.workspace" */
["websliver_workspace_aggregate_order_by"]: {
	count?: ValueTypes["order_by"] | undefined | null,
	max?: ValueTypes["websliver_workspace_max_order_by"] | undefined | null,
	min?: ValueTypes["websliver_workspace_min_order_by"] | undefined | null
};
	/** input type for inserting array relation for remote table "websliver.workspace" */
["websliver_workspace_arr_rel_insert_input"]: {
	data: Array<ValueTypes["websliver_workspace_insert_input"]>,
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_workspace_on_conflict"] | undefined | null
};
	/** Boolean expression to filter rows from the table "websliver.workspace". All fields are combined with a logical 'AND'. */
["websliver_workspace_bool_exp"]: {
	_and?: Array<ValueTypes["websliver_workspace_bool_exp"]> | undefined | null,
	_not?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null,
	_or?: Array<ValueTypes["websliver_workspace_bool_exp"]> | undefined | null,
	created_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	default?: ValueTypes["Boolean_comparison_exp"] | undefined | null,
	entries?: ValueTypes["websliver_entry_bool_exp"] | undefined | null,
	name?: ValueTypes["String_comparison_exp"] | undefined | null,
	return_type_tags?: ValueTypes["websliver_return_type_tag_bool_exp"] | undefined | null,
	uid?: ValueTypes["uuid_comparison_exp"] | undefined | null,
	updated_at?: ValueTypes["timestamptz_comparison_exp"] | undefined | null,
	user?: ValueTypes["websliver_user_bool_exp"] | undefined | null,
	user_id?: ValueTypes["String_comparison_exp"] | undefined | null,
	visits?: ValueTypes["websliver_visit_bool_exp"] | undefined | null
};
	/** unique or primary key constraints on table "websliver.workspace" */
["websliver_workspace_constraint"]:websliver_workspace_constraint;
	/** input type for inserting data into table "websliver.workspace" */
["websliver_workspace_insert_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	default?: boolean | undefined | null,
	entries?: ValueTypes["websliver_entry_arr_rel_insert_input"] | undefined | null,
	name?: string | undefined | null,
	return_type_tags?: ValueTypes["websliver_return_type_tag_arr_rel_insert_input"] | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	user?: ValueTypes["websliver_user_obj_rel_insert_input"] | undefined | null,
	user_id?: string | undefined | null,
	visits?: ValueTypes["websliver_visit_arr_rel_insert_input"] | undefined | null
};
	/** aggregate max on columns */
["websliver_workspace_max_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	name?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	user_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by max() on columns of table "websliver.workspace" */
["websliver_workspace_max_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	name?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	user_id?: ValueTypes["order_by"] | undefined | null
};
	/** aggregate min on columns */
["websliver_workspace_min_fields"]: AliasType<{
	created_at?:boolean | `@${string}`,
	name?:boolean | `@${string}`,
	uid?:boolean | `@${string}`,
	updated_at?:boolean | `@${string}`,
	user_id?:boolean | `@${string}`,
		__typename?: boolean | `@${string}`
}>;
	/** order by min() on columns of table "websliver.workspace" */
["websliver_workspace_min_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	name?: ValueTypes["order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	user_id?: ValueTypes["order_by"] | undefined | null
};
	/** response of any mutation on the table "websliver.workspace" */
["websliver_workspace_mutation_response"]: AliasType<{
	/** number of rows affected by the mutation */
	affected_rows?:boolean | `@${string}`,
	/** data from the rows affected by the mutation */
	returning?:ValueTypes["websliver_workspace"],
		__typename?: boolean | `@${string}`
}>;
	/** input type for inserting object relation for remote table "websliver.workspace" */
["websliver_workspace_obj_rel_insert_input"]: {
	data: ValueTypes["websliver_workspace_insert_input"],
	/** upsert condition */
	on_conflict?: ValueTypes["websliver_workspace_on_conflict"] | undefined | null
};
	/** on_conflict condition type for table "websliver.workspace" */
["websliver_workspace_on_conflict"]: {
	constraint: ValueTypes["websliver_workspace_constraint"],
	update_columns: Array<ValueTypes["websliver_workspace_update_column"]>,
	where?: ValueTypes["websliver_workspace_bool_exp"] | undefined | null
};
	/** Ordering options when selecting data from "websliver.workspace". */
["websliver_workspace_order_by"]: {
	created_at?: ValueTypes["order_by"] | undefined | null,
	default?: ValueTypes["order_by"] | undefined | null,
	entries_aggregate?: ValueTypes["websliver_entry_aggregate_order_by"] | undefined | null,
	name?: ValueTypes["order_by"] | undefined | null,
	return_type_tags_aggregate?: ValueTypes["websliver_return_type_tag_aggregate_order_by"] | undefined | null,
	uid?: ValueTypes["order_by"] | undefined | null,
	updated_at?: ValueTypes["order_by"] | undefined | null,
	user?: ValueTypes["websliver_user_order_by"] | undefined | null,
	user_id?: ValueTypes["order_by"] | undefined | null,
	visits_aggregate?: ValueTypes["websliver_visit_aggregate_order_by"] | undefined | null
};
	/** primary key columns input for table: websliver_workspace */
["websliver_workspace_pk_columns_input"]: {
	uid: ValueTypes["uuid"]
};
	/** select columns of table "websliver.workspace" */
["websliver_workspace_select_column"]:websliver_workspace_select_column;
	/** input type for updating data in table "websliver.workspace" */
["websliver_workspace_set_input"]: {
	created_at?: ValueTypes["timestamptz"] | undefined | null,
	default?: boolean | undefined | null,
	name?: string | undefined | null,
	uid?: ValueTypes["uuid"] | undefined | null,
	updated_at?: ValueTypes["timestamptz"] | undefined | null,
	user_id?: string | undefined | null
};
	/** update columns of table "websliver.workspace" */
["websliver_workspace_update_column"]:websliver_workspace_update_column
  }

export type ModelTypes = {
    ["Boolean_cast_exp"]: GraphQLTypes["Boolean_cast_exp"];
	/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
["Boolean_comparison_exp"]: GraphQLTypes["Boolean_comparison_exp"];
	/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
["String_comparison_exp"]: GraphQLTypes["String_comparison_exp"];
	["bigint"]:any;
	["bigint_cast_exp"]: GraphQLTypes["bigint_cast_exp"];
	/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
["bigint_comparison_exp"]: GraphQLTypes["bigint_comparison_exp"];
	["jsonb"]:any;
	["jsonb_cast_exp"]: GraphQLTypes["jsonb_cast_exp"];
	/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
["jsonb_comparison_exp"]: GraphQLTypes["jsonb_comparison_exp"];
	/** mutation root */
["mutation_root"]: {
		/** delete data from the table: "websliver.entry" */
	delete_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.entry" */
	delete_websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** delete data from the table: "websliver.favicon" */
	delete_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.favicon" */
	delete_websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** delete data from the table: "websliver.return_type_affected_rows" */
	delete_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.return_type_affected_rows" */
	delete_websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** delete data from the table: "websliver.return_type_tag" */
	delete_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.return_type_tag" */
	delete_websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** delete data from the table: "websliver.user" */
	delete_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.user" */
	delete_websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** delete data from the table: "websliver.visit" */
	delete_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.visit" */
	delete_websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** delete data from the table: "websliver.workspace" */
	delete_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.workspace" */
	delete_websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined,
	/** insert data into the table: "websliver.entry" */
	insert_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.entry" */
	insert_websliver_entry_one?: GraphQLTypes["websliver_entry"] | undefined,
	/** insert data into the table: "websliver.favicon" */
	insert_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.favicon" */
	insert_websliver_favicon_one?: GraphQLTypes["websliver_favicon"] | undefined,
	/** insert data into the table: "websliver.return_type_affected_rows" */
	insert_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.return_type_affected_rows" */
	insert_websliver_return_type_affected_rows_one?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** insert data into the table: "websliver.return_type_tag" */
	insert_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.return_type_tag" */
	insert_websliver_return_type_tag_one?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** insert data into the table: "websliver.user" */
	insert_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.user" */
	insert_websliver_user_one?: GraphQLTypes["websliver_user"] | undefined,
	/** insert data into the table: "websliver.visit" */
	insert_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.visit" */
	insert_websliver_visit_one?: GraphQLTypes["websliver_visit"] | undefined,
	/** insert data into the table: "websliver.workspace" */
	insert_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.workspace" */
	insert_websliver_workspace_one?: GraphQLTypes["websliver_workspace"] | undefined,
	/** update data of the table: "websliver.entry" */
	update_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** update single row of the table: "websliver.entry" */
	update_websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** update data of the table: "websliver.favicon" */
	update_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** update single row of the table: "websliver.favicon" */
	update_websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** update data of the table: "websliver.return_type_affected_rows" */
	update_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** update single row of the table: "websliver.return_type_affected_rows" */
	update_websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** update data of the table: "websliver.return_type_tag" */
	update_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** update single row of the table: "websliver.return_type_tag" */
	update_websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** update data of the table: "websliver.user" */
	update_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** update single row of the table: "websliver.user" */
	update_websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** update data of the table: "websliver.visit" */
	update_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** update single row of the table: "websliver.visit" */
	update_websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** update data of the table: "websliver.workspace" */
	update_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** update single row of the table: "websliver.workspace" */
	update_websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined,
	/** execute VOLATILE function "websliver.copy_entries" which returns "websliver.entry" */
	websliver_copy_entries: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.copy_entries_aff_rows" which returns "websliver.return_type_affected_rows" */
	websliver_copy_entries_aff_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** execute VOLATILE function "websliver.replace_tags" which returns "websliver.entry" */
	websliver_replace_tags: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.upsert_entries" which returns "websliver.entry" */
	websliver_upsert_entries: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.upsert_entries_aff_rows" which returns "websliver.return_type_affected_rows" */
	websliver_upsert_entries_aff_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** column ordering options */
["order_by"]: GraphQLTypes["order_by"];
	["query_root"]: {
		/** fetch data from the table: "websliver.entry" */
	websliver_entry: Array<GraphQLTypes["websliver_entry"]>,
	/** fetch aggregated fields from the table: "websliver.entry" */
	websliver_entry_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	/** fetch data from the table: "websliver.entry" using primary key columns */
	websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** fetch data from the table: "websliver.favicon" */
	websliver_favicon: Array<GraphQLTypes["websliver_favicon"]>,
	/** fetch aggregated fields from the table: "websliver.favicon" */
	websliver_favicon_aggregate: GraphQLTypes["websliver_favicon_aggregate"],
	/** fetch data from the table: "websliver.favicon" using primary key columns */
	websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** fetch data from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows_aggregate: GraphQLTypes["websliver_return_type_affected_rows_aggregate"],
	/** fetch data from the table: "websliver.return_type_affected_rows" using primary key columns */
	websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** fetch data from the table: "websliver.return_type_tag" */
	websliver_return_type_tag: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_tag" */
	websliver_return_type_tag_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.return_type_tag" using primary key columns */
	websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** execute function "websliver.search_tags" which returns "websliver.return_type_tag" */
	websliver_search_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** execute function "websliver.search_tags" and query aggregates on result of table type "websliver.return_type_tag" */
	websliver_search_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.user" */
	websliver_user: Array<GraphQLTypes["websliver_user"]>,
	/** fetch aggregated fields from the table: "websliver.user" */
	websliver_user_aggregate: GraphQLTypes["websliver_user_aggregate"],
	/** fetch data from the table: "websliver.user" using primary key columns */
	websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** fetch data from the table: "websliver.visit" */
	websliver_visit: Array<GraphQLTypes["websliver_visit"]>,
	/** fetch aggregated fields from the table: "websliver.visit" */
	websliver_visit_aggregate: GraphQLTypes["websliver_visit_aggregate"],
	/** fetch data from the table: "websliver.visit" using primary key columns */
	websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** fetch data from the table: "websliver.workspace" */
	websliver_workspace: Array<GraphQLTypes["websliver_workspace"]>,
	/** fetch aggregated fields from the table: "websliver.workspace" */
	websliver_workspace_aggregate: GraphQLTypes["websliver_workspace_aggregate"],
	/** fetch data from the table: "websliver.workspace" using primary key columns */
	websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined
};
	["subscription_root"]: {
		/** fetch data from the table: "websliver.entry" */
	websliver_entry: Array<GraphQLTypes["websliver_entry"]>,
	/** fetch aggregated fields from the table: "websliver.entry" */
	websliver_entry_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	/** fetch data from the table: "websliver.entry" using primary key columns */
	websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** fetch data from the table: "websliver.favicon" */
	websliver_favicon: Array<GraphQLTypes["websliver_favicon"]>,
	/** fetch aggregated fields from the table: "websliver.favicon" */
	websliver_favicon_aggregate: GraphQLTypes["websliver_favicon_aggregate"],
	/** fetch data from the table: "websliver.favicon" using primary key columns */
	websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** fetch data from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows_aggregate: GraphQLTypes["websliver_return_type_affected_rows_aggregate"],
	/** fetch data from the table: "websliver.return_type_affected_rows" using primary key columns */
	websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** fetch data from the table: "websliver.return_type_tag" */
	websliver_return_type_tag: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_tag" */
	websliver_return_type_tag_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.return_type_tag" using primary key columns */
	websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** execute function "websliver.search_tags" which returns "websliver.return_type_tag" */
	websliver_search_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** execute function "websliver.search_tags" and query aggregates on result of table type "websliver.return_type_tag" */
	websliver_search_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.user" */
	websliver_user: Array<GraphQLTypes["websliver_user"]>,
	/** fetch aggregated fields from the table: "websliver.user" */
	websliver_user_aggregate: GraphQLTypes["websliver_user_aggregate"],
	/** fetch data from the table: "websliver.user" using primary key columns */
	websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** fetch data from the table: "websliver.visit" */
	websliver_visit: Array<GraphQLTypes["websliver_visit"]>,
	/** fetch aggregated fields from the table: "websliver.visit" */
	websliver_visit_aggregate: GraphQLTypes["websliver_visit_aggregate"],
	/** fetch data from the table: "websliver.visit" using primary key columns */
	websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** fetch data from the table: "websliver.workspace" */
	websliver_workspace: Array<GraphQLTypes["websliver_workspace"]>,
	/** fetch aggregated fields from the table: "websliver.workspace" */
	websliver_workspace_aggregate: GraphQLTypes["websliver_workspace_aggregate"],
	/** fetch data from the table: "websliver.workspace" using primary key columns */
	websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined
};
	["timestamptz"]:any;
	["timestamptz_cast_exp"]: GraphQLTypes["timestamptz_cast_exp"];
	/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
["timestamptz_comparison_exp"]: GraphQLTypes["timestamptz_comparison_exp"];
	["uuid"]:any;
	["uuid_cast_exp"]: GraphQLTypes["uuid_cast_exp"];
	/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
["uuid_comparison_exp"]: GraphQLTypes["uuid_comparison_exp"];
	["websliver_copy_entries_aff_rows_args"]: GraphQLTypes["websliver_copy_entries_aff_rows_args"];
	["websliver_copy_entries_args"]: GraphQLTypes["websliver_copy_entries_args"];
	/** An entry represents a saved location meta object. */
["websliver_entry"]: {
		created_at: GraphQLTypes["timestamptz"],
	description: string,
	/** An object relationship */
	favicon?: GraphQLTypes["websliver_favicon"] | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location: string,
	tags: GraphQLTypes["jsonb"],
	title: string,
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.entry" */
["websliver_entry_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_entry_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_entry"]>
};
	/** aggregate fields of "websliver.entry" */
["websliver_entry_aggregate_fields"]: {
		count: number,
	max?: GraphQLTypes["websliver_entry_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_entry_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.entry" */
["websliver_entry_aggregate_order_by"]: GraphQLTypes["websliver_entry_aggregate_order_by"];
	/** append existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_append_input"]: GraphQLTypes["websliver_entry_append_input"];
	/** input type for inserting array relation for remote table "websliver.entry" */
["websliver_entry_arr_rel_insert_input"]: GraphQLTypes["websliver_entry_arr_rel_insert_input"];
	/** Boolean expression to filter rows from the table "websliver.entry". All fields are combined with a logical 'AND'. */
["websliver_entry_bool_exp"]: GraphQLTypes["websliver_entry_bool_exp"];
	/** unique or primary key constraints on table "websliver.entry" */
["websliver_entry_constraint"]: GraphQLTypes["websliver_entry_constraint"];
	/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
["websliver_entry_delete_at_path_input"]: GraphQLTypes["websliver_entry_delete_at_path_input"];
	/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
["websliver_entry_delete_elem_input"]: GraphQLTypes["websliver_entry_delete_elem_input"];
	/** delete key/value pair or string element. key/value pairs are matched based on their key value */
["websliver_entry_delete_key_input"]: GraphQLTypes["websliver_entry_delete_key_input"];
	/** input type for inserting data into table "websliver.entry" */
["websliver_entry_insert_input"]: GraphQLTypes["websliver_entry_insert_input"];
	/** aggregate max on columns */
["websliver_entry_max_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.entry" */
["websliver_entry_max_order_by"]: GraphQLTypes["websliver_entry_max_order_by"];
	/** aggregate min on columns */
["websliver_entry_min_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.entry" */
["websliver_entry_min_order_by"]: GraphQLTypes["websliver_entry_min_order_by"];
	/** response of any mutation on the table "websliver.entry" */
["websliver_entry_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_entry"]>
};
	/** on_conflict condition type for table "websliver.entry" */
["websliver_entry_on_conflict"]: GraphQLTypes["websliver_entry_on_conflict"];
	/** Ordering options when selecting data from "websliver.entry". */
["websliver_entry_order_by"]: GraphQLTypes["websliver_entry_order_by"];
	/** primary key columns input for table: websliver_entry */
["websliver_entry_pk_columns_input"]: GraphQLTypes["websliver_entry_pk_columns_input"];
	/** prepend existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_prepend_input"]: GraphQLTypes["websliver_entry_prepend_input"];
	/** select columns of table "websliver.entry" */
["websliver_entry_select_column"]: GraphQLTypes["websliver_entry_select_column"];
	/** input type for updating data in table "websliver.entry" */
["websliver_entry_set_input"]: GraphQLTypes["websliver_entry_set_input"];
	/** update columns of table "websliver.entry" */
["websliver_entry_update_column"]: GraphQLTypes["websliver_entry_update_column"];
	/** The favicon for a location. Reason for hash is that some favicon data is longer than what PG allows for unique constraints */
["websliver_favicon"]: {
		created_at: GraphQLTypes["timestamptz"],
	data: string,
	/** An array relationship */
	entries: Array<GraphQLTypes["websliver_entry"]>,
	/** An aggregate relationship */
	entries_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	hash: string,
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"]
};
	/** aggregated selection of "websliver.favicon" */
["websliver_favicon_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_favicon_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_favicon"]>
};
	/** aggregate fields of "websliver.favicon" */
["websliver_favicon_aggregate_fields"]: {
		count: number,
	max?: GraphQLTypes["websliver_favicon_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_favicon_min_fields"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.favicon". All fields are combined with a logical 'AND'. */
["websliver_favicon_bool_exp"]: GraphQLTypes["websliver_favicon_bool_exp"];
	/** unique or primary key constraints on table "websliver.favicon" */
["websliver_favicon_constraint"]: GraphQLTypes["websliver_favicon_constraint"];
	/** input type for inserting data into table "websliver.favicon" */
["websliver_favicon_insert_input"]: GraphQLTypes["websliver_favicon_insert_input"];
	/** aggregate max on columns */
["websliver_favicon_max_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** aggregate min on columns */
["websliver_favicon_min_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** response of any mutation on the table "websliver.favicon" */
["websliver_favicon_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_favicon"]>
};
	/** input type for inserting object relation for remote table "websliver.favicon" */
["websliver_favicon_obj_rel_insert_input"]: GraphQLTypes["websliver_favicon_obj_rel_insert_input"];
	/** on_conflict condition type for table "websliver.favicon" */
["websliver_favicon_on_conflict"]: GraphQLTypes["websliver_favicon_on_conflict"];
	/** Ordering options when selecting data from "websliver.favicon". */
["websliver_favicon_order_by"]: GraphQLTypes["websliver_favicon_order_by"];
	/** primary key columns input for table: websliver_favicon */
["websliver_favicon_pk_columns_input"]: GraphQLTypes["websliver_favicon_pk_columns_input"];
	/** select columns of table "websliver.favicon" */
["websliver_favicon_select_column"]: GraphQLTypes["websliver_favicon_select_column"];
	/** input type for updating data in table "websliver.favicon" */
["websliver_favicon_set_input"]: GraphQLTypes["websliver_favicon_set_input"];
	/** update columns of table "websliver.favicon" */
["websliver_favicon_update_column"]: GraphQLTypes["websliver_favicon_update_column"];
	["websliver_replace_tags_args"]: GraphQLTypes["websliver_replace_tags_args"];
	/** columns and relationships of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows"]: {
		affected_rows: GraphQLTypes["bigint"]
};
	/** aggregated selection of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_return_type_affected_rows_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** aggregate fields of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate_fields"]: {
		avg?: GraphQLTypes["websliver_return_type_affected_rows_avg_fields"] | undefined,
	count: number,
	max?: GraphQLTypes["websliver_return_type_affected_rows_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_return_type_affected_rows_min_fields"] | undefined,
	stddev?: GraphQLTypes["websliver_return_type_affected_rows_stddev_fields"] | undefined,
	stddev_pop?: GraphQLTypes["websliver_return_type_affected_rows_stddev_pop_fields"] | undefined,
	stddev_samp?: GraphQLTypes["websliver_return_type_affected_rows_stddev_samp_fields"] | undefined,
	sum?: GraphQLTypes["websliver_return_type_affected_rows_sum_fields"] | undefined,
	var_pop?: GraphQLTypes["websliver_return_type_affected_rows_var_pop_fields"] | undefined,
	var_samp?: GraphQLTypes["websliver_return_type_affected_rows_var_samp_fields"] | undefined,
	variance?: GraphQLTypes["websliver_return_type_affected_rows_variance_fields"] | undefined
};
	/** aggregate avg on columns */
["websliver_return_type_affected_rows_avg_fields"]: {
		affected_rows?: number | undefined
};
	/** Boolean expression to filter rows from the table "websliver.return_type_affected_rows". All fields are combined with a logical 'AND'. */
["websliver_return_type_affected_rows_bool_exp"]: GraphQLTypes["websliver_return_type_affected_rows_bool_exp"];
	/** unique or primary key constraints on table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_constraint"]: GraphQLTypes["websliver_return_type_affected_rows_constraint"];
	/** input type for incrementing numeric columns in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_inc_input"]: GraphQLTypes["websliver_return_type_affected_rows_inc_input"];
	/** input type for inserting data into table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_insert_input"]: GraphQLTypes["websliver_return_type_affected_rows_insert_input"];
	/** aggregate max on columns */
["websliver_return_type_affected_rows_max_fields"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** aggregate min on columns */
["websliver_return_type_affected_rows_min_fields"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** response of any mutation on the table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** on_conflict condition type for table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_on_conflict"]: GraphQLTypes["websliver_return_type_affected_rows_on_conflict"];
	/** Ordering options when selecting data from "websliver.return_type_affected_rows". */
["websliver_return_type_affected_rows_order_by"]: GraphQLTypes["websliver_return_type_affected_rows_order_by"];
	/** primary key columns input for table: websliver_return_type_affected_rows */
["websliver_return_type_affected_rows_pk_columns_input"]: GraphQLTypes["websliver_return_type_affected_rows_pk_columns_input"];
	/** select columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_select_column"]: GraphQLTypes["websliver_return_type_affected_rows_select_column"];
	/** input type for updating data in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_set_input"]: GraphQLTypes["websliver_return_type_affected_rows_set_input"];
	/** aggregate stddev on columns */
["websliver_return_type_affected_rows_stddev_fields"]: {
		affected_rows?: number | undefined
};
	/** aggregate stddev_pop on columns */
["websliver_return_type_affected_rows_stddev_pop_fields"]: {
		affected_rows?: number | undefined
};
	/** aggregate stddev_samp on columns */
["websliver_return_type_affected_rows_stddev_samp_fields"]: {
		affected_rows?: number | undefined
};
	/** aggregate sum on columns */
["websliver_return_type_affected_rows_sum_fields"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** update columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_update_column"]: GraphQLTypes["websliver_return_type_affected_rows_update_column"];
	/** aggregate var_pop on columns */
["websliver_return_type_affected_rows_var_pop_fields"]: {
		affected_rows?: number | undefined
};
	/** aggregate var_samp on columns */
["websliver_return_type_affected_rows_var_samp_fields"]: {
		affected_rows?: number | undefined
};
	/** aggregate variance on columns */
["websliver_return_type_affected_rows_variance_fields"]: {
		affected_rows?: number | undefined
};
	/** Only used as a return type for replace_tags and search_tags */
["websliver_return_type_tag"]: {
		name: string,
	uses: GraphQLTypes["bigint"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_return_type_tag_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_return_type_tag"]>
};
	/** aggregate fields of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_fields"]: {
		avg?: GraphQLTypes["websliver_return_type_tag_avg_fields"] | undefined,
	count: number,
	max?: GraphQLTypes["websliver_return_type_tag_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_return_type_tag_min_fields"] | undefined,
	stddev?: GraphQLTypes["websliver_return_type_tag_stddev_fields"] | undefined,
	stddev_pop?: GraphQLTypes["websliver_return_type_tag_stddev_pop_fields"] | undefined,
	stddev_samp?: GraphQLTypes["websliver_return_type_tag_stddev_samp_fields"] | undefined,
	sum?: GraphQLTypes["websliver_return_type_tag_sum_fields"] | undefined,
	var_pop?: GraphQLTypes["websliver_return_type_tag_var_pop_fields"] | undefined,
	var_samp?: GraphQLTypes["websliver_return_type_tag_var_samp_fields"] | undefined,
	variance?: GraphQLTypes["websliver_return_type_tag_variance_fields"] | undefined
};
	/** order by aggregate values of table "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_order_by"]: GraphQLTypes["websliver_return_type_tag_aggregate_order_by"];
	/** input type for inserting array relation for remote table "websliver.return_type_tag" */
["websliver_return_type_tag_arr_rel_insert_input"]: GraphQLTypes["websliver_return_type_tag_arr_rel_insert_input"];
	/** aggregate avg on columns */
["websliver_return_type_tag_avg_fields"]: {
		uses?: number | undefined
};
	/** order by avg() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_avg_order_by"]: GraphQLTypes["websliver_return_type_tag_avg_order_by"];
	/** Boolean expression to filter rows from the table "websliver.return_type_tag". All fields are combined with a logical 'AND'. */
["websliver_return_type_tag_bool_exp"]: GraphQLTypes["websliver_return_type_tag_bool_exp"];
	/** unique or primary key constraints on table "websliver.return_type_tag" */
["websliver_return_type_tag_constraint"]: GraphQLTypes["websliver_return_type_tag_constraint"];
	/** input type for incrementing numeric columns in table "websliver.return_type_tag" */
["websliver_return_type_tag_inc_input"]: GraphQLTypes["websliver_return_type_tag_inc_input"];
	/** input type for inserting data into table "websliver.return_type_tag" */
["websliver_return_type_tag_insert_input"]: GraphQLTypes["websliver_return_type_tag_insert_input"];
	/** aggregate max on columns */
["websliver_return_type_tag_max_fields"]: {
		name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_max_order_by"]: GraphQLTypes["websliver_return_type_tag_max_order_by"];
	/** aggregate min on columns */
["websliver_return_type_tag_min_fields"]: {
		name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_min_order_by"]: GraphQLTypes["websliver_return_type_tag_min_order_by"];
	/** response of any mutation on the table "websliver.return_type_tag" */
["websliver_return_type_tag_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_return_type_tag"]>
};
	/** on_conflict condition type for table "websliver.return_type_tag" */
["websliver_return_type_tag_on_conflict"]: GraphQLTypes["websliver_return_type_tag_on_conflict"];
	/** Ordering options when selecting data from "websliver.return_type_tag". */
["websliver_return_type_tag_order_by"]: GraphQLTypes["websliver_return_type_tag_order_by"];
	/** primary key columns input for table: websliver_return_type_tag */
["websliver_return_type_tag_pk_columns_input"]: GraphQLTypes["websliver_return_type_tag_pk_columns_input"];
	/** select columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_select_column"]: GraphQLTypes["websliver_return_type_tag_select_column"];
	/** input type for updating data in table "websliver.return_type_tag" */
["websliver_return_type_tag_set_input"]: GraphQLTypes["websliver_return_type_tag_set_input"];
	/** aggregate stddev on columns */
["websliver_return_type_tag_stddev_fields"]: {
		uses?: number | undefined
};
	/** order by stddev() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_order_by"]: GraphQLTypes["websliver_return_type_tag_stddev_order_by"];
	/** aggregate stddev_pop on columns */
["websliver_return_type_tag_stddev_pop_fields"]: {
		uses?: number | undefined
};
	/** order by stddev_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_pop_order_by"]: GraphQLTypes["websliver_return_type_tag_stddev_pop_order_by"];
	/** aggregate stddev_samp on columns */
["websliver_return_type_tag_stddev_samp_fields"]: {
		uses?: number | undefined
};
	/** order by stddev_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_samp_order_by"]: GraphQLTypes["websliver_return_type_tag_stddev_samp_order_by"];
	/** aggregate sum on columns */
["websliver_return_type_tag_sum_fields"]: {
		uses?: GraphQLTypes["bigint"] | undefined
};
	/** order by sum() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_sum_order_by"]: GraphQLTypes["websliver_return_type_tag_sum_order_by"];
	/** update columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_update_column"]: GraphQLTypes["websliver_return_type_tag_update_column"];
	/** aggregate var_pop on columns */
["websliver_return_type_tag_var_pop_fields"]: {
		uses?: number | undefined
};
	/** order by var_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_pop_order_by"]: GraphQLTypes["websliver_return_type_tag_var_pop_order_by"];
	/** aggregate var_samp on columns */
["websliver_return_type_tag_var_samp_fields"]: {
		uses?: number | undefined
};
	/** order by var_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_samp_order_by"]: GraphQLTypes["websliver_return_type_tag_var_samp_order_by"];
	/** aggregate variance on columns */
["websliver_return_type_tag_variance_fields"]: {
		uses?: number | undefined
};
	/** order by variance() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_variance_order_by"]: GraphQLTypes["websliver_return_type_tag_variance_order_by"];
	["websliver_upsert_entries_aff_rows_args"]: GraphQLTypes["websliver_upsert_entries_aff_rows_args"];
	["websliver_upsert_entries_args"]: GraphQLTypes["websliver_upsert_entries_args"];
	/** The owner of one or more devices and one or more workspaces */
["websliver_user"]: {
		created_at: GraphQLTypes["timestamptz"],
	/** managed by auth */
	email: string,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid: string,
	updated_at: GraphQLTypes["timestamptz"],
	/** managed by auth */
	username: string,
	/** An array relationship */
	workspaces: Array<GraphQLTypes["websliver_workspace"]>,
	/** An aggregate relationship */
	workspaces_aggregate: GraphQLTypes["websliver_workspace_aggregate"]
};
	/** aggregated selection of "websliver.user" */
["websliver_user_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_user_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_user"]>
};
	/** aggregate fields of "websliver.user" */
["websliver_user_aggregate_fields"]: {
		count: number,
	max?: GraphQLTypes["websliver_user_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_user_min_fields"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.user". All fields are combined with a logical 'AND'. */
["websliver_user_bool_exp"]: GraphQLTypes["websliver_user_bool_exp"];
	/** unique or primary key constraints on table "websliver.user" */
["websliver_user_constraint"]: GraphQLTypes["websliver_user_constraint"];
	/** input type for inserting data into table "websliver.user" */
["websliver_user_insert_input"]: GraphQLTypes["websliver_user_insert_input"];
	/** aggregate max on columns */
["websliver_user_max_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined
};
	/** aggregate min on columns */
["websliver_user_min_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined
};
	/** response of any mutation on the table "websliver.user" */
["websliver_user_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_user"]>
};
	/** input type for inserting object relation for remote table "websliver.user" */
["websliver_user_obj_rel_insert_input"]: GraphQLTypes["websliver_user_obj_rel_insert_input"];
	/** on_conflict condition type for table "websliver.user" */
["websliver_user_on_conflict"]: GraphQLTypes["websliver_user_on_conflict"];
	/** Ordering options when selecting data from "websliver.user". */
["websliver_user_order_by"]: GraphQLTypes["websliver_user_order_by"];
	/** primary key columns input for table: websliver_user */
["websliver_user_pk_columns_input"]: GraphQLTypes["websliver_user_pk_columns_input"];
	/** select columns of table "websliver.user" */
["websliver_user_select_column"]: GraphQLTypes["websliver_user_select_column"];
	/** input type for updating data in table "websliver.user" */
["websliver_user_set_input"]: GraphQLTypes["websliver_user_set_input"];
	/** update columns of table "websliver.user" */
["websliver_user_update_column"]: GraphQLTypes["websliver_user_update_column"];
	/** A historical visit of a location. 'source_id' represents the parent visit if any. */
["websliver_visit"]: {
		created_at: GraphQLTypes["timestamptz"],
	location: string,
	source_title: string,
	source_url: string,
	title: string,
	uid: GraphQLTypes["uuid"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.visit" */
["websliver_visit_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_visit_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_visit"]>
};
	/** aggregate fields of "websliver.visit" */
["websliver_visit_aggregate_fields"]: {
		count: number,
	max?: GraphQLTypes["websliver_visit_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_visit_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.visit" */
["websliver_visit_aggregate_order_by"]: GraphQLTypes["websliver_visit_aggregate_order_by"];
	/** input type for inserting array relation for remote table "websliver.visit" */
["websliver_visit_arr_rel_insert_input"]: GraphQLTypes["websliver_visit_arr_rel_insert_input"];
	/** Boolean expression to filter rows from the table "websliver.visit". All fields are combined with a logical 'AND'. */
["websliver_visit_bool_exp"]: GraphQLTypes["websliver_visit_bool_exp"];
	/** unique or primary key constraints on table "websliver.visit" */
["websliver_visit_constraint"]: GraphQLTypes["websliver_visit_constraint"];
	/** input type for inserting data into table "websliver.visit" */
["websliver_visit_insert_input"]: GraphQLTypes["websliver_visit_insert_input"];
	/** aggregate max on columns */
["websliver_visit_max_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.visit" */
["websliver_visit_max_order_by"]: GraphQLTypes["websliver_visit_max_order_by"];
	/** aggregate min on columns */
["websliver_visit_min_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.visit" */
["websliver_visit_min_order_by"]: GraphQLTypes["websliver_visit_min_order_by"];
	/** response of any mutation on the table "websliver.visit" */
["websliver_visit_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_visit"]>
};
	/** on_conflict condition type for table "websliver.visit" */
["websliver_visit_on_conflict"]: GraphQLTypes["websliver_visit_on_conflict"];
	/** Ordering options when selecting data from "websliver.visit". */
["websliver_visit_order_by"]: GraphQLTypes["websliver_visit_order_by"];
	/** primary key columns input for table: websliver_visit */
["websliver_visit_pk_columns_input"]: GraphQLTypes["websliver_visit_pk_columns_input"];
	/** select columns of table "websliver.visit" */
["websliver_visit_select_column"]: GraphQLTypes["websliver_visit_select_column"];
	/** input type for updating data in table "websliver.visit" */
["websliver_visit_set_input"]: GraphQLTypes["websliver_visit_set_input"];
	/** update columns of table "websliver.visit" */
["websliver_visit_update_column"]: GraphQLTypes["websliver_visit_update_column"];
	/** A workspace represents an isolated group of tags and bookmarks */
["websliver_workspace"]: {
		created_at: GraphQLTypes["timestamptz"],
	default: boolean,
	/** An array relationship */
	entries: Array<GraphQLTypes["websliver_entry"]>,
	/** An aggregate relationship */
	entries_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	name: string,
	/** An array relationship */
	return_type_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** An aggregate relationship */
	return_type_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"],
	/** An object relationship */
	user: GraphQLTypes["websliver_user"],
	user_id: string,
	/** An array relationship */
	visits: Array<GraphQLTypes["websliver_visit"]>,
	/** An aggregate relationship */
	visits_aggregate: GraphQLTypes["websliver_visit_aggregate"]
};
	/** aggregated selection of "websliver.workspace" */
["websliver_workspace_aggregate"]: {
		aggregate?: GraphQLTypes["websliver_workspace_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_workspace"]>
};
	/** aggregate fields of "websliver.workspace" */
["websliver_workspace_aggregate_fields"]: {
		count: number,
	max?: GraphQLTypes["websliver_workspace_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_workspace_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.workspace" */
["websliver_workspace_aggregate_order_by"]: GraphQLTypes["websliver_workspace_aggregate_order_by"];
	/** input type for inserting array relation for remote table "websliver.workspace" */
["websliver_workspace_arr_rel_insert_input"]: GraphQLTypes["websliver_workspace_arr_rel_insert_input"];
	/** Boolean expression to filter rows from the table "websliver.workspace". All fields are combined with a logical 'AND'. */
["websliver_workspace_bool_exp"]: GraphQLTypes["websliver_workspace_bool_exp"];
	/** unique or primary key constraints on table "websliver.workspace" */
["websliver_workspace_constraint"]: GraphQLTypes["websliver_workspace_constraint"];
	/** input type for inserting data into table "websliver.workspace" */
["websliver_workspace_insert_input"]: GraphQLTypes["websliver_workspace_insert_input"];
	/** aggregate max on columns */
["websliver_workspace_max_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user_id?: string | undefined
};
	/** order by max() on columns of table "websliver.workspace" */
["websliver_workspace_max_order_by"]: GraphQLTypes["websliver_workspace_max_order_by"];
	/** aggregate min on columns */
["websliver_workspace_min_fields"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user_id?: string | undefined
};
	/** order by min() on columns of table "websliver.workspace" */
["websliver_workspace_min_order_by"]: GraphQLTypes["websliver_workspace_min_order_by"];
	/** response of any mutation on the table "websliver.workspace" */
["websliver_workspace_mutation_response"]: {
		/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_workspace"]>
};
	/** input type for inserting object relation for remote table "websliver.workspace" */
["websliver_workspace_obj_rel_insert_input"]: GraphQLTypes["websliver_workspace_obj_rel_insert_input"];
	/** on_conflict condition type for table "websliver.workspace" */
["websliver_workspace_on_conflict"]: GraphQLTypes["websliver_workspace_on_conflict"];
	/** Ordering options when selecting data from "websliver.workspace". */
["websliver_workspace_order_by"]: GraphQLTypes["websliver_workspace_order_by"];
	/** primary key columns input for table: websliver_workspace */
["websliver_workspace_pk_columns_input"]: GraphQLTypes["websliver_workspace_pk_columns_input"];
	/** select columns of table "websliver.workspace" */
["websliver_workspace_select_column"]: GraphQLTypes["websliver_workspace_select_column"];
	/** input type for updating data in table "websliver.workspace" */
["websliver_workspace_set_input"]: GraphQLTypes["websliver_workspace_set_input"];
	/** update columns of table "websliver.workspace" */
["websliver_workspace_update_column"]: GraphQLTypes["websliver_workspace_update_column"]
    }

export type GraphQLTypes = {
    ["Boolean_cast_exp"]: {
		String?: GraphQLTypes["String_comparison_exp"] | undefined
};
	/** Boolean expression to compare columns of type "Boolean". All fields are combined with logical 'AND'. */
["Boolean_comparison_exp"]: {
		_cast?: GraphQLTypes["Boolean_cast_exp"] | undefined,
	_eq?: boolean | undefined,
	_gt?: boolean | undefined,
	_gte?: boolean | undefined,
	_in?: Array<boolean> | undefined,
	_is_null?: boolean | undefined,
	_lt?: boolean | undefined,
	_lte?: boolean | undefined,
	_neq?: boolean | undefined,
	_nin?: Array<boolean> | undefined
};
	/** Boolean expression to compare columns of type "String". All fields are combined with logical 'AND'. */
["String_comparison_exp"]: {
		_eq?: string | undefined,
	_gt?: string | undefined,
	_gte?: string | undefined,
	/** does the column match the given case-insensitive pattern */
	_ilike?: string | undefined,
	_in?: Array<string> | undefined,
	/** does the column match the given POSIX regular expression, case insensitive */
	_iregex?: string | undefined,
	_is_null?: boolean | undefined,
	/** does the column match the given pattern */
	_like?: string | undefined,
	_lt?: string | undefined,
	_lte?: string | undefined,
	_neq?: string | undefined,
	/** does the column NOT match the given case-insensitive pattern */
	_nilike?: string | undefined,
	_nin?: Array<string> | undefined,
	/** does the column NOT match the given POSIX regular expression, case insensitive */
	_niregex?: string | undefined,
	/** does the column NOT match the given pattern */
	_nlike?: string | undefined,
	/** does the column NOT match the given POSIX regular expression, case sensitive */
	_nregex?: string | undefined,
	/** does the column NOT match the given SQL regular expression */
	_nsimilar?: string | undefined,
	/** does the column match the given POSIX regular expression, case sensitive */
	_regex?: string | undefined,
	/** does the column match the given SQL regular expression */
	_similar?: string | undefined
};
	["bigint"]: any;
	["bigint_cast_exp"]: {
		String?: GraphQLTypes["String_comparison_exp"] | undefined
};
	/** Boolean expression to compare columns of type "bigint". All fields are combined with logical 'AND'. */
["bigint_comparison_exp"]: {
		_cast?: GraphQLTypes["bigint_cast_exp"] | undefined,
	_eq?: GraphQLTypes["bigint"] | undefined,
	_gt?: GraphQLTypes["bigint"] | undefined,
	_gte?: GraphQLTypes["bigint"] | undefined,
	_in?: Array<GraphQLTypes["bigint"]> | undefined,
	_is_null?: boolean | undefined,
	_lt?: GraphQLTypes["bigint"] | undefined,
	_lte?: GraphQLTypes["bigint"] | undefined,
	_neq?: GraphQLTypes["bigint"] | undefined,
	_nin?: Array<GraphQLTypes["bigint"]> | undefined
};
	["jsonb"]: any;
	["jsonb_cast_exp"]: {
		String?: GraphQLTypes["String_comparison_exp"] | undefined
};
	/** Boolean expression to compare columns of type "jsonb". All fields are combined with logical 'AND'. */
["jsonb_comparison_exp"]: {
		_cast?: GraphQLTypes["jsonb_cast_exp"] | undefined,
	/** is the column contained in the given json value */
	_contained_in?: GraphQLTypes["jsonb"] | undefined,
	/** does the column contain the given json value at the top level */
	_contains?: GraphQLTypes["jsonb"] | undefined,
	_eq?: GraphQLTypes["jsonb"] | undefined,
	_gt?: GraphQLTypes["jsonb"] | undefined,
	_gte?: GraphQLTypes["jsonb"] | undefined,
	/** does the string exist as a top-level key in the column */
	_has_key?: string | undefined,
	/** do all of these strings exist as top-level keys in the column */
	_has_keys_all?: Array<string> | undefined,
	/** do any of these strings exist as top-level keys in the column */
	_has_keys_any?: Array<string> | undefined,
	_in?: Array<GraphQLTypes["jsonb"]> | undefined,
	_is_null?: boolean | undefined,
	_lt?: GraphQLTypes["jsonb"] | undefined,
	_lte?: GraphQLTypes["jsonb"] | undefined,
	_neq?: GraphQLTypes["jsonb"] | undefined,
	_nin?: Array<GraphQLTypes["jsonb"]> | undefined
};
	/** mutation root */
["mutation_root"]: {
	__typename: "mutation_root",
	/** delete data from the table: "websliver.entry" */
	delete_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.entry" */
	delete_websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** delete data from the table: "websliver.favicon" */
	delete_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.favicon" */
	delete_websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** delete data from the table: "websliver.return_type_affected_rows" */
	delete_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.return_type_affected_rows" */
	delete_websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** delete data from the table: "websliver.return_type_tag" */
	delete_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.return_type_tag" */
	delete_websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** delete data from the table: "websliver.user" */
	delete_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.user" */
	delete_websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** delete data from the table: "websliver.visit" */
	delete_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.visit" */
	delete_websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** delete data from the table: "websliver.workspace" */
	delete_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** delete single row from the table: "websliver.workspace" */
	delete_websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined,
	/** insert data into the table: "websliver.entry" */
	insert_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.entry" */
	insert_websliver_entry_one?: GraphQLTypes["websliver_entry"] | undefined,
	/** insert data into the table: "websliver.favicon" */
	insert_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.favicon" */
	insert_websliver_favicon_one?: GraphQLTypes["websliver_favicon"] | undefined,
	/** insert data into the table: "websliver.return_type_affected_rows" */
	insert_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.return_type_affected_rows" */
	insert_websliver_return_type_affected_rows_one?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** insert data into the table: "websliver.return_type_tag" */
	insert_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.return_type_tag" */
	insert_websliver_return_type_tag_one?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** insert data into the table: "websliver.user" */
	insert_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.user" */
	insert_websliver_user_one?: GraphQLTypes["websliver_user"] | undefined,
	/** insert data into the table: "websliver.visit" */
	insert_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.visit" */
	insert_websliver_visit_one?: GraphQLTypes["websliver_visit"] | undefined,
	/** insert data into the table: "websliver.workspace" */
	insert_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** insert a single row into the table: "websliver.workspace" */
	insert_websliver_workspace_one?: GraphQLTypes["websliver_workspace"] | undefined,
	/** update data of the table: "websliver.entry" */
	update_websliver_entry?: GraphQLTypes["websliver_entry_mutation_response"] | undefined,
	/** update single row of the table: "websliver.entry" */
	update_websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** update data of the table: "websliver.favicon" */
	update_websliver_favicon?: GraphQLTypes["websliver_favicon_mutation_response"] | undefined,
	/** update single row of the table: "websliver.favicon" */
	update_websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** update data of the table: "websliver.return_type_affected_rows" */
	update_websliver_return_type_affected_rows?: GraphQLTypes["websliver_return_type_affected_rows_mutation_response"] | undefined,
	/** update single row of the table: "websliver.return_type_affected_rows" */
	update_websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** update data of the table: "websliver.return_type_tag" */
	update_websliver_return_type_tag?: GraphQLTypes["websliver_return_type_tag_mutation_response"] | undefined,
	/** update single row of the table: "websliver.return_type_tag" */
	update_websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** update data of the table: "websliver.user" */
	update_websliver_user?: GraphQLTypes["websliver_user_mutation_response"] | undefined,
	/** update single row of the table: "websliver.user" */
	update_websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** update data of the table: "websliver.visit" */
	update_websliver_visit?: GraphQLTypes["websliver_visit_mutation_response"] | undefined,
	/** update single row of the table: "websliver.visit" */
	update_websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** update data of the table: "websliver.workspace" */
	update_websliver_workspace?: GraphQLTypes["websliver_workspace_mutation_response"] | undefined,
	/** update single row of the table: "websliver.workspace" */
	update_websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined,
	/** execute VOLATILE function "websliver.copy_entries" which returns "websliver.entry" */
	websliver_copy_entries: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.copy_entries_aff_rows" which returns "websliver.return_type_affected_rows" */
	websliver_copy_entries_aff_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** execute VOLATILE function "websliver.replace_tags" which returns "websliver.entry" */
	websliver_replace_tags: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.upsert_entries" which returns "websliver.entry" */
	websliver_upsert_entries: Array<GraphQLTypes["websliver_entry"]>,
	/** execute VOLATILE function "websliver.upsert_entries_aff_rows" which returns "websliver.return_type_affected_rows" */
	websliver_upsert_entries_aff_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** column ordering options */
["order_by"]: order_by;
	["query_root"]: {
	__typename: "query_root",
	/** fetch data from the table: "websliver.entry" */
	websliver_entry: Array<GraphQLTypes["websliver_entry"]>,
	/** fetch aggregated fields from the table: "websliver.entry" */
	websliver_entry_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	/** fetch data from the table: "websliver.entry" using primary key columns */
	websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** fetch data from the table: "websliver.favicon" */
	websliver_favicon: Array<GraphQLTypes["websliver_favicon"]>,
	/** fetch aggregated fields from the table: "websliver.favicon" */
	websliver_favicon_aggregate: GraphQLTypes["websliver_favicon_aggregate"],
	/** fetch data from the table: "websliver.favicon" using primary key columns */
	websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** fetch data from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows_aggregate: GraphQLTypes["websliver_return_type_affected_rows_aggregate"],
	/** fetch data from the table: "websliver.return_type_affected_rows" using primary key columns */
	websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** fetch data from the table: "websliver.return_type_tag" */
	websliver_return_type_tag: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_tag" */
	websliver_return_type_tag_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.return_type_tag" using primary key columns */
	websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** execute function "websliver.search_tags" which returns "websliver.return_type_tag" */
	websliver_search_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** execute function "websliver.search_tags" and query aggregates on result of table type "websliver.return_type_tag" */
	websliver_search_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.user" */
	websliver_user: Array<GraphQLTypes["websliver_user"]>,
	/** fetch aggregated fields from the table: "websliver.user" */
	websliver_user_aggregate: GraphQLTypes["websliver_user_aggregate"],
	/** fetch data from the table: "websliver.user" using primary key columns */
	websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** fetch data from the table: "websliver.visit" */
	websliver_visit: Array<GraphQLTypes["websliver_visit"]>,
	/** fetch aggregated fields from the table: "websliver.visit" */
	websliver_visit_aggregate: GraphQLTypes["websliver_visit_aggregate"],
	/** fetch data from the table: "websliver.visit" using primary key columns */
	websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** fetch data from the table: "websliver.workspace" */
	websliver_workspace: Array<GraphQLTypes["websliver_workspace"]>,
	/** fetch aggregated fields from the table: "websliver.workspace" */
	websliver_workspace_aggregate: GraphQLTypes["websliver_workspace_aggregate"],
	/** fetch data from the table: "websliver.workspace" using primary key columns */
	websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined
};
	["subscription_root"]: {
	__typename: "subscription_root",
	/** fetch data from the table: "websliver.entry" */
	websliver_entry: Array<GraphQLTypes["websliver_entry"]>,
	/** fetch aggregated fields from the table: "websliver.entry" */
	websliver_entry_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	/** fetch data from the table: "websliver.entry" using primary key columns */
	websliver_entry_by_pk?: GraphQLTypes["websliver_entry"] | undefined,
	/** fetch data from the table: "websliver.favicon" */
	websliver_favicon: Array<GraphQLTypes["websliver_favicon"]>,
	/** fetch aggregated fields from the table: "websliver.favicon" */
	websliver_favicon_aggregate: GraphQLTypes["websliver_favicon_aggregate"],
	/** fetch data from the table: "websliver.favicon" using primary key columns */
	websliver_favicon_by_pk?: GraphQLTypes["websliver_favicon"] | undefined,
	/** fetch data from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows: Array<GraphQLTypes["websliver_return_type_affected_rows"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_affected_rows" */
	websliver_return_type_affected_rows_aggregate: GraphQLTypes["websliver_return_type_affected_rows_aggregate"],
	/** fetch data from the table: "websliver.return_type_affected_rows" using primary key columns */
	websliver_return_type_affected_rows_by_pk?: GraphQLTypes["websliver_return_type_affected_rows"] | undefined,
	/** fetch data from the table: "websliver.return_type_tag" */
	websliver_return_type_tag: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** fetch aggregated fields from the table: "websliver.return_type_tag" */
	websliver_return_type_tag_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.return_type_tag" using primary key columns */
	websliver_return_type_tag_by_pk?: GraphQLTypes["websliver_return_type_tag"] | undefined,
	/** execute function "websliver.search_tags" which returns "websliver.return_type_tag" */
	websliver_search_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** execute function "websliver.search_tags" and query aggregates on result of table type "websliver.return_type_tag" */
	websliver_search_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	/** fetch data from the table: "websliver.user" */
	websliver_user: Array<GraphQLTypes["websliver_user"]>,
	/** fetch aggregated fields from the table: "websliver.user" */
	websliver_user_aggregate: GraphQLTypes["websliver_user_aggregate"],
	/** fetch data from the table: "websliver.user" using primary key columns */
	websliver_user_by_pk?: GraphQLTypes["websliver_user"] | undefined,
	/** fetch data from the table: "websliver.visit" */
	websliver_visit: Array<GraphQLTypes["websliver_visit"]>,
	/** fetch aggregated fields from the table: "websliver.visit" */
	websliver_visit_aggregate: GraphQLTypes["websliver_visit_aggregate"],
	/** fetch data from the table: "websliver.visit" using primary key columns */
	websliver_visit_by_pk?: GraphQLTypes["websliver_visit"] | undefined,
	/** fetch data from the table: "websliver.workspace" */
	websliver_workspace: Array<GraphQLTypes["websliver_workspace"]>,
	/** fetch aggregated fields from the table: "websliver.workspace" */
	websliver_workspace_aggregate: GraphQLTypes["websliver_workspace_aggregate"],
	/** fetch data from the table: "websliver.workspace" using primary key columns */
	websliver_workspace_by_pk?: GraphQLTypes["websliver_workspace"] | undefined
};
	["timestamptz"]: any;
	["timestamptz_cast_exp"]: {
		String?: GraphQLTypes["String_comparison_exp"] | undefined
};
	/** Boolean expression to compare columns of type "timestamptz". All fields are combined with logical 'AND'. */
["timestamptz_comparison_exp"]: {
		_cast?: GraphQLTypes["timestamptz_cast_exp"] | undefined,
	_eq?: GraphQLTypes["timestamptz"] | undefined,
	_gt?: GraphQLTypes["timestamptz"] | undefined,
	_gte?: GraphQLTypes["timestamptz"] | undefined,
	_in?: Array<GraphQLTypes["timestamptz"]> | undefined,
	_is_null?: boolean | undefined,
	_lt?: GraphQLTypes["timestamptz"] | undefined,
	_lte?: GraphQLTypes["timestamptz"] | undefined,
	_neq?: GraphQLTypes["timestamptz"] | undefined,
	_nin?: Array<GraphQLTypes["timestamptz"]> | undefined
};
	["uuid"]: any;
	["uuid_cast_exp"]: {
		String?: GraphQLTypes["String_comparison_exp"] | undefined
};
	/** Boolean expression to compare columns of type "uuid". All fields are combined with logical 'AND'. */
["uuid_comparison_exp"]: {
		_cast?: GraphQLTypes["uuid_cast_exp"] | undefined,
	_eq?: GraphQLTypes["uuid"] | undefined,
	_gt?: GraphQLTypes["uuid"] | undefined,
	_gte?: GraphQLTypes["uuid"] | undefined,
	_in?: Array<GraphQLTypes["uuid"]> | undefined,
	_is_null?: boolean | undefined,
	_lt?: GraphQLTypes["uuid"] | undefined,
	_lte?: GraphQLTypes["uuid"] | undefined,
	_neq?: GraphQLTypes["uuid"] | undefined,
	_nin?: Array<GraphQLTypes["uuid"]> | undefined
};
	["websliver_copy_entries_aff_rows_args"]: {
		destination_workspace_id?: GraphQLTypes["uuid"] | undefined,
	search_exclusions?: GraphQLTypes["jsonb"] | undefined,
	search_tags?: GraphQLTypes["jsonb"] | undefined,
	search_term?: string | undefined,
	source_workspace_id?: GraphQLTypes["uuid"] | undefined
};
	["websliver_copy_entries_args"]: {
		destination_workspace_id?: GraphQLTypes["uuid"] | undefined,
	search_exclusions?: GraphQLTypes["jsonb"] | undefined,
	search_tags?: GraphQLTypes["jsonb"] | undefined,
	search_term?: string | undefined,
	source_workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** An entry represents a saved location meta object. */
["websliver_entry"]: {
	__typename: "websliver_entry",
	created_at: GraphQLTypes["timestamptz"],
	description: string,
	/** An object relationship */
	favicon?: GraphQLTypes["websliver_favicon"] | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location: string,
	tags: GraphQLTypes["jsonb"],
	title: string,
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.entry" */
["websliver_entry_aggregate"]: {
	__typename: "websliver_entry_aggregate",
	aggregate?: GraphQLTypes["websliver_entry_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_entry"]>
};
	/** aggregate fields of "websliver.entry" */
["websliver_entry_aggregate_fields"]: {
	__typename: "websliver_entry_aggregate_fields",
	count: number,
	max?: GraphQLTypes["websliver_entry_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_entry_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.entry" */
["websliver_entry_aggregate_order_by"]: {
		count?: GraphQLTypes["order_by"] | undefined,
	max?: GraphQLTypes["websliver_entry_max_order_by"] | undefined,
	min?: GraphQLTypes["websliver_entry_min_order_by"] | undefined
};
	/** append existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_append_input"]: {
		tags?: GraphQLTypes["jsonb"] | undefined
};
	/** input type for inserting array relation for remote table "websliver.entry" */
["websliver_entry_arr_rel_insert_input"]: {
		data: Array<GraphQLTypes["websliver_entry_insert_input"]>,
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_entry_on_conflict"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.entry". All fields are combined with a logical 'AND'. */
["websliver_entry_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_entry_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_entry_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_entry_bool_exp"]> | undefined,
	created_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	description?: GraphQLTypes["String_comparison_exp"] | undefined,
	favicon?: GraphQLTypes["websliver_favicon_bool_exp"] | undefined,
	favicon_id?: GraphQLTypes["uuid_comparison_exp"] | undefined,
	location?: GraphQLTypes["String_comparison_exp"] | undefined,
	tags?: GraphQLTypes["jsonb_comparison_exp"] | undefined,
	title?: GraphQLTypes["String_comparison_exp"] | undefined,
	uid?: GraphQLTypes["uuid_comparison_exp"] | undefined,
	updated_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined,
	workspace_id?: GraphQLTypes["uuid_comparison_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.entry" */
["websliver_entry_constraint"]: websliver_entry_constraint;
	/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
["websliver_entry_delete_at_path_input"]: {
		tags?: Array<string> | undefined
};
	/** delete the array element with specified index (negative integers count from the end). throws an error if top level container is not an array */
["websliver_entry_delete_elem_input"]: {
		tags?: number | undefined
};
	/** delete key/value pair or string element. key/value pairs are matched based on their key value */
["websliver_entry_delete_key_input"]: {
		tags?: string | undefined
};
	/** input type for inserting data into table "websliver.entry" */
["websliver_entry_insert_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon?: GraphQLTypes["websliver_favicon_obj_rel_insert_input"] | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	tags?: GraphQLTypes["jsonb"] | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_obj_rel_insert_input"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** aggregate max on columns */
["websliver_entry_max_fields"]: {
	__typename: "websliver_entry_max_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.entry" */
["websliver_entry_max_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	description?: GraphQLTypes["order_by"] | undefined,
	favicon_id?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate min on columns */
["websliver_entry_min_fields"]: {
	__typename: "websliver_entry_min_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.entry" */
["websliver_entry_min_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	description?: GraphQLTypes["order_by"] | undefined,
	favicon_id?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** response of any mutation on the table "websliver.entry" */
["websliver_entry_mutation_response"]: {
	__typename: "websliver_entry_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_entry"]>
};
	/** on_conflict condition type for table "websliver.entry" */
["websliver_entry_on_conflict"]: {
		constraint: GraphQLTypes["websliver_entry_constraint"],
	update_columns: Array<GraphQLTypes["websliver_entry_update_column"]>,
	where?: GraphQLTypes["websliver_entry_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.entry". */
["websliver_entry_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	description?: GraphQLTypes["order_by"] | undefined,
	favicon?: GraphQLTypes["websliver_favicon_order_by"] | undefined,
	favicon_id?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	tags?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** primary key columns input for table: websliver_entry */
["websliver_entry_pk_columns_input"]: {
		uid: GraphQLTypes["uuid"]
};
	/** prepend existing jsonb value of filtered columns with new jsonb value */
["websliver_entry_prepend_input"]: {
		tags?: GraphQLTypes["jsonb"] | undefined
};
	/** select columns of table "websliver.entry" */
["websliver_entry_select_column"]: websliver_entry_select_column;
	/** input type for updating data in table "websliver.entry" */
["websliver_entry_set_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	description?: string | undefined,
	favicon_id?: GraphQLTypes["uuid"] | undefined,
	location?: string | undefined,
	tags?: GraphQLTypes["jsonb"] | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** update columns of table "websliver.entry" */
["websliver_entry_update_column"]: websliver_entry_update_column;
	/** The favicon for a location. Reason for hash is that some favicon data is longer than what PG allows for unique constraints */
["websliver_favicon"]: {
	__typename: "websliver_favicon",
	created_at: GraphQLTypes["timestamptz"],
	data: string,
	/** An array relationship */
	entries: Array<GraphQLTypes["websliver_entry"]>,
	/** An aggregate relationship */
	entries_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	hash: string,
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"]
};
	/** aggregated selection of "websliver.favicon" */
["websliver_favicon_aggregate"]: {
	__typename: "websliver_favicon_aggregate",
	aggregate?: GraphQLTypes["websliver_favicon_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_favicon"]>
};
	/** aggregate fields of "websliver.favicon" */
["websliver_favicon_aggregate_fields"]: {
	__typename: "websliver_favicon_aggregate_fields",
	count: number,
	max?: GraphQLTypes["websliver_favicon_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_favicon_min_fields"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.favicon". All fields are combined with a logical 'AND'. */
["websliver_favicon_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_favicon_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_favicon_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_favicon_bool_exp"]> | undefined,
	created_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	data?: GraphQLTypes["String_comparison_exp"] | undefined,
	entries?: GraphQLTypes["websliver_entry_bool_exp"] | undefined,
	hash?: GraphQLTypes["String_comparison_exp"] | undefined,
	uid?: GraphQLTypes["uuid_comparison_exp"] | undefined,
	updated_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.favicon" */
["websliver_favicon_constraint"]: websliver_favicon_constraint;
	/** input type for inserting data into table "websliver.favicon" */
["websliver_favicon_insert_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	entries?: GraphQLTypes["websliver_entry_arr_rel_insert_input"] | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** aggregate max on columns */
["websliver_favicon_max_fields"]: {
	__typename: "websliver_favicon_max_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** aggregate min on columns */
["websliver_favicon_min_fields"]: {
	__typename: "websliver_favicon_min_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** response of any mutation on the table "websliver.favicon" */
["websliver_favicon_mutation_response"]: {
	__typename: "websliver_favicon_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_favicon"]>
};
	/** input type for inserting object relation for remote table "websliver.favicon" */
["websliver_favicon_obj_rel_insert_input"]: {
		data: GraphQLTypes["websliver_favicon_insert_input"],
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_favicon_on_conflict"] | undefined
};
	/** on_conflict condition type for table "websliver.favicon" */
["websliver_favicon_on_conflict"]: {
		constraint: GraphQLTypes["websliver_favicon_constraint"],
	update_columns: Array<GraphQLTypes["websliver_favicon_update_column"]>,
	where?: GraphQLTypes["websliver_favicon_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.favicon". */
["websliver_favicon_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	data?: GraphQLTypes["order_by"] | undefined,
	entries_aggregate?: GraphQLTypes["websliver_entry_aggregate_order_by"] | undefined,
	hash?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined
};
	/** primary key columns input for table: websliver_favicon */
["websliver_favicon_pk_columns_input"]: {
		uid: GraphQLTypes["uuid"]
};
	/** select columns of table "websliver.favicon" */
["websliver_favicon_select_column"]: websliver_favicon_select_column;
	/** input type for updating data in table "websliver.favicon" */
["websliver_favicon_set_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	data?: string | undefined,
	hash?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined
};
	/** update columns of table "websliver.favicon" */
["websliver_favicon_update_column"]: websliver_favicon_update_column;
	["websliver_replace_tags_args"]: {
		new_tags?: GraphQLTypes["jsonb"] | undefined,
	old_tags?: GraphQLTypes["jsonb"] | undefined,
	workspace?: GraphQLTypes["uuid"] | undefined
};
	/** columns and relationships of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows"]: {
	__typename: "websliver_return_type_affected_rows",
	affected_rows: GraphQLTypes["bigint"]
};
	/** aggregated selection of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate"]: {
	__typename: "websliver_return_type_affected_rows_aggregate",
	aggregate?: GraphQLTypes["websliver_return_type_affected_rows_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** aggregate fields of "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_aggregate_fields"]: {
	__typename: "websliver_return_type_affected_rows_aggregate_fields",
	avg?: GraphQLTypes["websliver_return_type_affected_rows_avg_fields"] | undefined,
	count: number,
	max?: GraphQLTypes["websliver_return_type_affected_rows_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_return_type_affected_rows_min_fields"] | undefined,
	stddev?: GraphQLTypes["websliver_return_type_affected_rows_stddev_fields"] | undefined,
	stddev_pop?: GraphQLTypes["websliver_return_type_affected_rows_stddev_pop_fields"] | undefined,
	stddev_samp?: GraphQLTypes["websliver_return_type_affected_rows_stddev_samp_fields"] | undefined,
	sum?: GraphQLTypes["websliver_return_type_affected_rows_sum_fields"] | undefined,
	var_pop?: GraphQLTypes["websliver_return_type_affected_rows_var_pop_fields"] | undefined,
	var_samp?: GraphQLTypes["websliver_return_type_affected_rows_var_samp_fields"] | undefined,
	variance?: GraphQLTypes["websliver_return_type_affected_rows_variance_fields"] | undefined
};
	/** aggregate avg on columns */
["websliver_return_type_affected_rows_avg_fields"]: {
	__typename: "websliver_return_type_affected_rows_avg_fields",
	affected_rows?: number | undefined
};
	/** Boolean expression to filter rows from the table "websliver.return_type_affected_rows". All fields are combined with a logical 'AND'. */
["websliver_return_type_affected_rows_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_return_type_affected_rows_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_return_type_affected_rows_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_return_type_affected_rows_bool_exp"]> | undefined,
	affected_rows?: GraphQLTypes["bigint_comparison_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_constraint"]: websliver_return_type_affected_rows_constraint;
	/** input type for incrementing numeric columns in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_inc_input"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** input type for inserting data into table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_insert_input"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** aggregate max on columns */
["websliver_return_type_affected_rows_max_fields"]: {
	__typename: "websliver_return_type_affected_rows_max_fields",
	affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** aggregate min on columns */
["websliver_return_type_affected_rows_min_fields"]: {
	__typename: "websliver_return_type_affected_rows_min_fields",
	affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** response of any mutation on the table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_mutation_response"]: {
	__typename: "websliver_return_type_affected_rows_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_return_type_affected_rows"]>
};
	/** on_conflict condition type for table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_on_conflict"]: {
		constraint: GraphQLTypes["websliver_return_type_affected_rows_constraint"],
	update_columns: Array<GraphQLTypes["websliver_return_type_affected_rows_update_column"]>,
	where?: GraphQLTypes["websliver_return_type_affected_rows_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.return_type_affected_rows". */
["websliver_return_type_affected_rows_order_by"]: {
		affected_rows?: GraphQLTypes["order_by"] | undefined
};
	/** primary key columns input for table: websliver_return_type_affected_rows */
["websliver_return_type_affected_rows_pk_columns_input"]: {
		affected_rows: GraphQLTypes["bigint"]
};
	/** select columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_select_column"]: websliver_return_type_affected_rows_select_column;
	/** input type for updating data in table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_set_input"]: {
		affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** aggregate stddev on columns */
["websliver_return_type_affected_rows_stddev_fields"]: {
	__typename: "websliver_return_type_affected_rows_stddev_fields",
	affected_rows?: number | undefined
};
	/** aggregate stddev_pop on columns */
["websliver_return_type_affected_rows_stddev_pop_fields"]: {
	__typename: "websliver_return_type_affected_rows_stddev_pop_fields",
	affected_rows?: number | undefined
};
	/** aggregate stddev_samp on columns */
["websliver_return_type_affected_rows_stddev_samp_fields"]: {
	__typename: "websliver_return_type_affected_rows_stddev_samp_fields",
	affected_rows?: number | undefined
};
	/** aggregate sum on columns */
["websliver_return_type_affected_rows_sum_fields"]: {
	__typename: "websliver_return_type_affected_rows_sum_fields",
	affected_rows?: GraphQLTypes["bigint"] | undefined
};
	/** update columns of table "websliver.return_type_affected_rows" */
["websliver_return_type_affected_rows_update_column"]: websliver_return_type_affected_rows_update_column;
	/** aggregate var_pop on columns */
["websliver_return_type_affected_rows_var_pop_fields"]: {
	__typename: "websliver_return_type_affected_rows_var_pop_fields",
	affected_rows?: number | undefined
};
	/** aggregate var_samp on columns */
["websliver_return_type_affected_rows_var_samp_fields"]: {
	__typename: "websliver_return_type_affected_rows_var_samp_fields",
	affected_rows?: number | undefined
};
	/** aggregate variance on columns */
["websliver_return_type_affected_rows_variance_fields"]: {
	__typename: "websliver_return_type_affected_rows_variance_fields",
	affected_rows?: number | undefined
};
	/** Only used as a return type for replace_tags and search_tags */
["websliver_return_type_tag"]: {
	__typename: "websliver_return_type_tag",
	name: string,
	uses: GraphQLTypes["bigint"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate"]: {
	__typename: "websliver_return_type_tag_aggregate",
	aggregate?: GraphQLTypes["websliver_return_type_tag_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_return_type_tag"]>
};
	/** aggregate fields of "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_fields"]: {
	__typename: "websliver_return_type_tag_aggregate_fields",
	avg?: GraphQLTypes["websliver_return_type_tag_avg_fields"] | undefined,
	count: number,
	max?: GraphQLTypes["websliver_return_type_tag_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_return_type_tag_min_fields"] | undefined,
	stddev?: GraphQLTypes["websliver_return_type_tag_stddev_fields"] | undefined,
	stddev_pop?: GraphQLTypes["websliver_return_type_tag_stddev_pop_fields"] | undefined,
	stddev_samp?: GraphQLTypes["websliver_return_type_tag_stddev_samp_fields"] | undefined,
	sum?: GraphQLTypes["websliver_return_type_tag_sum_fields"] | undefined,
	var_pop?: GraphQLTypes["websliver_return_type_tag_var_pop_fields"] | undefined,
	var_samp?: GraphQLTypes["websliver_return_type_tag_var_samp_fields"] | undefined,
	variance?: GraphQLTypes["websliver_return_type_tag_variance_fields"] | undefined
};
	/** order by aggregate values of table "websliver.return_type_tag" */
["websliver_return_type_tag_aggregate_order_by"]: {
		avg?: GraphQLTypes["websliver_return_type_tag_avg_order_by"] | undefined,
	count?: GraphQLTypes["order_by"] | undefined,
	max?: GraphQLTypes["websliver_return_type_tag_max_order_by"] | undefined,
	min?: GraphQLTypes["websliver_return_type_tag_min_order_by"] | undefined,
	stddev?: GraphQLTypes["websliver_return_type_tag_stddev_order_by"] | undefined,
	stddev_pop?: GraphQLTypes["websliver_return_type_tag_stddev_pop_order_by"] | undefined,
	stddev_samp?: GraphQLTypes["websliver_return_type_tag_stddev_samp_order_by"] | undefined,
	sum?: GraphQLTypes["websliver_return_type_tag_sum_order_by"] | undefined,
	var_pop?: GraphQLTypes["websliver_return_type_tag_var_pop_order_by"] | undefined,
	var_samp?: GraphQLTypes["websliver_return_type_tag_var_samp_order_by"] | undefined,
	variance?: GraphQLTypes["websliver_return_type_tag_variance_order_by"] | undefined
};
	/** input type for inserting array relation for remote table "websliver.return_type_tag" */
["websliver_return_type_tag_arr_rel_insert_input"]: {
		data: Array<GraphQLTypes["websliver_return_type_tag_insert_input"]>,
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_return_type_tag_on_conflict"] | undefined
};
	/** aggregate avg on columns */
["websliver_return_type_tag_avg_fields"]: {
	__typename: "websliver_return_type_tag_avg_fields",
	uses?: number | undefined
};
	/** order by avg() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_avg_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.return_type_tag". All fields are combined with a logical 'AND'. */
["websliver_return_type_tag_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_return_type_tag_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_return_type_tag_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_return_type_tag_bool_exp"]> | undefined,
	name?: GraphQLTypes["String_comparison_exp"] | undefined,
	uses?: GraphQLTypes["bigint_comparison_exp"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined,
	workspace_id?: GraphQLTypes["uuid_comparison_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.return_type_tag" */
["websliver_return_type_tag_constraint"]: websliver_return_type_tag_constraint;
	/** input type for incrementing numeric columns in table "websliver.return_type_tag" */
["websliver_return_type_tag_inc_input"]: {
		uses?: GraphQLTypes["bigint"] | undefined
};
	/** input type for inserting data into table "websliver.return_type_tag" */
["websliver_return_type_tag_insert_input"]: {
		name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_obj_rel_insert_input"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** aggregate max on columns */
["websliver_return_type_tag_max_fields"]: {
	__typename: "websliver_return_type_tag_max_fields",
	name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_max_order_by"]: {
		name?: GraphQLTypes["order_by"] | undefined,
	uses?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate min on columns */
["websliver_return_type_tag_min_fields"]: {
	__typename: "websliver_return_type_tag_min_fields",
	name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_min_order_by"]: {
		name?: GraphQLTypes["order_by"] | undefined,
	uses?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** response of any mutation on the table "websliver.return_type_tag" */
["websliver_return_type_tag_mutation_response"]: {
	__typename: "websliver_return_type_tag_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_return_type_tag"]>
};
	/** on_conflict condition type for table "websliver.return_type_tag" */
["websliver_return_type_tag_on_conflict"]: {
		constraint: GraphQLTypes["websliver_return_type_tag_constraint"],
	update_columns: Array<GraphQLTypes["websliver_return_type_tag_update_column"]>,
	where?: GraphQLTypes["websliver_return_type_tag_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.return_type_tag". */
["websliver_return_type_tag_order_by"]: {
		name?: GraphQLTypes["order_by"] | undefined,
	uses?: GraphQLTypes["order_by"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** primary key columns input for table: websliver_return_type_tag */
["websliver_return_type_tag_pk_columns_input"]: {
		name: string
};
	/** select columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_select_column"]: websliver_return_type_tag_select_column;
	/** input type for updating data in table "websliver.return_type_tag" */
["websliver_return_type_tag_set_input"]: {
		name?: string | undefined,
	uses?: GraphQLTypes["bigint"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** aggregate stddev on columns */
["websliver_return_type_tag_stddev_fields"]: {
	__typename: "websliver_return_type_tag_stddev_fields",
	uses?: number | undefined
};
	/** order by stddev() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate stddev_pop on columns */
["websliver_return_type_tag_stddev_pop_fields"]: {
	__typename: "websliver_return_type_tag_stddev_pop_fields",
	uses?: number | undefined
};
	/** order by stddev_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_pop_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate stddev_samp on columns */
["websliver_return_type_tag_stddev_samp_fields"]: {
	__typename: "websliver_return_type_tag_stddev_samp_fields",
	uses?: number | undefined
};
	/** order by stddev_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_stddev_samp_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate sum on columns */
["websliver_return_type_tag_sum_fields"]: {
	__typename: "websliver_return_type_tag_sum_fields",
	uses?: GraphQLTypes["bigint"] | undefined
};
	/** order by sum() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_sum_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** update columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_update_column"]: websliver_return_type_tag_update_column;
	/** aggregate var_pop on columns */
["websliver_return_type_tag_var_pop_fields"]: {
	__typename: "websliver_return_type_tag_var_pop_fields",
	uses?: number | undefined
};
	/** order by var_pop() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_pop_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate var_samp on columns */
["websliver_return_type_tag_var_samp_fields"]: {
	__typename: "websliver_return_type_tag_var_samp_fields",
	uses?: number | undefined
};
	/** order by var_samp() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_var_samp_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate variance on columns */
["websliver_return_type_tag_variance_fields"]: {
	__typename: "websliver_return_type_tag_variance_fields",
	uses?: number | undefined
};
	/** order by variance() on columns of table "websliver.return_type_tag" */
["websliver_return_type_tag_variance_order_by"]: {
		uses?: GraphQLTypes["order_by"] | undefined
};
	["websliver_upsert_entries_aff_rows_args"]: {
		entries?: GraphQLTypes["jsonb"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	["websliver_upsert_entries_args"]: {
		entries?: GraphQLTypes["jsonb"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** The owner of one or more devices and one or more workspaces */
["websliver_user"]: {
	__typename: "websliver_user",
	created_at: GraphQLTypes["timestamptz"],
	/** managed by auth */
	email: string,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid: string,
	updated_at: GraphQLTypes["timestamptz"],
	/** managed by auth */
	username: string,
	/** An array relationship */
	workspaces: Array<GraphQLTypes["websliver_workspace"]>,
	/** An aggregate relationship */
	workspaces_aggregate: GraphQLTypes["websliver_workspace_aggregate"]
};
	/** aggregated selection of "websliver.user" */
["websliver_user_aggregate"]: {
	__typename: "websliver_user_aggregate",
	aggregate?: GraphQLTypes["websliver_user_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_user"]>
};
	/** aggregate fields of "websliver.user" */
["websliver_user_aggregate_fields"]: {
	__typename: "websliver_user_aggregate_fields",
	count: number,
	max?: GraphQLTypes["websliver_user_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_user_min_fields"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.user". All fields are combined with a logical 'AND'. */
["websliver_user_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_user_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_user_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_user_bool_exp"]> | undefined,
	created_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	email?: GraphQLTypes["String_comparison_exp"] | undefined,
	last_login_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	name?: GraphQLTypes["String_comparison_exp"] | undefined,
	uid?: GraphQLTypes["String_comparison_exp"] | undefined,
	updated_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	username?: GraphQLTypes["String_comparison_exp"] | undefined,
	workspaces?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.user" */
["websliver_user_constraint"]: websliver_user_constraint;
	/** input type for inserting data into table "websliver.user" */
["websliver_user_insert_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined,
	workspaces?: GraphQLTypes["websliver_workspace_arr_rel_insert_input"] | undefined
};
	/** aggregate max on columns */
["websliver_user_max_fields"]: {
	__typename: "websliver_user_max_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined
};
	/** aggregate min on columns */
["websliver_user_min_fields"]: {
	__typename: "websliver_user_min_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined
};
	/** response of any mutation on the table "websliver.user" */
["websliver_user_mutation_response"]: {
	__typename: "websliver_user_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_user"]>
};
	/** input type for inserting object relation for remote table "websliver.user" */
["websliver_user_obj_rel_insert_input"]: {
		data: GraphQLTypes["websliver_user_insert_input"],
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_user_on_conflict"] | undefined
};
	/** on_conflict condition type for table "websliver.user" */
["websliver_user_on_conflict"]: {
		constraint: GraphQLTypes["websliver_user_constraint"],
	update_columns: Array<GraphQLTypes["websliver_user_update_column"]>,
	where?: GraphQLTypes["websliver_user_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.user". */
["websliver_user_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	email?: GraphQLTypes["order_by"] | undefined,
	last_login_at?: GraphQLTypes["order_by"] | undefined,
	name?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	username?: GraphQLTypes["order_by"] | undefined,
	workspaces_aggregate?: GraphQLTypes["websliver_workspace_aggregate_order_by"] | undefined
};
	/** primary key columns input for table: websliver_user */
["websliver_user_pk_columns_input"]: {
		uid: string
};
	/** select columns of table "websliver.user" */
["websliver_user_select_column"]: websliver_user_select_column;
	/** input type for updating data in table "websliver.user" */
["websliver_user_set_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	email?: string | undefined,
	last_login_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: string | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	/** managed by auth */
	username?: string | undefined
};
	/** update columns of table "websliver.user" */
["websliver_user_update_column"]: websliver_user_update_column;
	/** A historical visit of a location. 'source_id' represents the parent visit if any. */
["websliver_visit"]: {
	__typename: "websliver_visit",
	created_at: GraphQLTypes["timestamptz"],
	location: string,
	source_title: string,
	source_url: string,
	title: string,
	uid: GraphQLTypes["uuid"],
	/** An object relationship */
	workspace: GraphQLTypes["websliver_workspace"],
	workspace_id: GraphQLTypes["uuid"]
};
	/** aggregated selection of "websliver.visit" */
["websliver_visit_aggregate"]: {
	__typename: "websliver_visit_aggregate",
	aggregate?: GraphQLTypes["websliver_visit_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_visit"]>
};
	/** aggregate fields of "websliver.visit" */
["websliver_visit_aggregate_fields"]: {
	__typename: "websliver_visit_aggregate_fields",
	count: number,
	max?: GraphQLTypes["websliver_visit_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_visit_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.visit" */
["websliver_visit_aggregate_order_by"]: {
		count?: GraphQLTypes["order_by"] | undefined,
	max?: GraphQLTypes["websliver_visit_max_order_by"] | undefined,
	min?: GraphQLTypes["websliver_visit_min_order_by"] | undefined
};
	/** input type for inserting array relation for remote table "websliver.visit" */
["websliver_visit_arr_rel_insert_input"]: {
		data: Array<GraphQLTypes["websliver_visit_insert_input"]>,
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_visit_on_conflict"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.visit". All fields are combined with a logical 'AND'. */
["websliver_visit_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_visit_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_visit_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_visit_bool_exp"]> | undefined,
	created_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	location?: GraphQLTypes["String_comparison_exp"] | undefined,
	source_title?: GraphQLTypes["String_comparison_exp"] | undefined,
	source_url?: GraphQLTypes["String_comparison_exp"] | undefined,
	title?: GraphQLTypes["String_comparison_exp"] | undefined,
	uid?: GraphQLTypes["uuid_comparison_exp"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined,
	workspace_id?: GraphQLTypes["uuid_comparison_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.visit" */
["websliver_visit_constraint"]: websliver_visit_constraint;
	/** input type for inserting data into table "websliver.visit" */
["websliver_visit_insert_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_obj_rel_insert_input"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** aggregate max on columns */
["websliver_visit_max_fields"]: {
	__typename: "websliver_visit_max_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by max() on columns of table "websliver.visit" */
["websliver_visit_max_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	source_title?: GraphQLTypes["order_by"] | undefined,
	source_url?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate min on columns */
["websliver_visit_min_fields"]: {
	__typename: "websliver_visit_min_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** order by min() on columns of table "websliver.visit" */
["websliver_visit_min_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	source_title?: GraphQLTypes["order_by"] | undefined,
	source_url?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** response of any mutation on the table "websliver.visit" */
["websliver_visit_mutation_response"]: {
	__typename: "websliver_visit_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_visit"]>
};
	/** on_conflict condition type for table "websliver.visit" */
["websliver_visit_on_conflict"]: {
		constraint: GraphQLTypes["websliver_visit_constraint"],
	update_columns: Array<GraphQLTypes["websliver_visit_update_column"]>,
	where?: GraphQLTypes["websliver_visit_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.visit". */
["websliver_visit_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	location?: GraphQLTypes["order_by"] | undefined,
	source_title?: GraphQLTypes["order_by"] | undefined,
	source_url?: GraphQLTypes["order_by"] | undefined,
	title?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	workspace?: GraphQLTypes["websliver_workspace_order_by"] | undefined,
	workspace_id?: GraphQLTypes["order_by"] | undefined
};
	/** primary key columns input for table: websliver_visit */
["websliver_visit_pk_columns_input"]: {
		uid: GraphQLTypes["uuid"]
};
	/** select columns of table "websliver.visit" */
["websliver_visit_select_column"]: websliver_visit_select_column;
	/** input type for updating data in table "websliver.visit" */
["websliver_visit_set_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	location?: string | undefined,
	source_title?: string | undefined,
	source_url?: string | undefined,
	title?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	workspace_id?: GraphQLTypes["uuid"] | undefined
};
	/** update columns of table "websliver.visit" */
["websliver_visit_update_column"]: websliver_visit_update_column;
	/** A workspace represents an isolated group of tags and bookmarks */
["websliver_workspace"]: {
	__typename: "websliver_workspace",
	created_at: GraphQLTypes["timestamptz"],
	default: boolean,
	/** An array relationship */
	entries: Array<GraphQLTypes["websliver_entry"]>,
	/** An aggregate relationship */
	entries_aggregate: GraphQLTypes["websliver_entry_aggregate"],
	name: string,
	/** An array relationship */
	return_type_tags: Array<GraphQLTypes["websliver_return_type_tag"]>,
	/** An aggregate relationship */
	return_type_tags_aggregate: GraphQLTypes["websliver_return_type_tag_aggregate"],
	uid: GraphQLTypes["uuid"],
	updated_at: GraphQLTypes["timestamptz"],
	/** An object relationship */
	user: GraphQLTypes["websliver_user"],
	user_id: string,
	/** An array relationship */
	visits: Array<GraphQLTypes["websliver_visit"]>,
	/** An aggregate relationship */
	visits_aggregate: GraphQLTypes["websliver_visit_aggregate"]
};
	/** aggregated selection of "websliver.workspace" */
["websliver_workspace_aggregate"]: {
	__typename: "websliver_workspace_aggregate",
	aggregate?: GraphQLTypes["websliver_workspace_aggregate_fields"] | undefined,
	nodes: Array<GraphQLTypes["websliver_workspace"]>
};
	/** aggregate fields of "websliver.workspace" */
["websliver_workspace_aggregate_fields"]: {
	__typename: "websliver_workspace_aggregate_fields",
	count: number,
	max?: GraphQLTypes["websliver_workspace_max_fields"] | undefined,
	min?: GraphQLTypes["websliver_workspace_min_fields"] | undefined
};
	/** order by aggregate values of table "websliver.workspace" */
["websliver_workspace_aggregate_order_by"]: {
		count?: GraphQLTypes["order_by"] | undefined,
	max?: GraphQLTypes["websliver_workspace_max_order_by"] | undefined,
	min?: GraphQLTypes["websliver_workspace_min_order_by"] | undefined
};
	/** input type for inserting array relation for remote table "websliver.workspace" */
["websliver_workspace_arr_rel_insert_input"]: {
		data: Array<GraphQLTypes["websliver_workspace_insert_input"]>,
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_workspace_on_conflict"] | undefined
};
	/** Boolean expression to filter rows from the table "websliver.workspace". All fields are combined with a logical 'AND'. */
["websliver_workspace_bool_exp"]: {
		_and?: Array<GraphQLTypes["websliver_workspace_bool_exp"]> | undefined,
	_not?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined,
	_or?: Array<GraphQLTypes["websliver_workspace_bool_exp"]> | undefined,
	created_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	default?: GraphQLTypes["Boolean_comparison_exp"] | undefined,
	entries?: GraphQLTypes["websliver_entry_bool_exp"] | undefined,
	name?: GraphQLTypes["String_comparison_exp"] | undefined,
	return_type_tags?: GraphQLTypes["websliver_return_type_tag_bool_exp"] | undefined,
	uid?: GraphQLTypes["uuid_comparison_exp"] | undefined,
	updated_at?: GraphQLTypes["timestamptz_comparison_exp"] | undefined,
	user?: GraphQLTypes["websliver_user_bool_exp"] | undefined,
	user_id?: GraphQLTypes["String_comparison_exp"] | undefined,
	visits?: GraphQLTypes["websliver_visit_bool_exp"] | undefined
};
	/** unique or primary key constraints on table "websliver.workspace" */
["websliver_workspace_constraint"]: websliver_workspace_constraint;
	/** input type for inserting data into table "websliver.workspace" */
["websliver_workspace_insert_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	default?: boolean | undefined,
	entries?: GraphQLTypes["websliver_entry_arr_rel_insert_input"] | undefined,
	name?: string | undefined,
	return_type_tags?: GraphQLTypes["websliver_return_type_tag_arr_rel_insert_input"] | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user?: GraphQLTypes["websliver_user_obj_rel_insert_input"] | undefined,
	user_id?: string | undefined,
	visits?: GraphQLTypes["websliver_visit_arr_rel_insert_input"] | undefined
};
	/** aggregate max on columns */
["websliver_workspace_max_fields"]: {
	__typename: "websliver_workspace_max_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user_id?: string | undefined
};
	/** order by max() on columns of table "websliver.workspace" */
["websliver_workspace_max_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	name?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	user_id?: GraphQLTypes["order_by"] | undefined
};
	/** aggregate min on columns */
["websliver_workspace_min_fields"]: {
	__typename: "websliver_workspace_min_fields",
	created_at?: GraphQLTypes["timestamptz"] | undefined,
	name?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user_id?: string | undefined
};
	/** order by min() on columns of table "websliver.workspace" */
["websliver_workspace_min_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	name?: GraphQLTypes["order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	user_id?: GraphQLTypes["order_by"] | undefined
};
	/** response of any mutation on the table "websliver.workspace" */
["websliver_workspace_mutation_response"]: {
	__typename: "websliver_workspace_mutation_response",
	/** number of rows affected by the mutation */
	affected_rows: number,
	/** data from the rows affected by the mutation */
	returning: Array<GraphQLTypes["websliver_workspace"]>
};
	/** input type for inserting object relation for remote table "websliver.workspace" */
["websliver_workspace_obj_rel_insert_input"]: {
		data: GraphQLTypes["websliver_workspace_insert_input"],
	/** upsert condition */
	on_conflict?: GraphQLTypes["websliver_workspace_on_conflict"] | undefined
};
	/** on_conflict condition type for table "websliver.workspace" */
["websliver_workspace_on_conflict"]: {
		constraint: GraphQLTypes["websliver_workspace_constraint"],
	update_columns: Array<GraphQLTypes["websliver_workspace_update_column"]>,
	where?: GraphQLTypes["websliver_workspace_bool_exp"] | undefined
};
	/** Ordering options when selecting data from "websliver.workspace". */
["websliver_workspace_order_by"]: {
		created_at?: GraphQLTypes["order_by"] | undefined,
	default?: GraphQLTypes["order_by"] | undefined,
	entries_aggregate?: GraphQLTypes["websliver_entry_aggregate_order_by"] | undefined,
	name?: GraphQLTypes["order_by"] | undefined,
	return_type_tags_aggregate?: GraphQLTypes["websliver_return_type_tag_aggregate_order_by"] | undefined,
	uid?: GraphQLTypes["order_by"] | undefined,
	updated_at?: GraphQLTypes["order_by"] | undefined,
	user?: GraphQLTypes["websliver_user_order_by"] | undefined,
	user_id?: GraphQLTypes["order_by"] | undefined,
	visits_aggregate?: GraphQLTypes["websliver_visit_aggregate_order_by"] | undefined
};
	/** primary key columns input for table: websliver_workspace */
["websliver_workspace_pk_columns_input"]: {
		uid: GraphQLTypes["uuid"]
};
	/** select columns of table "websliver.workspace" */
["websliver_workspace_select_column"]: websliver_workspace_select_column;
	/** input type for updating data in table "websliver.workspace" */
["websliver_workspace_set_input"]: {
		created_at?: GraphQLTypes["timestamptz"] | undefined,
	default?: boolean | undefined,
	name?: string | undefined,
	uid?: GraphQLTypes["uuid"] | undefined,
	updated_at?: GraphQLTypes["timestamptz"] | undefined,
	user_id?: string | undefined
};
	/** update columns of table "websliver.workspace" */
["websliver_workspace_update_column"]: websliver_workspace_update_column
    }
/** column ordering options */
export const enum order_by {
	asc = "asc",
	asc_nulls_first = "asc_nulls_first",
	asc_nulls_last = "asc_nulls_last",
	desc = "desc",
	desc_nulls_first = "desc_nulls_first",
	desc_nulls_last = "desc_nulls_last"
}
/** unique or primary key constraints on table "websliver.entry" */
export const enum websliver_entry_constraint {
	Entry_pkey = "Entry_pkey",
	entry_title_location_workspace_id_key = "entry_title_location_workspace_id_key"
}
/** select columns of table "websliver.entry" */
export const enum websliver_entry_select_column {
	created_at = "created_at",
	description = "description",
	favicon_id = "favicon_id",
	location = "location",
	tags = "tags",
	title = "title",
	uid = "uid",
	updated_at = "updated_at",
	workspace_id = "workspace_id"
}
/** update columns of table "websliver.entry" */
export const enum websliver_entry_update_column {
	created_at = "created_at",
	description = "description",
	favicon_id = "favicon_id",
	location = "location",
	tags = "tags",
	title = "title",
	uid = "uid",
	updated_at = "updated_at",
	workspace_id = "workspace_id"
}
/** unique or primary key constraints on table "websliver.favicon" */
export const enum websliver_favicon_constraint {
	favicon_hash_key = "favicon_hash_key",
	favicon_pkey = "favicon_pkey"
}
/** select columns of table "websliver.favicon" */
export const enum websliver_favicon_select_column {
	created_at = "created_at",
	data = "data",
	hash = "hash",
	uid = "uid",
	updated_at = "updated_at"
}
/** update columns of table "websliver.favicon" */
export const enum websliver_favicon_update_column {
	created_at = "created_at",
	data = "data",
	hash = "hash",
	uid = "uid",
	updated_at = "updated_at"
}
/** unique or primary key constraints on table "websliver.return_type_affected_rows" */
export const enum websliver_return_type_affected_rows_constraint {
	return_type_affected_rows_pkey = "return_type_affected_rows_pkey"
}
/** select columns of table "websliver.return_type_affected_rows" */
export const enum websliver_return_type_affected_rows_select_column {
	affected_rows = "affected_rows"
}
/** update columns of table "websliver.return_type_affected_rows" */
export const enum websliver_return_type_affected_rows_update_column {
	affected_rows = "affected_rows"
}
/** unique or primary key constraints on table "websliver.return_type_tag" */
export const enum websliver_return_type_tag_constraint {
	return_type_tag_pkey = "return_type_tag_pkey"
}
/** select columns of table "websliver.return_type_tag" */
export const enum websliver_return_type_tag_select_column {
	name = "name",
	uses = "uses",
	workspace_id = "workspace_id"
}
/** update columns of table "websliver.return_type_tag" */
export const enum websliver_return_type_tag_update_column {
	name = "name",
	uses = "uses",
	workspace_id = "workspace_id"
}
/** unique or primary key constraints on table "websliver.user" */
export const enum websliver_user_constraint {
	user_pkey = "user_pkey"
}
/** select columns of table "websliver.user" */
export const enum websliver_user_select_column {
	created_at = "created_at",
	email = "email",
	last_login_at = "last_login_at",
	name = "name",
	uid = "uid",
	updated_at = "updated_at",
	username = "username"
}
/** update columns of table "websliver.user" */
export const enum websliver_user_update_column {
	created_at = "created_at",
	email = "email",
	last_login_at = "last_login_at",
	name = "name",
	uid = "uid",
	updated_at = "updated_at",
	username = "username"
}
/** unique or primary key constraints on table "websliver.visit" */
export const enum websliver_visit_constraint {
	visit_pkey = "visit_pkey"
}
/** select columns of table "websliver.visit" */
export const enum websliver_visit_select_column {
	created_at = "created_at",
	location = "location",
	source_title = "source_title",
	source_url = "source_url",
	title = "title",
	uid = "uid",
	workspace_id = "workspace_id"
}
/** update columns of table "websliver.visit" */
export const enum websliver_visit_update_column {
	created_at = "created_at",
	location = "location",
	source_title = "source_title",
	source_url = "source_url",
	title = "title",
	uid = "uid",
	workspace_id = "workspace_id"
}
/** unique or primary key constraints on table "websliver.workspace" */
export const enum websliver_workspace_constraint {
	workspace_pkey = "workspace_pkey"
}
/** select columns of table "websliver.workspace" */
export const enum websliver_workspace_select_column {
	created_at = "created_at",
	default = "default",
	name = "name",
	uid = "uid",
	updated_at = "updated_at",
	user_id = "user_id"
}
/** update columns of table "websliver.workspace" */
export const enum websliver_workspace_update_column {
	created_at = "created_at",
	default = "default",
	name = "name",
	uid = "uid",
	updated_at = "updated_at",
	user_id = "user_id"
}