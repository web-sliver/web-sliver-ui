import { order_by } from './apiClient/zeus'
import { Client } from './client'

export * from './entries/queries'
export * from './tags/queries'
export * from './workspaces/queries'


export const getExportData = async ({
  workspaceId,
  disableFavicons = false,
  host,
  token,
}: {
  workspaceId?: string,
  disableFavicons?: boolean,
  host: string,
  token: string,
}) => {

  const client = new Client({ host, token })

  let entryFilter = { order_by: [{ uid: order_by.asc }] }
  let workspaceFilter = { order_by: [{ uid: order_by.asc }], where: {} }
  let faviconFilter = { order_by: [{ uid: order_by.asc }], where: {} }

  if (workspaceId) {
    workspaceFilter.where = {
      uid: { _eq: workspaceId }
    }

    faviconFilter.where = {
      entries: { workspace_id: { _eq: workspaceId } }
    }
  }

  if (disableFavicons) {
    // ensure the query returns no favicons
    faviconFilter.where = {
      entries: { workspace_id: { _eq: '00000000-0000-0000-0000-000000000000' } }
    }
  }

  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      workspaceFilter,
      {
        uid: true,
        name: true,
        default: true,
        created_at: true,
        updated_at: true,
        entries: [
          entryFilter,
          {
            uid: true,
            title: true,
            description: true,
            location: true,
            tags: [{}, true],
            created_at: true,
            updated_at: true,
            favicon_id: true,
          }
        ]
      }
    ],
    websliver_favicon: [
      faviconFilter,
      {
        uid: true,
        data: true,
        hash: true,
        created_at: true,
        updated_at: true,
      }
    ]
  })


  try {
    const resp = await executeQuery()
    return resp || {}

  } catch (e) {
    console.error(e)
    return {}
  }
}
