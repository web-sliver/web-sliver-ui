import { Chain } from "./apiClient/zeus"

export class Client {
  host: string
  token: string

  constructor({ host, token }: { host: string, token: string}) {
    this.host = host
    this.token = token
  }

  getQueryExecutor = () => Chain(this.host, {
    headers: {
      'content-type': 'application/json',
      'Authorization': `Bearer ${this.token}`
    }
  })

  buildQuery = () => this.getQueryExecutor()('query')
  buildMutation = () => this.getQueryExecutor()('mutation')
}
