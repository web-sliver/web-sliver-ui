import { Client } from '../client'


export const deleteWorkspace = async ({ workspaceId, host, token }: { workspaceId: string, host: string, token: string, }) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    delete_websliver_workspace: [
      {
        where: {
          uid: { _in: [workspaceId] }
        }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.delete_websliver_workspace?.affected_rows

  } catch (e) {
    console.error(e)
    return
  }
}

export const insertWorkspace = async ({ workspace, host, token }: { workspace: any, host: string, token: string }) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    insert_websliver_workspace: [
      {
        objects: [workspace]
      },
      {
        affected_rows: true,
        returning: {
          uid: true
        }
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.insert_websliver_workspace

  } catch (e) {
    console.error(e)
    return
  }
}

export const updateWorkspace = async ({ workspace, host, token }: { workspace: any, host: string, token: string, }) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    update_websliver_workspace_by_pk: [
      {
        _set: {
          default: workspace.default,
          name: workspace.name || ''
        },
        pk_columns: { uid: workspace.uid }
      },
      {
        uid: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.update_websliver_workspace_by_pk

  } catch (e) {
    console.error(e)
    return
  }
}

export const resetDefaultWorkspace = async ({ host, token, }: { host: string, token: string, }) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    update_websliver_workspace: [
      {
        _set: { default: false },
        where: { default: { _eq: true } }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.update_websliver_workspace

  } catch (e) {
    console.error(e)
    return
  }
}
