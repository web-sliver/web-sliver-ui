import { order_by } from '../apiClient/zeus'
import { Client } from '../client'


export const getWorkspaces = async ({ host, token }: { host: string, token: string }) => {

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_workspace: [
      {
        order_by: [
          { default: order_by.desc },
          { name: order_by.asc_nulls_first }
        ]
      },
      {
        uid: true,
        name: true,
        default: true,
        entries_aggregate: [
          {},
          {
            aggregate: {
              count: [{}, true]
            }
          }
        ]
      }
    ]
  })

  try {
    const resp = await executeQuery()
    const workspaces = resp.websliver_workspace.map((workspace: any) => ({
      ...workspace,
      entriesCount: workspace.entries_aggregate.aggregate.count
    }))

    return workspaces

  } catch (e) {
    console.error(e)
    return []
  }
}
