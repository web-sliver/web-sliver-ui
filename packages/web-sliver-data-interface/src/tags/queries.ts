import { Client } from '../client'

import { DEFAULT_ENTRY_LIMIT, parseSortDirection } from '../utils'


export const getMatchingTags = async ({
  workspaceId,
  value = '',
  offset = 0,
  limit = DEFAULT_ENTRY_LIMIT,
  sortField = 'name',
  sortDirection = 'asc',
  host,
  token,
}: {
  workspaceId: string,
  value?: string,
  offset?: number,
  limit?: number,
  sortField?: string,
  sortDirection?: string,
  host: string,
  token: string,
}) => {

  sortField = sortField || 'name'
  sortDirection = sortDirection || 'asc'

  if (!token) {
    console.error('Token not provided.')
    return []
  }

  if (workspaceId.length == 0) {
    return []
  }

  let terms = value.split('.*')
  let exclusions: string[] = []
  let inputs: string[] = []

  terms.forEach((term: string) => {
    if (term.startsWith('-')) {
      term = term.slice(1).trim()

      if (term.length !==0) {
        exclusions.push(term)
      }

    } else {
      term = term.trim()

      if (term.length !== 0) {
        inputs.push(term)
      }
    }
  })

  // note: nisimilar was not available. updated when it becomes available

  const exclusionTerm = exclusions.length === 0 ? '' : `%(${exclusions.join('|')})%`
  const inputTerm = inputs.length === 0 ? '' : `.*${inputs.join('.*')}.*`

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_search_tags: [
      {
        where: {
          name: {
            _iregex: inputTerm,
            _nsimilar: exclusionTerm,
          },
          workspace_id: { _eq: workspaceId }
        },
        limit,
        offset,
        order_by: [{ [sortField]: parseSortDirection(sortDirection) }]
      },
      {
        name: true,
        uses: true
      }
    ]
  })

  try {
    const resp = await executeQuery()

    return resp.websliver_search_tags

  } catch (e) {
    console.error(e)
    return []
  }
}

export const getMatchingTagsCount = async ({
  workspaceId,
  value = '',
  host,
  token,
}: {
  workspaceId: string,
  value: string,
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return 0
  }

  if (workspaceId.length == 0) {
    return 0
  }


  let terms = value.split('.*')
  let exclusions: string[] = []
  let inputs: string[] = []

  terms.forEach((term: string) => {
    if (term.startsWith('-')) {
      term = term.slice(1).trim()

      if (term.length !==0) {
        exclusions.push(term)
      }

    } else {
      term = term.trim()

      if (term.length !== 0) {
        inputs.push(term)
      }
    }
  })

  // note: nisimilar was not available. updated when it becomes available

  const exclusionTerm = exclusions.length === 0 ? '' : `%(${exclusions.join('|')})%`
  const inputTerm = inputs.length === 0 ? '' : `.*${inputs.join('.*')}.*`

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildQuery()({
    websliver_search_tags_aggregate: [
      {
        where: {
          name: {
            _iregex: inputTerm,
            _nsimilar: exclusionTerm,
          },
          workspace_id: { _eq: workspaceId }
        },
      },
      {
        aggregate: {
          count: [{}, true]
        }
      }
    ]
  })

  try {
    const resp = await executeQuery()

    return resp.websliver_search_tags_aggregate.aggregate?.count || 0

  } catch (e) {
    console.error(e)
    return -1
  }
}
