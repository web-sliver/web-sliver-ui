import { getEntriesByIds } from '../entries/queries'
import { updateEntries } from '../entries/mutations'
import { Client } from '../client'
import { getEntriesQParams, } from '../utils'
import { Entry, SearchQuery } from '../types'


export const replaceTags = async ({ workspaceId, oldTags, newTags, host, token, }: {
  workspaceId: string,
  oldTags: string[],
  newTags: string[],
  host: string,
  token: string,
}) => {
  // replace all in oldTags and add new Tags. oldTags also functions as filtering in this case

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    websliver_replace_tags: [
      {
        args: {
          new_tags: newTags,
          old_tags: oldTags,
          workspace: workspaceId
        }
      },
      {
        uid: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.websliver_replace_tags

  } catch (e) {
    console.error(e)
    return
  }
}

export const addTagsToEntries = async ({ workspaceId, entryIds, tags, host, token, }: {
  workspaceId: string,
  entryIds: string[],
  tags: string[],
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const entries = await getEntriesByIds({ entryIds: entryIds, workspaceId: workspaceId, host, token })

  const updatedEntries = entries.map((entry: any) => ({
    uid: entry.uid,
    tags: Array.from(new Set([...entry.tags, ...tags]))
  }))

  await updateEntries({ entries: updatedEntries, normalize: false, host, token })
}

export const addTagsToEntriesByQuery = async ({ query, tags, host, token, }: {
  query: SearchQuery,
  tags: string[],
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const params = getEntriesQParams(query)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    update_websliver_entry: [
      {
        ...params,
        _append: { tags }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.update_websliver_entry

  } catch (e) {
    console.error(e)
    return
  }
}

export const deleteTagForEntriesByQuery = async ({ query, tag, host, token, }: {
  query: SearchQuery,
  tag: string,
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const params = getEntriesQParams(query)

  const client = new Client({ host, token })
  const executeQuery = async () => await client.buildMutation()({
    update_websliver_entry: [
      {
        ...params,
        _delete_key: { tags: tag }
      },
      {
        affected_rows: true
      }
    ]
  })

  try {
    const resp = await executeQuery()
    return resp.update_websliver_entry

  } catch (e) {
    console.error(e)
    return
  }
}

export const deleteTagsForEntriesByQuery = async ({ query, tags, host, token, }: {
  query: SearchQuery,
  tags: string[],
  host: string,
  token: string,
}) => {

  for (const tag of tags) {
    await deleteTagForEntriesByQuery({ query, tag, host, token, })
  }
}

export const deleteTagsForEntries = async ({ workspaceId, entryIds, tags, host, token }: {
  workspaceId: string,
  entryIds: string[],
  tags: string[],
  host: string,
  token: string,
}) => {

  if (!token) {
    console.error('Token not provided.')
    return
  }

  const entries = await getEntriesByIds({ entryIds: entryIds, workspaceId: workspaceId, host, token })

  const updatedEntries = entries.map((entry: Entry) => ({
    uid: entry.uid,
    tags: Array.from(new Set(entry.tags.filter((tag: string) => !tags.includes(tag))))
  }))

  await updateEntries({ entries: updatedEntries, normalize: false, host, token })
}
