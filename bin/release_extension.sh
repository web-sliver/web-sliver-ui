#!/usr/bin/env bash

EXTENSION_VERSION=$1

if [[ -z $EXTENSION_VERSION ]]; then
    printf '\n\nMissing version parameter!\n'
    exit -1
fi

if [[ -z $WEB_EXT_API_KEY || -z $WEB_EXT_API_SECRET || -z $GITLAB_TOKEN ]]; then
    printf '\n\nMissing environment variables!\n'
    exit -2
fi

printf "\n\nUpdating version: $EXTENSION_VERSION ..\n"
sed -i -e "s/\"version\": \".*\"/\"version\": \"$EXTENSION_VERSION\"/" apps/web-sliver-extension/package.json apps/web-sliver-extension/src/manifest.json

printf '\n\nUpdating packages ..\n'
yarn workspace web-sliver-extension install

printf '\n\nBuilding extension ..\n'
yarn package.extension

printf '\n\nPublishing extension ..\n'
./bin/publish_extension.sh $EXTENSION_VERSION

printf '\n\nCreating commit and pushing ..\n'
git add apps/web-sliver-extension/package.json apps/web-sliver-extension/src/manifest.json
git commit -m "release[ws-ext]: version $EXTENSION_VERSION"
git push

printf '\n\nAll done.\n'