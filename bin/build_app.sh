#!/usr/bin/env bash

rsync \
    -avzP \
    --exclude-from=bin/rsync_exclude.txt \
    --delete \
    --delete-excluded \
    ./ \
    $SSH_USER@$SSH_HOST:$SSH_PATH
