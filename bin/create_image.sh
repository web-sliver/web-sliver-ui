#!/usr/bin/env bash

# todo parametrize to allow for dev builds

# export environment variables from .env file
set -o allexport
source .env.websliver.docker
set +o allexport

echo "WEBSLIVER_APP_URL=$WEBSLIVER_APP_URL"

docker build \
    --build-arg WEBSLIVER_APP_URL=$WEBSLIVER_APP_URL \
    --build-arg WEBSLIVER_API_URL=$WEBSLIVER_API_URL \
    --build-arg WEBSLIVER_AUTH_DOMAIN=$WEBSLIVER_AUTH_DOMAIN \
    --build-arg WEBSLIVER_AUTH_CLIENT_ID=$WEBSLIVER_AUTH_CLIENT_ID \
    -t websliver-local \
    -f ws-base.dockerfile \
    .

docker tag websliver-local docker-registry.dssl.dystopianside.org/websliver:latest
docker push docker-registry.dssl.dystopianside.org/websliver:latest
