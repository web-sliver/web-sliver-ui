#!/usr/bin/env bash

# set this to new version
EXTENSION_VERSION=$1

EXTENSION_FILE="50286437468a42299749-$EXTENSION_VERSION.xpi"


# add ?select=package_file to the url for more details about the upload
curl \
    --request PUT \
    --header "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    --upload-file ./apps/web-sliver-extension/dist-pub/$EXTENSION_FILE \
    "https://gitlab.com/api/v4/projects/19120454/packages/generic/web-sliver-extension/$EXTENSION_VERSION/web-sliver-extension-$EXTENSION_VERSION.xpi"
