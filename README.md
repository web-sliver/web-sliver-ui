# WebSliver UI

## Installing the UI project
TBD

## Installing the extension (Firefox)
1. Go to the [extension downloads page](https://gitlab.com/web-sliver/web-sliver-ui/-/packages/?type=&orderBy=created_at&sort=desc&search%5B%5D=web-sliver-extension)
2. Download the latest version of web-sliver-extension
3. Open the file using Firefox and click Add when prompted.

alternately:

3. Open [about:addons](about:addons), press ctrl+shift+A or go to the Firefox add-ons settings window.
4. Click the gear button and then "Install Add-on From File..."
5. Select the downloaded file and install it.

optionally:

6. (for self-hosted instances) Click the add-on button and then the gear button. This will open the Options page. In there you will be able to input your instance URL.

## Mobile Sharing
Using the [HTTP Shortcuts](https://play.google.com/store/apps/details?id=ch.rmy.android.http_shortcuts) Android app, import [this file](/bin/other/shortcuts.json) and edit the variables with your own configuration.

## Publishing the extension
1. Make sure the following environment variables are set: WEB_EXT_API_KEY, WEB_EXT_API_SECRET, GITLAB_TOKEN
2. Run: `./bin/release_extension.sh <version>`
